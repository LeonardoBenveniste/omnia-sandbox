import sbt._
import Keys._
import play.Play.autoImport._
import com.typesafe.sbt.web.PathMapping
import com.typesafe.sbt.web.SbtWeb
import com.typesafe.sbt.web.SbtWeb.autoImport._
import com.typesafe.sbt.jse.SbtJsEngine
import com.typesafe.sbt.jse.SbtJsEngine.autoImport._
import org.databrary.sbt.angular.templates.AngularTemplates.autoImport._
import com.typesafe.sbt.gzip.SbtGzip.autoImport._
import net.ground5hark.sbt.concat.SbtConcat.autoImport._
import com.typesafe.sbt.digest.SbtDigest.autoImport._
import com.typesafe.sbt.SbtNativePackager.Universal
import PlayKeys._

object ApplicationBuild extends Build {

  val appName         = "mvp"
  val appVersion      = "2.0.85"

  val appDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    anorm,
    ws,
    cache,
    "com.typesafe.slick" %% "slick" % "2.1.0",
    "org.mindrot" % "jbcrypt" % "0.3m",
    "org.apache.commons" % "commons-email" % "1.3.2",
    "com.google.inject" % "guice" % "3.0",
    "com.tzavellas" % "sse-guice" % "0.7.1",
    "io.dropwizard.metrics" % "metrics-core" % "3.1.0",
    "io.dropwizard.metrics" % "metrics-jvm" % "3.1.0",
    "net.alchim31" % "metrics-influxdb" % "0.5.0",
    "org.webjars" %% "webjars-play" % "2.3.0-2",
    "org.webjars" % "font-awesome" % "4.3.0-1",
    "net.debasishg" %% "redisclient" % "3.0",
    "org.logback-extensions" % "logback-ext-loggly" % "0.1.2",
    "io.dropwizard.metrics" % "metrics-core" % "3.1.2",
    "org.coursera" % "metrics-datadog" % "1.1.2"
  )

  val concatGroups = Seq(

    "javascripts/omnia-controllers.min.js" -> group(Seq(
      "javascripts/controllers/parent.js",
      "javascripts/controllers/patient.js",
      "javascripts/controllers/patient-list.js",
      "javascripts/controllers/patient-search.js",
      "javascripts/controllers/navbar.js",
      "javascripts/controllers/timeline.js",
      "javascripts/controllers/flat-timeline.js",
      "javascripts/controllers/timeline-controls.js",
      "javascripts/controllers/timeline-filters.js",
      "javascripts/controllers/problems-card.js",
      "javascripts/controllers/allergies-card.js",
      "javascripts/controllers/evolution-form.js",
      "javascripts/controllers/medical-info-forms/medical-info-container.js",
      "javascripts/controllers/medical-info-forms/medical-info-evolution.js",
      "javascripts/controllers/medical-info-forms/ingreso.js",
      "javascripts/controllers/medical-info-forms/egreso.js",
      "javascripts/controllers/medical-info-forms/pq.js",
      "javascripts/controllers/medical-info-forms/pa.js",
      "javascripts/controllers/medical-info-forms/monitoreo.js",
      "javascripts/controllers/patient-header.js",
      "javascripts/controllers/log.js",
      "javascripts/controllers/problem-form.js",
      "javascripts/controllers/evolution-new-form.js"
    )),

    "javascripts/omnia.min.js" -> group(Seq(
      "javascripts/lib/jquery.js",
      "javascripts/lib/idle-timer.min.js",
      "javascripts/lib/select2.js",
      "javascripts/lib/typeahead.jquery.min.js",
      "javascripts/lib/stacktrace.js",
      "javascripts/lib/underscore.js",
      "javascripts/lib/bootstrap.js",
      "javascripts/lib/toastr.js",
      "javascripts/lib/moment.js",
      "javascripts/lib/angular.js",
      "javascripts/lib/angular-locale_es-ar.js",
      "javascripts/lib/angular-route.js",
      "javascripts/lib/angular-animate.js",
      "javascripts/lib/angular-sanitize.js",
      "javascripts/lib/angular-typeahead.js",
      "javascripts/lib/angular-cookies.js",
      "javascripts/lib/md5.js",
      "javascripts/lib/ekko-lightbox.js",
      "javascripts/lib/bootstrap-datetimepicker.js",
      "javascripts/lib/bootstrap-timepicker.min.js",
      "javascripts/lib/haywire.min.js",
      "javascripts/lib/highcharts.js",
      "javascripts/services.js",
      "javascripts/timeline-services.js",
      "javascripts/animations.js",
      "javascripts/app.js",
      "javascripts/login.js"
    )),

    "stylesheets/all.css" -> group(Seq(

      "stylesheets/toastr.css",
      "stylesheets/datetimepicker-custom.css",
      "stylesheets/bootstrap.css",
      "stylesheets/bootstrap-reset.css",
      "stylesheets/style.css",
      "stylesheets/style-responsive.css",
      "stylesheets/ekko-lightbox.css",
      "stylesheets/main.css",
      "stylesheets/custom-medical-info.css",
      "stylesheets/bootstrap-timepicker.min.css"
    ))
  )

  val main = Project(appName, file(".")).enablePlugins(play.PlayScala).enablePlugins(SbtWeb).settings(
    version := appVersion,
    libraryDependencies ++= appDependencies,
    scalaVersion := "2.10.4",
    scalacOptions ++= Seq("-Xlint", "-deprecation", "-feature", "-language:postfixOps", "-Xfatal-warnings"),
    parallelExecution in Test := false,
    Concat.groups := concatGroups,
    pipelineStages := Seq(concat, digest, gzip),
    AngularTemplatesKeys.outputJs := Some("templates.min.js"),
    sources in doc in Compile := List(),
    AngularTemplatesKeys.module := "ehrApp",
    JsEngineKeys.engineType := JsEngineKeys.EngineType.Node,
    AngularTemplatesKeys.naming := (path => "/assets/" + path)
  )
}
