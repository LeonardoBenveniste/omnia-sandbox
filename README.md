# README #

### Requirements ###

* Java 7 installed
* Play! Framework [2.2.1](https://downloads.typesafe.com/play/2.2.1/play-2.2.1.zip) installed
* NodeJs installed
* npm installed
* [typescript](http://www.typescriptlang.org/index.html#download-links) installed

### Running ###
* open a terminal and go where this README file is located
* ```> play run```
* point your browser to: [http://localhost:9000](http://localhost:9000)

### Misc ###
To make sure all .ts files are compiled (specially before pushing changes) run:
```> play clean assets```

