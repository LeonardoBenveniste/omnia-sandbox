import play.api._
import play.api.Play.current
import play.api.libs.concurrent.Akka
import akka.actor._
import _root_.util.ContextAwareExecutor.Implicits._
import services.TimelineLog
import models.TimelineEntry

class TimelineActor(log: TimelineLog) extends Actor {

  override def preStart = {
    Akka.system.eventStream.subscribe(context.self, classOf[TimelineEntry])
  }

  def receive = {
    case entry: TimelineEntry => {
      log.append(entry).map(identity)
      Logger.info(entry.toString)
    }
  }
}

object TimelineActor {
  def props(log: TimelineLog) = Props(new TimelineActor(log))
}