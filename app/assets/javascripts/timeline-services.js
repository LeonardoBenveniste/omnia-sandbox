/// <reference path="lib/moment.d.ts" />
/// <reference path="lib/underscore.d.ts" />
/// <reference path="lib/angular.d.ts" />
/// <reference path="model.d.ts" />
/// <reference path="services.d.ts" />
/// <reference path="timeline-services.d.ts" />
omniaServices.service('FlatTimeline', ['$http', '$q', 'VitalSigns', 'Problems', 'Indication', 'Results', 'ContingencyService', '$rootScope',
    function ($http, $q, VitalSigns, Problems, Indication, Results, Contingency, $rootScope) {
        // TODO: refactor this using typescript compliant code.
        var _indexedBy = function (collection, key) {
            // make arrays of elements with the same 'key'.
            var arrays = _(collection).groupBy(function (obj) { return obj[key]; });
            // index the array by that 'key' (not a regular 0...N index).
            _(arrays).each(function (it, key) { return arrays[key] = it.shift(); });
            return arrays;
        };
        var getByEpisode = function (patientId, episodeId) {
            var request = $http({
                url: "/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/timeline",
                method: 'get'
            });
            return get(request);
        };
        var get = function (request) {
            return request.then(function (response) {
                var evWithAttachments = response.data.evolutions;
                _(response.data.attachments).each(function (att) {
                    var relatedEvolution = _(evWithAttachments).find(function (evolution) {
                        return evolution.id === att.parentId && att.kind === 'finding';
                    });
                    var relatedResult = _(response.data.results).find(function (result) {
                        return result.id === att.parentId && att.kind === 'result';
                    });
                    if (relatedEvolution) {
                        relatedEvolution.attachments = [att];
                    }
                    if (relatedResult) {
                        relatedResult.attachments = [att];
                    }
                });
                _(response.data.results).each(function (result) {
                    result.category = Results.getCategory(result.category);
                });
                var problems = _indexedBy(response.data.problems, 'id');
                var evolutions = _indexedBy(evWithAttachments, 'id');
                var indications = _indexedBy(response.data.indications, 'id');
                var vitalSigns = _indexedBy(response.data.vital_signs, 'id');
                var treatments = _indexedBy(response.data.treatments, 'id');
                var results = _indexedBy(response.data.results, 'id');
                var solicitations = _indexedBy(response.data.solicitations, 'id');
                var medicalInfo = _indexedBy(response.data.medical_info, 'id');
                var authors = _indexedBy(response.data.authors, 'id');
                var contingencies = _indexedBy(response.data.contingencies, 'id');
                var attachments = _(response.data.attachments).groupBy(function (it) { return it.parentId; });
                var overrides = _(evolutions).map(function (evolution) {
                    return +evolution.overridenBy;
                });
                var entryByDay = function (timeline, day) {
                    return _(timeline).find(function (entry) {
                        return entry.day === day && entry.ownerId === Omnia.currentUserId;
                    });
                };
                var addEvolutionName = function (problem) {
                    problem.evolutionName = Problems.evolutionName(problem);
                };
                var addProblems = function (evolution) {
                    var evProblems = response.data.evolution_problems[evolution.id];
                    evProblems = _(evProblems).map(function (id) { return problems[id]; });
                    _(evProblems).each(addEvolutionName);
                    evolution.problems = evProblems;
                };
                _(response.data.evolutions).each(addProblems);
                var extractProblems = function (info) {
                    var problems = JSON.parse(info.value).problems;
                    if (problems) {
                        _(problems).each(addEvolutionName);
                    }
                    return problems || [];
                };
                var evolutionAuthors = [];
                var medicalInfoProblems = [];
                _(response.data.timeline).each(function (incident, day) {
                    // TODO: we have the author information but we also need to store the ACTIVE
                    // specialty the author had set when doing the diagnostic.
                    incident.author = authors[incident.ownerId];
                    delete incident.patientId;
                    delete incident.ownerId;
                    delete incident.id;
                    switch (incident.kind) {
                        case 'evolution.created':
                            var evolution = evolutions[incident.data1];
                            evolution.owner = incident.author;
                            evolution.editedBy = evolution.overridenBy;
                            incident.evolution = evolution;
                            incident.dirty = !!evolution.overridenBy;
                            if (!incident.dirty && !_(evolutionAuthors).findWhere({ id: evolution.owner.id })) {
                                evolutionAuthors.push(evolution.owner);
                            }
                            break;
                        case 'evolution.edited':
                            incident.old = evolutions[incident.data1];
                            incident.current = evolutions[incident.data2];
                            evolutions[incident.data2].overrides = true;
                            break;
                        case 'evolution.deleted':
                            incident.evolution = evolutions[incident.data1];
                            break;
                        case 'background.created':
                        case 'problem.created':
                            var problem = problems[incident.data1];
                            problem.owner = incident.author;
                            problem.humanState = Problems.status(problem);
                            incident.problem = problem;
                            break;
                        case 'vitalsign.created':
                            var vitalSign = vitalSigns[incident.data1];
                            vitalSign.owner = incident.author;
                            vitalSign.human = VitalSigns.formatHuman(vitalSign);
                            incident.vitalSign = vitalSign;
                            incident.dirty = (vitalSign.status === 'deleted');
                            break;
                        case 'vitalsign.deleted':
                            var deletedSign = vitalSigns[incident.data1];
                            deletedSign.owner = incident.author;
                            deletedSign.human = VitalSigns.formatHuman(deletedSign);
                            incident.vitalSign = deletedSign;
                            incident.dirty = true;
                            break;
                        case 'indication.created':
                        case 'indication.edited':
                        case 'indication.active':
                        case 'indication.suspended':
                        case 'indication.deleted':
                            var indication = indications[incident.data1];
                            indication.owner = incident.author;
                            indication.treatment = treatments[indication.treatmentId];
                            incident.indication = indication;
                            incident.indication.human = Indication.formatHuman(indication);
                            incident.dirty = (indication.state === 'deleted');
                            break;
                        case 'result.created':
                            var result = results[incident.data1];
                            result.owner = incident.author;
                            incident.result = result;
                            break;
                        case 'result.edited':
                            var currentResult = results[incident.data1], oldResult = results[incident.data2];
                            incident.current = currentResult;
                            incident.old = oldResult;
                            break;
                        case 'solicitation.created':
                        case 'solicitation.deleted':
                            var sol = solicitations[incident.data1];
                            sol.owner = incident.author;
                            incident.solicitation = sol;
                            incident.dirty = (sol.status === 'deleted');
                            break;
                        case 'problem.edited':
                            var current = problems[incident.data2], old = problems[incident.data1];
                            current.humanState = Problems.status(current);
                            incident.current = current;
                            old.humanState = Problems.status(old);
                            incident.old = old;
                            break;
                        case 'medicalInfo.closed':
                            var info = medicalInfo[incident.data1];
                            info.owner = incident.author;
                            incident.medicalInfo = info;
                            incident.dirty = !!info.updatedBy;
                            if (!incident.dirty) {
                                medicalInfoProblems = _.union(medicalInfoProblems, extractProblems(info));
                                if (!_(evolutionAuthors).findWhere({ id: info.owner.id })) {
                                    evolutionAuthors.push(info.owner);
                                }
                            }
                            break;
                        case 'medicalInfo.edited':
                            incident.old = medicalInfo[incident.data1];
                            incident.current = medicalInfo[incident.data2];
                            medicalInfo[incident.data2].overrides = true;
                            break;
                        case 'medicalInfo.deleted':
                            incident.medicalInfo = medicalInfo[incident.data1];
                            break;
                        case 'contingency.created':
                            incident.contingency = contingencies[incident.data1];
                            incident.contingency.humanKind = Contingency.humanKind(incident.contingency.kind);
                            incident.contingency.attachments = attachments[incident.data1];
                            incident.createdOverride = incident.contingency.generated;
                            incident.dirty = (incident.contingency.status === 'deleted');
                            break;
                        case 'contingency.deleted':
                            incident.contingency = contingencies[incident.data1];
                            incident.contingency.humanKind = Contingency.humanKind(incident.contingency.kind);
                            incident.contingency.attachments = attachments[incident.data1];
                            break;
                        default:
                            break;
                    }
                    delete incident.data1;
                    delete incident.data2;
                });
                var isDirty = function (item) {
                    return item.overridenBy;
                };
                var byName = function (item) {
                    return item.name;
                };
                var evolutionNameSort = function (item) {
                    return item.evolutionName.toLowerCase();
                };
                var evolutionProblems = _.chain(response.data.evolutions).reject(isDirty).pluck('problems').flatten().uniq(false, byName).sortBy(evolutionNameSort).value();
                var problemsForFilters = _.uniq(_.union(evolutionProblems, medicalInfoProblems), false, byName);
                var authorsForFilters = angular.copy(response.data.authors);
                var filtersInitData = { problems: problemsForFilters, authors: authorsForFilters, evAuthors: evolutionAuthors };
                $rootScope.$emit('timeline-filters-init', filtersInitData);
                return response.data;
            });
        };
        return {
            getByEpisode: getByEpisode
        };
    }]);
omniaServices.factory('TimelineUtils', ['VitalSigns', 'Indication', function (VitalSigns, Indication) {
        var currentYear = moment().month(0).date(0).hour(0).minute(0).second(0).valueOf();
        var getDisplayTimestamp = function (entry) {
            var overriden = (typeof entry.createdOverride !== 'undefined');
            return overriden ? entry.createdOverride : entry.created;
        };
        function createdOrder(data) {
            data = _(data).sortBy(function (entry) { return getDisplayTimestamp(entry) * -1; });
            return data;
        }
        ;
        function log(data) {
            window.console.log(data);
            return data;
        }
        ;
        function humanDate(timestamp) {
            var m = moment(timestamp), dayName = m.format('dddd'), day = m.format('D'), month = m.format('MMMM'), year = m.format('YYYY'), noYearDate = dayName + ' ' + day + ' de ' + month;
            // append year if older than current year.
            return timestamp < currentYear ? noYearDate + ' de ' + year : noYearDate;
        }
        ;
        var humanHour = function (ts) { return moment(ts).format('HH:mm'); };
        function appendHumanDate(data) {
            _(data).each(function (entry) {
                var ts = getDisplayTimestamp(entry);
                entry.humanDate = humanDate(ts);
                entry.humanHour = humanHour(ts);
            });
            return data;
        }
        ;
        function appendHumanFormats(data) {
            _(data).each(function (entry) {
                switch (entry.kind) {
                    case 'vitalsign.created':
                        entry.humanVitalSign = VitalSigns.formatHuman(entry.vitalSign);
                        break;
                    case 'indication.created':
                        entry.humanIndication = Indication.formatHuman(entry.indication);
                        break;
                }
            });
            return data;
        }
        ;
        function renderables(renderableKinds) {
            return function (data) {
                return _(data).filter(function (entry) { return _(renderableKinds).contains(entry.kind); });
            };
        }
        ;
        function filterPristine(data) {
            return _(data).reject(function (entry) { return entry.dirty; });
        }
        ;
        function unixDay(when) {
            return Math.ceil(when / 1000 / 3600 / 24);
        }
        ;
        function filterByEvolutionProblem(problemName) {
            var hasEvolutionProblem = function (item) {
                if (item.evolution) {
                    return _(item.evolution.problems).some(function (p) { return p.name === problemName; });
                }
                else if (item.medicalInfo) {
                    var problems = JSON.parse(item.medicalInfo.value).problems;
                    return problems && problems.some(function (p) { return p.name === problemName; });
                }
                return false;
            };
            return function (timeline) {
                if (problemName && problemName.length > 0) {
                    return _(timeline).filter(hasEvolutionProblem);
                }
                else {
                    return timeline;
                }
            };
        }
        ;
        function filterByAuthor(authorId) {
            return function (timeline) {
                if (authorId && authorId > 0) {
                    return _(timeline).filter(function (entry) { return entry.author.id === authorId; });
                }
                else {
                    return timeline;
                }
            };
        }
        ;
        function filterByDateRange(start, end) {
            var startDay = start ? unixDay(start) : 0;
            var endDay = unixDay(end || Date.now());
            return function (timeline) {
                return _(timeline).filter(function (entry) { return entry.day >= startDay && entry.day <= endDay; });
            };
        }
        ;
        return {
            createdOrder: createdOrder,
            log: log,
            humanDate: humanDate,
            humanHour: humanHour,
            appendHumanDate: appendHumanDate,
            appendHumanFormats: appendHumanFormats,
            renderables: renderables,
            filterPristine: filterPristine,
            unixDay: unixDay,
            filterByEvolutionProblem: filterByEvolutionProblem,
            filterByAuthor: filterByAuthor,
            filterByDateRange: filterByDateRange
        };
    }]);
