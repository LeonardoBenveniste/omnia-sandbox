/// <reference path="log.d.ts" />
/// <reference path="../timeline-services.d.ts" />
/// <reference path="../services.d.ts" />
/// <reference path="../lib/moment.d.ts" />
/// <reference path="../lib/jquery.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../lib/angular.d.ts" />
ehrControllers.controller('LogController', ['$scope', '$rootScope', '$http', '$routeParams', 'Alerts', 'FlatTimeline', 'Diacritics',
    'Problems', 'TimelineUtils', 'UserInfo', 'Solicitations', 'MedicalInfo', '$interpolate',
    function ($scope, $rootScope, $http, $routeParams, Alerts, FlatTimeline, Diacritics, Problems, TimelineUtils, UserInfo, Solicitations, MedicalInfo, $interpolate) {
        Omnia.trackEvent('Ingreso al Log de Eventos', { patient: $routeParams.id });
        var validLogEvents = ['evolution.created', 'evolution.deleted', 'evolution.edited', 'indication.created',
            'problem.created', 'problem.edited', 'vitalsign.created', 'vitalsign.deleted', 'result.created', 'result.edited',
            'background.created', 'indication.edited', 'indication.suspended', 'indication.active', 'indication.deleted',
            'solicitation.created', 'solicitation.deleted', 'medicalInfo.closed', 'medicalInfo.edited', 'medicalInfo.deleted',
            'contingency.created', 'contingency.deleted'];
        var filterInvalid = function (timeline) {
            return _(timeline).filter(function (ev) {
                var overridesEvolution = ev.kind === 'evolution.created' && ev.evolution.overrides;
                var overridesMedicalInfo = ev.kind === 'medicalInfo.closed' && ev.medicalInfo.overrides;
                return validLogEvents.indexOf(ev.kind) >= 0 && !overridesEvolution && !overridesMedicalInfo;
            });
        };
        $scope.noAccentsComparator = Diacritics.noAccentsComparator;
        var appendDayField = function (timeline) {
            return _(timeline).map(function (entry) {
                entry.day = TimelineUtils.unixDay(entry.created);
                return entry;
            });
        };
        var sortByCreationDate = function (timeline) {
            return _(timeline).sortBy(function (entry) { return entry.created; });
        };
        var formatEntries = function (timeline) {
            return _(timeline).map(appendEntryInfo);
        };
        var buildProblemTitle = function (problem) {
            var prefix = ['active', 'solved'].indexOf(problem.state) >= 0 ? 'Problema ' : '';
            var suffix = ['allergy', 'adverse-effect'].indexOf(problem.state) >= 0 ? ' a' : '';
            return prefix + Problems.status(problem) + suffix + ': <b>' + problem.name + '</b>';
        };
        var contingencyExtraInfo = function (ctg) {
            var tab = "&nbsp;&nbsp;&nbsp;&nbsp;";
            var atts = ctg.attachments.map(function (att) { return (tab + " - <a href=\"" + att.url + "\" target=\"_blank\">" + att.name + "</a>"); });
            atts.unshift("Documentos Adjuntos:");
            return [("Fecha y hora de la contingencia: " + moment(ctg.generated).format('DD-MM-YYYY HH:mm[hs]'))].concat(atts);
        };
        var appendEntryInfo = function (event) {
            switch (event.kind) {
                case 'evolution.created':
                    event._title = 'Evolución de: ' + formatProblemList(event.evolution.problems);
                    event._extra = evolutionExtraInfo(event.evolution);
                    break;
                case 'evolution.deleted':
                    event._title = buildLabel('Eliminada', 'danger') + 'Evolución de: ' + formatProblemList(event.evolution.problems);
                    event._extra = evolutionExtraInfo(event.evolution);
                    break;
                case 'evolution.edited':
                    event._title = buildLabel('Editada', 'warning') + 'Evolución de: ' + formatProblemList(event.current.problems);
                    event._extra = getDeltaLines(event.old, event.current, evolutionStringMap);
                    break;
                case 'medicalInfo.closed':
                    event._title = 'Evolución (ficha) de: ' + formatProblemList(JSON.parse(event.medicalInfo.value).problems);
                    event._extra = getExtraInfo(medicalInfoStringMap(event.medicalInfo));
                    break;
                case 'medicalInfo.edited':
                    event._title = buildLabel('Editada', 'warning') + 'Evolución (ficha) de: ' + formatProblemList(JSON.parse(event.current.value).problems);
                    event._extra = getDeltaLines(event.old, event.current, medicalInfoStringMap);
                    break;
                case 'medicalInfo.deleted':
                    event._title = buildLabel('Eliminada', 'danger') + 'Evolución (ficha) de: ' + formatProblemList(JSON.parse(event.medicalInfo.value).problems);
                    event._extra = getExtraInfo(medicalInfoStringMap(event.medicalInfo));
                    break;
                case 'indication.created':
                    event._title = 'Indicación: <b>' + treatmentKindLabels[event.indication.treatment.kind] + '</b>';
                    event._extra = getExtraInfo(indicationStringMap(event.indication));
                    break;
                case 'indication.edited':
                    event._title = buildLabel('Editada', 'warning') + 'Indicación: <b>' + treatmentKindLabels[event.indication.treatment.kind] + '</b>';
                    event._extra = getExtraInfo(indicationStringMap(event.indication));
                    break;
                case 'indication.active':
                case 'indication.suspended':
                    var status = event.kind.substring(11);
                    event._title = 'Indicación <b>' + treatmentKindLabels[event.indication.treatment.kind] + '</b> ' + indicationEditionLabels[status];
                    event._extra = getExtraInfo(indicationStringMap(event.indication));
                    break;
                case 'indication.deleted':
                    event._title = buildLabel('Eliminada', 'danger') + 'Indicación: <b>' + treatmentKindLabels[event.indication.treatment.kind] + '</b>';
                    event._extra = getExtraInfo(indicationStringMap(event.indication));
                    break;
                case 'background.created':
                case 'problem.created':
                    event._title = buildProblemTitle(event.problem);
                    event._extra = getExtraInfo(problemStringMap(event.problem));
                    break;
                case 'problem.edited':
                    if (event.current.state === 'deleted') {
                        event._title = buildLabel('Eliminado', 'danger') + buildProblemTitle(event.old);
                        event._extra = getExtraInfo(problemStringMap(event.old));
                    }
                    else {
                        event._title = buildLabel('Editado', 'warning') + buildProblemTitle(event.current);
                        event._extra = getDeltaLines(event.old, event.current, problemStringMap);
                    }
                    break;
                case 'vitalsign.created':
                    event._title = 'Signo Vital';
                    event._extra = [event.vitalSign.human];
                    break;
                case 'vitalsign.deleted':
                    event._title = buildLabel('Eliminado', 'danger') + 'Signo Vital';
                    event._extra = [event.vitalSign.human];
                    break;
                case 'result.created':
                    event._title = 'Resultado de <b>' + event.result.category.label + '</b>';
                    event._extra = getExtraInfo(resultStringMap(event.result));
                    break;
                case 'result.edited':
                    if (event.current.category.value === 'deleted') {
                        event._title = buildLabel('Eliminado', 'danger') + 'Resultado de <b>' + event.old.category.label + '</b>';
                        event._extra = getExtraInfo(resultStringMap(event.old));
                    }
                    else {
                        event._title = buildLabel('Editado', 'warning') + 'Resultado de <b>' + event.current.category.label + '</b>';
                        event._extra = getDeltaLines(event.old, event.current, resultStringMap);
                    }
                    break;
                case 'solicitation.created':
                    event._title = 'Solicitud de <b>' + _(Solicitations.kinds).findWhere({ 'name': event.solicitation.kind }).label + '</b>';
                    event._extra = getExtraInfo(solicitationStringMap(event.solicitation));
                    break;
                case 'solicitation.deleted':
                    event._title = buildLabel('Eliminado', 'danger') + 'Solicitud de <b>' + _(Solicitations.kinds).findWhere({ 'name': event.solicitation.kind }).label + '</b>';
                    event._extra = getExtraInfo(solicitationStringMap(event.solicitation));
                    break;
                case 'contingency.created':
                    var ctg = event.contingency;
                    event._title = "Reconciliacion de Contingencia: <b> " + ctg.humanKind + " </b>";
                    event._extra = contingencyExtraInfo(ctg);
                    break;
                case 'contingency.deleted':
                    var _ctg = event.contingency;
                    event._title = buildLabel('Eliminado', 'danger') + ("Reconciliacion de Contingencia: <b> " + _ctg.humanKind + " </b>");
                    event._extra = contingencyExtraInfo(_ctg);
                    break;
            }
            return event;
        };
        $scope.appliedFilters = {};
        $scope.filter = {};
        var buildLogHtml = function (timeline) {
            var f = _.compose(sortByCreationDate, appendDayField, filterByAction($scope.appliedFilters.action), TimelineUtils.filterByDateRange($scope.appliedFilters.start, $scope.appliedFilters.end), TimelineUtils.filterByAuthor($scope.appliedFilters.author), formatEntries, filterInvalid);
            return f(timeline);
        };
        var filterByAction = function (action) {
            return function (timeline) {
                if (action) {
                    return _(timeline).filter(function (entry) {
                        return entry.kind.indexOf(action) === 0;
                    });
                }
                else {
                    return timeline;
                }
            };
        };
        $scope.logLoaded = false;
        var renderLog = function () {
            $scope.logEvents = buildLogHtml($scope.rawLogData);
            $scope.logEventsPdf = _($scope.logEvents).map(pdfValues);
            $scope.updatePrintFilter();
            $scope.logLoaded = true;
        };
        var eventBasicKind = function (e) {
            return e.kind.substring(0, e.kind.indexOf('.'));
        };
        var initFilters = function (timeline, authors) {
            var actionLabels = {
                'evolution': 'Evolución',
                'problem': 'Problema',
                'indication': 'Indicación',
                'vitalsign': 'Signo Vital',
                'result': 'Resultado',
                'medicalInfo': 'Ficha'
            };
            var addLabel = function (action) {
                return { id: action, label: actionLabels[action] };
            };
            var addFullName = function (author) {
                return { id: author.id, fullName: author.firstName + ' ' + author.lastName };
            };
            $scope.filterAuthors = _(authors).map(addFullName);
            if ($scope.filterAuthors.length === 1) {
                $scope.filter.author = $scope.filterAuthors[0];
            }
            $scope.filterActions = _.chain(timeline).map(eventBasicKind).uniq().map(addLabel).value();
        };
        var medicalInfoEvolutionTemplates = {};
        FlatTimeline.getByEpisode($routeParams.id, $routeParams.episodeId).then(function (response) {
            initFilters(response.timeline, response.authors);
            $scope.rawLogData = response.timeline;
            MedicalInfo.getEvolutionTemplates().then(function (templates) {
                medicalInfoEvolutionTemplates = templates;
                renderLog();
            });
        });
        $scope.applyFilters = function (filter) {
            var json = angular.copy(filter);
            json.start = json.start ? moment(json.start.toString(), 'DD/MM/YYYY').toDate().getTime() : 0;
            json.end = json.end ? moment(json.end.toString(), 'DD/MM/YYYY').toDate().getTime() : Date.now();
            $scope.appliedFilters = json;
            renderLog();
            Omnia.trackEvent('Log de Paciente: Filtros Aplicados', json);
        };
        $scope.cleanFilters = function () {
            $scope.filter = {};
            $scope.appliedFilters = {};
            $scope.uncheckAll();
            renderLog();
        };
        var pdfValues = function (event) {
            return {
                date: moment(event.created).format('DD/MM/YYYY HH:mm') + ' hs',
                location: event.location,
                title: event._title,
                extra: event._extra,
                author: event.author.firstName + ' ' + event.author.lastName + ' - ' + event.author.license,
                specialty: event.specialty
            };
        };
        $scope.openPdf = function () {
            window.open('about:blank', 'pdf_form', 'menubar=0, scrollbar=0, location=0, status=0, toolbar=0, titlebar=0');
            $('#openpdf').click();
        };
        $scope.showEvent = function (kind) {
            return validLogEvents.indexOf(kind) >= 0;
        };
        $scope.updatePrintFilter = function () {
            $scope.logEventsPdf = _($scope.logEvents).filter(function (e) { return e._print === true; }).map(pdfValues);
        };
        $scope.checkAll = function () {
            angular.forEach($scope.logEvents, function (item) {
                item._print = true;
            });
            $scope.updatePrintFilter();
            $scope.checker = true;
        };
        $scope.uncheckAll = function () {
            angular.forEach($scope.logEvents, function (item) {
                item._print = false;
            });
            $scope.checker = false;
            $scope.updatePrintFilter();
        };
        $scope.preventExpand = function (e) {
            e.stopPropagation();
        };
        $scope.toggleAll = function () {
            if ($scope.checker) {
                $scope.uncheckAll();
            }
            else {
                $scope.checkAll();
            }
        };
        $scope.hasChecked = function () {
            return $scope.logEventsPdf ? $scope.logEventsPdf.length > 0 : false;
        };
        $scope.allChecked = function () {
            if (typeof $scope.logEvents === "undefined") {
                return false;
            }
            for (var j = 0; j < $scope.logEvents.length; j++) {
                if ($scope.logEvents[j]._print == false) {
                    $scope.checker = false;
                    return false;
                }
            }
            return true;
        };
        // TODO: move to 'Indications' service.
        var treatmentKindLabels = {
            'drug': 'Farmacológica',
            'diet': 'Dieta',
            'vaccine': 'Inmunización',
            'recommendation': 'Recomendación'
        };
        var indicationEditionLabels = {
            'active': 'Reactivada',
            'suspended': 'Suspendida',
            'deleted': 'Eliminada'
        };
        var formatProblemList = function (problems) {
            var names = _(problems).map(function (p) {
                return '<b>' + p.name + '</b>';
            });
            return names.join(', ');
        };
        $scope.toggleExpand = function (index) {
            $('#row-' + index).toggle('slow');
            $('#span-plus-' + index).toggle();
            $('#span-minus-' + index).toggle();
        };
        var sortedProblemNames = function (problems) {
            return _(problems).sortBy('name').map(function (p) {
                return '<b>' + p.name + '</b>';
            });
        };
        var buildLink = function (url, text) {
            return '<a target="_blank" href="' + url + '">' + text + '</a>';
        };
        var buildLabel = function (text, type) {
            return '<span class="label label-' + type + '">' + text + '</span>&nbsp;&nbsp;&nbsp;';
        };
        var getAttachmentLink = function (attachments) {
            return attachments ? buildLink(attachments[0].url, attachments[0].name) : '[No posee]';
        };
        var problemStringMap = function (problem) {
            return {
                started: problem.started ? 'Fecha Inicio: ' + moment(problem.started).format('DD/MM/YYYY') : '',
                ended: problem.ended ? 'Fecha de resolución:' + moment(problem.ended).format('DD/MM/YYYY') : '',
                familyRelation: problem.familyRelation ? 'Familiar: ' + Problems.findFamilyRelation(problem.familyRelation).label : '',
                severity: problem.severity ? 'Severidad: ' + Problems.severityLabels[problem.severity] : '',
                note: 'Nota: ' + (problem.note ? problem.note : '[No posee]')
            };
        };
        var evolutionStringMap = function (e) {
            return {
                author: 'Autor: ' + e.owner.firstName + ' ' + e.owner.lastName,
                description: 'Descripción: ' + e.description,
                date: 'Fecha: ' + moment(e.date).format('DD/MM/YYYY'),
                problems: 'Problemas: ' + sortedProblemNames(e.problems),
                attachments: 'Archivo adjunto: ' + getAttachmentLink(e.attachments)
            };
        };
        var medicalInfoStringMap = function (mi) {
            var json = {
                'data': JSON.parse(mi.value),
                'helpers': MedicalInfo.getEvolutionHelpers()
            };
            return {
                description: 'Descripción: ' + $interpolate(medicalInfoEvolutionTemplates[mi.kind])(json)
            };
        };
        var indicationStringMap = function (i) {
            var t = i.treatment;
            return {
                human: i.human,
                dose: t.dose && t.frequencyInHours ? 'Dosis: ' + t.dose + ' cada ' + t.frequencyInHours + ' horas' : '',
                starts: 'Fecha de inicio: ' + moment(i.starts).format('DD/MM/YYYY'),
                duration: i.durationInDays && !i.chronic ? 'Duración: ' + i.durationInDays + ' días' : '',
                chronic: i.chronic ? 'Continuar crónicamente' : '',
                comments: i.comments ? 'Comentarios: ' + i.comments : ''
            };
        };
        var resultStringMap = function (r) {
            var desc = JSON.parse(r.description);
            return {
                type: 'Tipo de estudio: ' + r.resultType,
                description: desc.descriptionStr ? 'Descripción: ' + desc.descriptionStr : '',
                issued: r.issued ? 'Fecha de realización: ' + moment(r.issued).format('DD/MM/YYYY') : '',
                attachments: 'Archivo adjunto: ' + getAttachmentLink(r.attachments)
            };
        };
        var formatWhen = function (conditions) {
            var output = '';
            if (conditions.date) {
                output = conditions.date + (conditions.time ? ' - ' + conditions.time + 'hs.' : '');
            }
            else {
                output = Solicitations.conditionsMoment[conditions.when].label;
            }
            return output;
        };
        var solicitationStringMap = function (s) {
            var desc = JSON.parse(s.description);
            var conditions = JSON.parse(s.conditions);
            var practices = _(desc).map(function (p) { return p.label; }).join(', ');
            return {
                practices: 'Prácticas seleccionadas: ' + practices,
                sample: conditions.sample ? 'Muestra: ' + Solicitations.conditionsSample[conditions.sample] : '',
                transport: conditions.transport ? 'Traslado: ' + Solicitations.conditionsTransport[conditions.transport] : '',
                when: conditions.when ? 'Realizar: ' + formatWhen(conditions) : '',
                reason: conditions.selectedProblem ? 'Motivo: ' + conditions.selectedProblem : '',
                diagnostic: conditions.diagnostic ? 'Diagnóstico presuntivo: ' + conditions.diagnostic : '',
                antibiotics: conditions.antibiotics ? 'Recibe antibióticos actualmente? ' + Solicitations.conditionsYesNo[conditions.antibiotics] : '',
                prevTreatment: conditions.previousTreatment ? 'Tratamiento anterior: ' + Solicitations.conditionsYesNo[conditions.previousTreatment] : '',
                prevBiopsy: conditions.previousBiopsy ? 'Biopsia anterior: ' + Solicitations.conditionsYesNo[conditions.previousBiopsy] : '',
                comments: conditions.comments ? 'Observaciones: ' + conditions.comments : ''
            };
        };
        var getDeltaLines = function (old, current, mappingFn) {
            var lines = [];
            var oldLines = [];
            var currentLines = [];
            var oldMap = mappingFn(old);
            var currentMap = mappingFn(current);
            for (var k in oldMap) {
                if (oldMap[k] !== currentMap[k]) {
                    if (oldMap[k].length > 0)
                        oldLines.push('- ' + oldMap[k]);
                    if (currentMap[k].length > 0)
                        currentLines.push('- ' + currentMap[k]);
                }
            }
            if (oldLines.length > 0) {
                lines = _.union(['Anterior'], oldLines);
            }
            if (currentLines.length > 0) {
                lines = _.union(lines, ['Actual'], currentLines);
            }
            return lines;
        };
        var getExtraInfo = function (infoMap) {
            var lines = [];
            for (var k in infoMap) {
                if (infoMap[k].length > 0) {
                    lines.push(infoMap[k]);
                }
            }
            return lines;
        };
        var evolutionExtraInfo = function (evolution) {
            var evoMap = evolutionStringMap(evolution);
            var lines = [evoMap.description, evoMap.date];
            if (evolution.attachments) {
                lines.push(evoMap.attachments);
            }
            return lines;
        };
        $scope.anyContingency = function () {
            var ts = $scope.rawLogData;
            return typeof ts !== 'undefined' && ts.some(function (t) { return t.kind === 'contingency.created'; });
        };
        $scope.hasPrintAccess = UserInfo.hasPrintAccess();
    }]);
