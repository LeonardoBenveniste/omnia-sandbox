/// <reference path="../lib/moment.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../lib/angular.d.ts" />
/// <reference path="../services.d.ts" />
ehrControllers.controller('FlatTimelineController', ['$scope', '$rootScope', '$routeParams',
    'FlatTimeline', 'Evolutions', 'Alerts', 'TimelineUtils', 'Confirm', 'MedicalInfo', function ($scope, $rootScope, $routeParams, FlatTimeline, Evolutions, Alerts, TimelineUtils, Confirm, MedicalInfo) {
        // for now just don't render the entries. TODO: delete all presentation code.
        var renderableEntries = ['evolution.created', 'medicalInfo.closed', 'episode.closed', 'episode.current', 'contingency.created'];
        var currentYear = moment().month(0).date(0).hour(0).minute(0).second(0);
        var rawTimelineData = [];
        $scope.filters = {};
        var unbind = $rootScope.$on('timeline-filters-apply', function (event, data) {
            $scope.filters = data;
            $scope.redraw();
        });
        var timelineFormat = function (timeline) {
            var f = _.compose(TimelineUtils.filterByDateRange($scope.filters.start, $scope.filters.end), TimelineUtils.filterByEvolutionProblem($scope.filters.problem), TimelineUtils.filterByAuthor($scope.filters.author), TimelineUtils.appendHumanFormats, TimelineUtils.appendHumanDate, TimelineUtils.createdOrder, TimelineUtils.renderables(renderableEntries), TimelineUtils.filterPristine);
            return f(timeline);
        };
        $scope.currentYear = currentYear;
        $scope.redraw = function () {
            $scope.loading.timeline = true;
            FlatTimeline.getByEpisode($routeParams.id, $routeParams.episodeId).then(function (data) {
                rawTimelineData = insertClosedEpisodes(data.timeline);
                $scope.tl = timelineFormat(rawTimelineData);
            })['finally'](function () {
                $scope.loading.timeline = false;
            });
        };
        var insertClosedEpisodes = function (timeline) {
            if (+$routeParams.episodeId !== 0 || !$scope.patient) {
                return timeline;
            }
            var tl = angular.copy(timeline);
            _($scope.patient.episodes).each(function (e) {
                var kind = 'episode.current';
                if (e.end) {
                    kind = 'episode.closed';
                }
                var entry = {
                    'kind': kind,
                    'created': e.start,
                    'episode': e,
                    'day': TimelineUtils.unixDay(e.start),
                    'specialty': ''
                };
                tl.push(entry);
            });
            return tl;
        };
        $scope.episodeUrl = function (episode) {
            return "/patients/" + $routeParams.id + "/episodes/" + episode + "/evolutions";
        };
        $scope._patientLoaded.then(function () {
            $scope.isCurrentEpisode = $scope.currentEpisodeId === +$routeParams.episodeId;
            $scope.isOutpatient = +$routeParams.episodeId === 0;
            $scope.redraw();
        });
        var unixDay = function (when) {
            return Math.ceil(when / 1000 / 3600 / 24);
        };
        var addNewTlEntry = function (kind, domainObjectName, domainObject) {
            var entry = {
                author: { fullName: Omnia.currentUserName, id: Omnia.currentUserId, license: Omnia.currentUserLicense },
                created: Date.now(),
                day: unixDay(Date.now()),
                kind: kind,
                dirty: false,
                specialty: Omnia.currentSpecialty
            };
            entry[domainObjectName] = domainObject;
            rawTimelineData.push(entry);
            $scope.tl = timelineFormat(rawTimelineData);
        };
        var eventTypes = ['evolution.created', 'evolution.deleted', 'evolution.overriden',
            'medicalInfo.closed', 'medicalInfo.deleted', 'medicalInfo.edited'];
        _(eventTypes).each(function (eventType) {
            var unbind = $rootScope.$on(eventType, function (event, object) {
                $scope.redraw();
            });
            $scope.$on('$destroy', unbind);
        });
        $scope.isDeleted = function (ev) {
            return ev.overridenBy === -1;
        };
        $scope.isEdited = function (ev) {
            return ev.overridenBy && ev.overridenBy !== -1;
        };
        $scope.editEvolution = function (evolution) {
            $rootScope.$emit('edit.evolution', angular.copy(evolution));
            $scope.returnToTop();
        };
        $scope.deleteEvolution = function (evolution) {
            Confirm.show({ 'message': '¿Desea eliminar la evolución?' }).then(function () {
                Evolutions.remove(evolution).success(function (stuff) {
                    Alerts.success('Evolución eliminada');
                });
            });
        };
        $scope.deleteMedicalInfo = function (info) {
            Confirm.show({ 'message': '¿Desea eliminar la evolución?' }).then(function () {
                MedicalInfo.remove($routeParams.id, $routeParams.episodeId, info).success(function () {
                    Alerts.success('Evolución eliminada');
                    $rootScope.$emit('medicalInfo.deleted');
                });
            });
        };
        $scope.getSignature = function (e) {
            if (e.kind.indexOf('episode.') === 0) {
                return '';
            }
            else {
                return e.author.firstName + " " + e.author.lastName + " - " + e.author.license + " (" + e.specialty + ")";
            }
        };
        $scope.parseValue = function (jsonString) {
            return JSON.parse(jsonString);
        };
        $scope.getMedicalInfoTitle = MedicalInfo.getTitle;
        $scope.editMedicalInfo = function (info) {
            $rootScope.$emit('edit.medicalInfo', angular.copy(info));
            $scope.returnToTop();
        };
        $scope.openNewMedicalInfo = function (infoType) {
            $rootScope.$emit('new.medicalInfo', infoType);
        };
    }]);
