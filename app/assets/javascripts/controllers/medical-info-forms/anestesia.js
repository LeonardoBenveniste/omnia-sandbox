/// <reference path="../../lib/jquery.d.ts" />
/// <reference path="../../lib/underscore.d.ts" />
/// <reference path="../../lib/angular.d.ts" />
/// <reference path="../../model.d.ts" />
/// <reference path="../../services.d.ts" />
ehrControllers.controller('Anestesia', ['$scope', 'Problems', '$routeParams', '$interval',
    function ($scope, Problems, $routeParams, $interval) {
        /*
          Los pasos de la ficha. Son requeridos.

          nbr = Número (number) del paso de la ficha.
          title = Título del paso de la ficha
        */
        
        $scope.steps = [
            { 'nbr': 1, 'title': 'Ejemplo Paso 1' },
            { 'nbr': 2, 'title': 'Ejemplo Paso 2' }
        ];
        function crearProblemaDeEjemplo() {
            var existingProblems = [];
            return Problems.getOrCreate('Problema de ejemplo', 'active', existingProblems, $routeParams.id, $routeParams.episodeId);
        }
        /*
          Toda ficha está asociada a uno o más problemas, en este caso se genera
          uno de ejemplo.
        */
        var problem = crearProblemaDeEjemplo();
        $scope.data.problems = [problem];

        //Valida el formulario
        $scope.isValidForm = function(){
            if (($scope.data._1.selectedAnestesias && $scope.data._1.selectedAnestesias.length > 0) && $scope.data._1.selectedProcedimiento) return true;
            else return false;
        }
        //Valida el dialog de edicion
        $scope.isValidEdit = function(){
            if ((tempSelectedAnestesias && tempSelectedAnestesias.length > 0) && tempSelectedProcedimiento) return true;
            else return false;
        }

        //Step 1
        var roundMinutes = Math.round(moment().minute() / 5) * 5; //Redondea a los 5 minutos mas cercanos
        var defaultData = { 'date': moment().format('DD/MM/YYYY'), 'time': moment().minute(roundMinutes).format('HH:mm') };
        $scope.data._1 = $scope.data._1 || defaultData;

        var parte = {'id': '5343242'}; //Reemplazar por data dinamica
        $scope.data.parte = $scope.data.parte || parte;

        var procedimientos = [{'id': 1, 'name': 'Convencional'},
                                {'id': 2, 'name': 'Ambulatoria'},
                                {'id': 3, 'name': 'Alta complejidad'}]; //Reemplazar por data dinamica
        $scope.procedimientos = $scope.procedimientos || procedimientos;

        var anestesias = [{'id': 1, 'name': 'Local'},
                                {'id': 2, 'name': 'Regional'},
                                {'id': 3, 'name': 'Plexual'},
                                {'id': 4, 'name': 'General'}]; //Reemplazar por data dinamica
        $scope.anestesias = $scope.anestesias || anestesias;

        var selectedProcedimiento = {};
        $scope.selectProcedimiento = function ($proc){
            if (selectedProcedimiento != $proc.procedimiento.id) selectedProcedimiento = $proc.procedimiento;
            else selectedProcedimiento = '';
            $scope.data._1.selectedProcedimiento = selectedProcedimiento || $scope.data._1.selectedProcedimiento;
            $scope.persistData();
        };

        $scope.isProcedimientoSelected = function(id) {
            if(selectedProcedimiento) return selectedProcedimiento.id == id;
            else return;
        };

        var selectedAnestesias = [];
        $scope.selectAnestesia = function ($proc){
            var anestesiaExists = false;
            for (var i = 0; i < selectedAnestesias.length; i++) {
                if(selectedAnestesias[i].id == $proc.anestesia.id) {
                    anestesiaExists = true;
                    var anestesiaIndex = i;
                }
            }
            if (!anestesiaExists) selectedAnestesias.push($proc.anestesia);
            else selectedAnestesias.splice(anestesiaIndex, 1);
            $scope.data._1.selectedAnestesias = selectedAnestesias;
            $scope.persistData();
        };
        
        $scope.isAnestesiaSelected = function(anestesia) {
            if(selectedAnestesias) {
                for (var i = 0; i < selectedAnestesias.length; i++) {
                    if(selectedAnestesias[i].id == anestesia.id) return true;
                }
            }
            else return;
        };

        //step1
        $scope.$on('edit-medical-info', function (ev, data) {
            var value = JSON.parse(data.value)
            if(value._1.selectedAnestesias)
                if(value._1.selectedAnestesias.length > 0) $scope.next();
        });


        //step2
        var tempSelectedProcedimiento = {};
        var tempSelectedAnestesias = [];
        var tempControlMonitor = [];
        var tempFluids = [];
        var currentStartingTime = 0;
        var ISODate;
        
        //Monitor Default Data
        var fc = {'visible': false, 'id': 'fc', 'name': 'Frecuencia cardíaca', 'min': "0", 'max': "300", 'start': 75, 'step': "1"};
        var fr = {'visible': false, 'id': 'fr', 'name': 'Frecuencia Respiratoria', 'min': "0", 'max': "60", 'start': 22, 'step': "1"};
        var pa = {'visible': false, 'id': 'pa', 'name': 'Presion arterial', 'min': "0", 'max': "120", 'start': 80, 'step': "1", 'minH': 60, 'maxH': 300, 'startH': 120,};
        var saturacion = {'visible': false, 'id': 'saturacion', 'name': 'Saturacion O2', 'min': "0", 'max': "100", 'start': 100, 'step': "1"};
        var et = {'visible': false, 'id': 'et', 'name': 'ET O2', 'min': "0", 'max': "40", 'start': 30, 'step': "1"};
        var temperatura = {'visible': false, 'id': 'temperatura', 'name': 'Temperatura', 'min': "35", 'max': "42", 'start': 37, 'step': "0.1"};
        var pvc = {'visible': false, 'id': 'pvc', 'name': 'PVC', 'min': "-5", 'max': "20", 'start': 3, 'step': "1"};
        var pp = {'visible': false, 'id': 'pp', 'name': 'Presion pulmonar', 'min': "2", 'max': "40", 'start': 8, 'step': "1", 'minH': 10, 'maxH': 80, 'startH': 20,};
        var pw = {'visible': false, 'id': 'pw', 'name': 'Presion Wedge', 'min': "0", 'max': "40", 'start': 16, 'step': "1"};
        var ic = {'visible': false, 'id': 'ic', 'name': 'Indice cardíaco', 'min': "1", 'max': "5", 'start': 3, 'step': "0.1"};
        var vm = {'visible': false, 'id': 'vm', 'name': 'Volumen minuto', 'min': "2", 'max': "10", 'start': 5, 'step': "0.1"};
        var rvs = {'visible': false, 'id': 'rvs', 'name': 'RVS', 'min': "800", 'max': "2000", 'start': 1300, 'step': "10"};
        var rvp = {'visible': false, 'id': 'rvp', 'name': 'RVP', 'min': "50", 'max': "600", 'start': 200, 'step': "10"};
        var VT = {'visible': false, 'id': 'VT', 'name': 'VT', 'min': "6", 'max': "1000", 'start': 350, 'step': "1"};  
        var PEEP = {'visible': false, 'id': 'PEEP', 'name': 'PEEP', 'min': "0", 'max': "40", 'start': 5, 'step': "1"};  
        var FiO2 = {'visible': false, 'id': 'FiO2', 'name': 'FiO2', 'min': "21", 'max': "100", 'start': 100, 'step': "1"};  
        var Ti = {'visible': false, 'id': 'Ti', 'name': 'Tiempo inspiratorio', 'min': "0", 'max': "1.5", 'start': 1, 'step': "0.1"};  
        var Ppico = {'visible': false, 'id': 'Presión pico', 'name': 'P pico', 'min': "0", 'max': "100", 'start': 25, 'step': "1"};  
        var Pmedia = {'visible': false, 'id': 'Presión media', 'name': 'P media', 'min': "0", 'max': "40", 'start': 10, 'step': "1"};  
        var Pplateau = {'visible': false, 'id': 'Presión plateau', 'name': 'P plateau', 'min': "0", 'max': "40", 'start': 15, 'step': "1"};  
        var pc = {'visible': false, 'id': 'pc', 'name': 'Presión control', 'min': "5", 'max': "40", 'start': 15, 'step': "1"}; 
        //End Monitor Default Data

        $scope.$on('medical-info-before-step', (ev, context) => {
            if (context.step === 2 && $scope.info.status === 'open') {
                ISODate = moment($scope.data._1.date + ' ' + $scope.data._1.time, "DD/MM/YYYY HH:mm").toISOString();
                tempSelectedProcedimiento = $scope.data._1.selectedProcedimiento;
                tempSelectedAnestesias = $scope.data._1.selectedAnestesias.slice();
                var shownTimes = [];
                for (var i = 0; i < 11; i++){
                    shownTimes.push(moment(ISODate).add('m', (5 * i)).valueOf());
                }
                if(!$scope.data._2){
                    var defaultData2 = {};
                    defaultData2.shownTimes = shownTimes;
                    defaultData2.formatedDate = moment(ISODate).format('dddd, LL');
                    defaultData2.table = {};
                    $scope.data._2 = defaultData2;
                }
                else {
                    $scope.data._2.shownTimes = shownTimes;
                    $scope.data._2.formatedDate = moment(ISODate).format('dddd, LL');
                }
                if(!$scope.data._2.table.monitor) setTableMonitorData();
                setTableData();
                $scope.data._2.selectedAccesos = [];
                $scope.data._2.selectedAccesos.push({'data': {}});
                if(!$scope.data._2.diuresis) $scope.data._2.diuresis = [];
                if(!$scope.data._2.perdida) $scope.data._2.perdida = [];
                $scope.startCheckClass();   
            }
        })

        function setTableMonitorData(){
            var monitor = {};
            monitor.name = 'Monitoreo';
            monitor.data = [];
            
            switch ($scope.data._1.selectedProcedimiento.id){
                case 1: //Convencional
                    fc.visible = true;
                    fr.visible = true;
                    pa.visible = true;
                    saturacion.visible = true;
                    et.visible = true;
                    temperatura.visible = true;
                    break;
                case 2: //Ambulatoria
                    fc.visible = true;
                    pa.visible = true;
                    saturacion.visible = true;
                    break;
                case 3: //Alta complejidad
                    pvc.visible = true;
                    pp.visible = true;
                    pw.visible = true;
                    ic.visible = true;
                    vm.visible = true;
                    rvs.visible = true;
                    rvp.visible = true;
                    fc.visible = true;
                    fr.visible = true;
                    pa.visible = true;
                    saturacion.visible = true;
                    et.visible = true;
                    temperatura.visible = true;
                    break;
            }
            monitor.data.push(fc);
            monitor.data.push(fr);
            monitor.data.push(pa);
            monitor.data.push(saturacion);
            monitor.data.push(et);
            monitor.data.push(temperatura);
            monitor.data.push(pvc);
            monitor.data.push(pp);
            monitor.data.push(pw);
            monitor.data.push(ic);
            monitor.data.push(vm);
            monitor.data.push(rvs);
            monitor.data.push(rvp);
            monitor.data.push(VT);
            monitor.data.push(PEEP);
            monitor.data.push(FiO2);
            monitor.data.push(Ti);
            monitor.data.push(Ppico);
            monitor.data.push(Pmedia);
            monitor.data.push(Pplateau);
            monitor.data.push(pc);

            if($scope.data._2.table.monitor){
                for (var i = 0; i < monitor.data.length; i++) {
                    var monitorData = monitor.data[i];
                    if(monitorData.visible == true){
                        for (var j = 0; j < $scope.data._2.table.monitor.data.length; j++) {
                            var element = $scope.data._2.table.monitor.data[j];
                            if(element.id == monitorData.id) element.visible = true;
                        }
                    } 
                }
            }
            else $scope.data._2.table.monitor = monitor;
            $scope.persistData();
        }

        function setTableData(){
            var procedures = {};
            procedures.name = 'Procedimientos';
            procedures.categories = [{'id':'prep', 'name': 'Preparación y chequeo'},
                                     {'id':'sondas', 'name': 'Sondas'},
                                     {'id':'accesos', 'name': 'Accesos vasculares'},
                                     {'id':'vias', 'name': 'Vía aérea'},
                                     {'id':'punciones', 'name': 'Punciones anestésicas'}];
            procedures.categories.prep = [{'name': 'Conexión a monitor', 'state': true, 'data': [
                                                            {'name': 'ECG', 'state': true},
                                                            {'name': 'TA, TA invasiva', 'state': true},
                                                            {'name': 'Saturación de pulso', 'state': true},
                                                            {'name': 'Capnografía', 'state': true}]},
                                          {'name': 'Seteo de alarmas', 'state': true},
                                          {'name': 'Profilaxis ocular', 'state': true},
                                          {'name': 'Profilaxis de decúbitos', 'state': true},
                                          {'name': 'Chequeo de mesa anestésica', 'state': true}];
            procedures.categories.sondas = [{'name': 'Sonda nasoenteral', 'state': false},
                                            {'name': 'Sonda orogástrica', 'state': false},
                                            {'name': 'Sonda vesical', 'state': false}];
            procedures.categories.accesos = [{'previous': false, 'name': 'Arterial radial derecho'},
                                             {'previous': false, 'name': 'Arterial radial izquierdo'},
                                             {'previous': false, 'name': 'Arterial femoral derecho'},
                                             {'previous': false, 'name': 'Arterial femoral izquierdo'},
                                             {'previous': false, 'name': 'Arterial pedio derecho'},
                                             {'previous': false, 'name': 'Arterial pedio izquierdo'},
                                             {'previous': false, 'name': 'Venoso en mano derecha', 'cateter': ''},
                                             {'previous': false, 'name': 'Venoso en mano izquierda', 'cateter': ''},
                                             {'previous': false, 'name': 'Venoso en antebrazo derecho', 'cateter': ''},
                                             {'previous': false, 'name': 'Venoso en antebrazo izquierdo', 'cateter': ''},
                                             {'previous': false, 'name': 'Venoso central femoral derecho', 'lumenes': ''},
                                             {'previous': false, 'name': 'Venoso central femoral izquierdo', 'lumenes': ''},
                                             {'previous': false, 'name': 'Venoso central yugular anterior derecha', 'lumenes': ''},
                                             {'previous': false, 'name': 'Venoso central yugular anterior izquierda', 'lumenes': ''},
                                             {'previous': false, 'name': 'Venoso central yugular posterior derecha', 'lumenes': ''},
                                             {'previous': false, 'name': 'Venoso central yugular posterior izquierda', 'lumenes': ''},
                                             {'previous': false, 'name': 'Venoso central subclavia derecha', 'lumenes': ''},
                                             {'previous': false, 'name': 'Venoso central subclavia izquierda', 'lumenes': ''}];
            procedures.categories.vias = [{'name': 'Cánula nasal', 'state': false},
                                         {'name': 'Mascarilla facial', 'state': false},
                                         {'name': 'Máscara laríngea', 'state': false},
                                         {'name': 'Intubación Traqueal', 'state': false, 'data':[
                                             {'name': 'Via', 'value': 'Orotraqueal'},
                                             {'name': 'Número de tubo', 'value': '7.5'},
                                             {'name': 'Balón', 'value': 'Con balón'},
                                             {'name': 'Técnica', 'value': 'Convencional'},
                                             {'name': 'Resultado', 'value':'Satisfactorio', 'data':[
                                                {'name': 'Entrada de aire derecha', 'state': true},
                                                {'name': 'Entrada de aire izquierda', 'state': true},
                                             ]}
                                         ]},
                                         {'name': 'Fast track', 'state': false},
                                         ];
            procedures.categories.punciones = [{'name': 'Tipo de punción', 'state': true, 'value': 'Epidural'},
                                               {'name': 'Nivel', 'state': true, 'value': 'Torácico'},
                                               {'name': 'Aguja', 'state': true, 'value': '16 T'},
                                               {'name': 'Punción', 'state': true, 'value': 'Única'},
                                               {'name': 'Resultado', 'state': true, 'value': 'Satisfactorio'},
                                               {'name': 'Comentarios', 'state': true, 'value': ''}];
            var eventos = {};
            eventos.name = 'Eventos';
            eventos.tiempos = [{'id': 1, 'name': 'Inicio de la anestesia'},
                               {'id': 2, 'name': 'Fin de la anestesia'},
                               {'id': 3, 'name': 'Recuperación anestésica'},
                               {'id': 4, 'name': 'Inicio de la cirugía'},
                               {'id': 5, 'name': 'Fin de la cirugía'},
                               {'id': 6, 'name': 'Inicio Clampeo Aórtico'},
                               {'id': 7, 'name': 'Fin Clampeo Aórtico'},
                               {'id': 8, 'name': 'Inicio de circulación extracorpórea'},
                               {'id': 9, 'name': 'Fin de circulación extracorpórea'},
                               {'id': 10, 'name': 'Inicio de manguito hemostatico'},
                               {'id': 11, 'name': 'Fin de manguito hemostático'}];
            eventos.eventos = [{'id':21, 'name': 'Hemorragia masiva'},
                               {'id':22, 'name': 'Paro cardiaco intraoperatorio'},
                               {'id':23, 'name': 'Taquiarritmia'},
                               {'id':24, 'name': 'Bradiarritmia'},
                               {'id':25, 'name': 'Hipertermia maligna'},
                               {'id':26, 'name': 'Accidente cerebrovascular '},
                               {'id':27, 'name': 'Isquemia miocárdica intraoperatoria'},
                               {'id':28, 'name': 'Convulsiones'},
                               {'id':29, 'name': 'Anafilaxia'},
                               {'id':30, 'name': 'Intoxicación por anestésicos locales'},
                               {'id':31, 'name': 'Neumotorax'},
                               {'id':32, 'name': 'Otro evento'}];
            var fluids = {};
            fluids.name = 'Fluidos';
            fluids.data = [{'qty': 0, 'unit': 'unidades','name': 'Albúmina'},
                           {'qty': 0, 'unit': 'unidades','name': 'Bicarbonato de sodio'},
                           {'qty': 0, 'unit': 'unidades','name': 'ClNa 0.45%'},
                           {'qty': 0, 'unit': 'unidades','name': 'ClNa 0.9% (solución fisiológica)'},
                           {'qty': 0, 'unit': 'unidades','name': 'Concentrado de Plaquetas'},
                           {'qty': 0, 'unit': 'unidades','name': 'Crioprecipitados'},
                           {'qty': 0, 'unit': 'unidades','name': 'Dextran 40'},
                           {'qty': 0, 'unit': 'unidades','name': 'Dextran 70'},
                           {'qty': 0, 'unit': 'unidades','name': 'Dextrosa 10%'},
                           {'qty': 0, 'unit': 'unidades','name': 'Dextrosa 25%'},
                           {'qty': 0, 'unit': 'unidades','name': 'Dextrosa 5%'},
                           {'qty': 0, 'unit': 'unidades','name': 'Dextrosa 50%'},
                           {'qty': 0, 'unit': 'unidades','name': 'Gelafundin 4%'},
                           {'qty': 0, 'unit': 'unidades','name': 'Glóbulos Rojos Concentrados'},
                           {'qty': 0, 'unit': 'unidades','name': 'Glóbulos Rojos Filtrados'},
                           {'qty': 0, 'unit': 'unidades','name': 'Glóbulos Rojos Irradiados Y Filtrados'},
                           {'qty': 0, 'unit': 'unidades','name': 'Glóbulos Rojos Irradiados'},
                           {'qty': 0, 'unit': 'unidades','name': 'Glóbulos Rojos Lavados Filtrados'},
                           {'qty': 0, 'unit': 'unidades','name': 'Glóbulos Rojos Lavados Irradiados'},
                           {'qty': 0, 'unit': 'unidades','name': 'Glóbulos Rojos Lavados'},
                           {'qty': 0, 'unit': 'unidades','name': 'Haemaccel 3.5%'},
                           {'qty': 0, 'unit': 'unidades','name': 'Manitol'},
                           {'qty': 0, 'unit': 'unidades','name': 'Plaquetas (Dador único)'},
                           {'qty': 0, 'unit': 'unidades','name': 'Plaquetas Filtradas'},
                           {'qty': 0, 'unit': 'unidades','name': 'Plaquetas Irradiadas y Filtradas'},
                           {'qty': 0, 'unit': 'unidades','name': 'Plaquetas Irradiadas'},
                           {'qty': 0, 'unit': 'unidades','name': 'Plaquetas'},
                           {'qty': 0, 'unit': 'unidades','name': 'Plasma Fresco Congelado'},
                           {'qty': 0, 'unit': 'unidades','name': 'Ringer Lactato'},
                           {'qty': 0, 'unit': 'unidades','name': 'Sangre Autóloga'},
                           {'qty': 0, 'unit': 'unidades','name': 'Sangre Entera'},
                           {'qty': 0, 'unit': 'unidades','name': 'Voluven 6%'}]
            
            $scope.data._2.table.procedures = procedures;
            $scope.data._2.table.eventos = eventos;
            $scope.data._2.table.fluids = fluids;
            $scope.persistData();
        }

        $scope.backTimeBtn = function(){
            return currentStartingTime == 0
        }

        $scope.addEmptyAcceso = function(){
            $scope.data._2.selectedAccesos.push({'data': {}});
        }
        $scope.timeBck = function(){
            currentStartingTime += 1;
            var tempShownTimes = [];
            for (var i = 0; i < 11; i++){
                tempShownTimes.push(moment(ISODate).subtract('m', (currentStartingTime * 5)).add('m', (5 * i)).valueOf());
            }
            $scope.data._2.shownTimes = tempShownTimes;
            $scope.data._2.formatedDate = moment(tempShownTimes[0]).format('dddd DD [de] MMMM');
        }

        $scope.timeFwd = function(){
            currentStartingTime -= 1;
            var tempShownTimes = [];
            for (var i = 0; i < 11; i++){
                tempShownTimes.push(moment(ISODate).subtract('m', (currentStartingTime * 5)).add('m', (5 * i)).valueOf());
            }
            $scope.data._2.shownTimes = tempShownTimes;
            $scope.data._2.formatedDate = moment(tempShownTimes[0]).format('dddd DD [de] MMMM');
        }

        $scope.editData1 = function(){
            tempSelectedProcedimiento = $scope.data._1.selectedProcedimiento;
            tempSelectedAnestesias = $scope.data._1.selectedAnestesias.slice();
            document.getElementById('editAnestesiaDialog').showModal();
        }

        $scope.closeDialog = function(id){
            document.getElementById(id).close();
        }

        $scope.modifyAnestesiaData = function(){
            $scope.data._1.selectedProcedimiento = tempSelectedProcedimiento;
            $scope.data._1.selectedAnestesias = tempSelectedAnestesias;
            $scope.persistData();
            setTableMonitorData();
            document.getElementById('editAnestesiaDialog').close();
        }

        //Funciones duplicadas para edicion de datos de anestesia
        $scope.tempSelectProcedimiento = function ($proc){
            if (tempSelectedProcedimiento != $proc.procedimiento.id) tempSelectedProcedimiento = $proc.procedimiento;
            else tempSelectedProcedimiento = '';
        };

        $scope.isTempProcedimientoSelected = function(id) {
            if(tempSelectedProcedimiento) return tempSelectedProcedimiento.id == id;
            else return;
        };

        $scope.tempSelectAnestesia = function ($proc){
            var tempAnestesiaExists = false;
            for (var i = 0; i < tempSelectedAnestesias.length; i++) {
                if(tempSelectedAnestesias[i].id == $proc.anestesia.id) {
                    tempAnestesiaExists = true;
                    var anestesiaIndex = i;
                }
            }
            if (!tempAnestesiaExists) tempSelectedAnestesias.push($proc.anestesia);
            else tempSelectedAnestesias.splice(anestesiaIndex, 1);
        };
        
        $scope.isTempAnestesiaSelected = function(anestesia) {
            if(tempSelectedAnestesias) {
                for (var i = 0; i < tempSelectedAnestesias.length; i++) {
                    if(tempSelectedAnestesias[i].id == anestesia.id) return true;
                }
            }
            else return;
        };

        $scope.formatTime = function(time){
            return moment(time).format('HH:mm');
        }

        $scope.getMonitorData = function(time, id){
            var timestamp = moment(time);
            if($scope.data._2.monitor){
                var monitor = $scope.data._2.monitor;
                if(id != 'pa' && id != 'pp'){
                    for (var i = 0; i < monitor.length; i++) {
                        if(monitor[i].time == time) return monitor[i][id];
                    }
                    return ''
                }
                else {
                    for (var i = 0; i < monitor.length; i++) {
                        if(monitor[i].time == time && id == 'pa') return monitor[i]['pa1'];
                        if(monitor[i].time == time && id == 'pp') return monitor[i]['pp1'];
                    }
                    return ''
                }
            }
        }
        $scope.getP2Data = function(time, id){
            var timestamp = moment(time);
            if($scope.data._2.monitor){
                var monitor = $scope.data._2.monitor;
                for (var i = 0; i < monitor.length; i++) {
                    if(monitor[i].time == time && id == 'pa') return monitor[i]['pa2'];
                    if(monitor[i].time == time && id == 'pp') return monitor[i]['pp2'];
                }
                return ''
            }
        }

        function compareTime(a,b) {
            if (a.time < b.time)
                return -1;
            if (a.time > b.time)
                return 1;
            return 0;
        }

        $scope.setMonitorData = function(time, id, event){
            var value = parseInt(event.target.value);
            var min = parseInt(event.target.min);
            var max = parseInt(event.target.max);
            if(value < min || value > max) {
                event.target.classList.add('invalidMonitorData');
                return;
            }
            else event.target.classList.remove('invalidMonitorData');

            var timestamp = moment(time);
            if(!$scope.data._2.monitor) $scope.data._2.monitor = [];
            var monitor = $scope.data._2.monitor;
            if(id != 'pa' && id != 'pp'){
                for (var i = 0; i < monitor.length; i++) {
                    if(monitor[i].time == time) {
                        monitor[i][id] = event.target.value;
                        var timeExists = true;
                    }
                }
                if(!timeExists){
                    var newTimeStamp = {}
                    newTimeStamp.time = time;
                    newTimeStamp[id] = event.target.value;
                    monitor.push(newTimeStamp);
                }
            }
            else {
                var p1 = event.target.value;
                if(p1){
                    for (var i = 0; i < monitor.length; i++) {
                        if(monitor[i].time == time) {
                            if(id == 'pa') monitor[i].pa1 = p1;
                            if(id == 'pp') monitor[i].pp1 = p1;
                            var timeExists = true;
                        }
                    }
                    if(!timeExists){
                        var newTimeStamp = {}
                        newTimeStamp.time = time;
                        if(id == 'pa') newTimeStamp.pa1 = p1;
                        if(id == 'pp') newTimeStamp.pp1 = p1;
                        monitor.push(newTimeStamp);
                    }
                }
            }
            
            monitor.sort(compareTime);
            $scope.persistData();
        }
        $scope.setP2Data = function(time, id, event){
            var value = parseInt(event.target.value);
            var min = parseInt(event.target.min);
            var max = parseInt(event.target.max);
            if(value < min || value > max) {
                event.target.classList.add('invalidMonitorData');
                return;
            }
            else event.target.classList.remove('invalidMonitorData');

            var timestamp = moment(time);
            if(!$scope.data._2.monitor) $scope.data._2.monitor = [];
            var monitor = $scope.data._2.monitor;
            var p2 = event.target.value;
            if(p2){
                for (var i = 0; i < monitor.length; i++) {
                    if(monitor[i].time == time) {
                        if(id == 'pa') monitor[i].pa2 = p2;
                        if(id == 'pp') monitor[i].pp2 = p2;
                        var timeExists = true;
                    }
                }
                if(!timeExists){
                    var newTimeStamp = {}
                    newTimeStamp.time = time;
                    if(id == 'pa') monitor[i].pa2 = p2;
                    if(id == 'pp') monitor[i].pp2 = p2;
                    monitor.push(newTimeStamp);
                }
            }
            monitor.sort(compareTime);
            $scope.persistData();
        }

        $scope.setTimelineData = function(currentTime, data, section, event){
            if(!$scope.data._2[section]) $scope.data._2[section] = {};
            if(!$scope.data._2.timeline) $scope.data._2.timeline = {};
            var timeline = $scope.data._2.timeline;
            if (!timeline[currentTime]) timeline[currentTime] = [];
            var newTimelineInfo = {};
            //Procedures
            if(section == 'procedures'){
                var procedures = $scope.data._2.procedures;
                //Timeline
                if ($scope.categoryPrep == true) newTimelineInfo.type = 'Preparación y chequeo';
                else if ($scope.categorySondas == true) newTimelineInfo.type = 'Sondas';
                else if ($scope.categoryAccesos == true) newTimelineInfo.type = 'Accesos vasculares';
                else if ($scope.categoryVias == true) newTimelineInfo.type = 'Vía aérea';
                else if ($scope.categoryPunciones == true) newTimelineInfo.type = 'Punciones anestésicas';
                
                if($scope.categoryAccesos == true){
                    for (var i = 0; i < $scope.data._2.selectedAccesos.length; i++) {
                        var element = $scope.data._2.selectedAccesos[i];
                            if(Object.keys(element.data).length == 0) $scope.data._2.selectedAccesos.splice(i, 1);
                    }
                    newTimelineInfo.info = $scope.data._2.selectedAccesos;
                }
                else {
                    newTimelineInfo.info = $scope.data._2.table.procedures.categories[data.id];
                }

                for(var i = 0;i < timeline[currentTime].length; i++){
                    if(timeline[currentTime][i].type == newTimelineInfo.type) timeline[currentTime][i] = {};
                }
                timeline[currentTime].push(newTimelineInfo);
                //Intern
                if (!procedures[data.id]) procedures[data.id] = [];
                for (var i = 0; i < procedures[data.id].length; i++) {
                    if(procedures[data.id][i].time == currentTime) {
                        if($scope.categoryAccesos == true) procedures[data.id][i].data = $scope.data._2.selectedAccesos;
                        else procedures[data.id][i].data = $scope.data._2.table.procedures.categories[data.id];
                        var timeExists = true;
                    }
                }
                if(!timeExists){
                    var newTimeStamp = {}
                    newTimeStamp.time = currentTime;
                    if($scope.categoryAccesos == true) newTimeStamp.data = $scope.data._2.selectedAccesos;
                    else newTimeStamp.data = $scope.data._2.table.procedures.categories[data.id];
                    procedures[data.id].push(newTimeStamp);
                }
                procedures[data.id].sort(compareTime);
            }
            //Events
            else if(section == 'events'){
                var events = $scope.data._2.events;

                 //Timeline
                newTimelineInfo.type = data.name;
                newTimelineInfo.info = data.comments;
                for(var i = 0;i < timeline[currentTime].length; i++){
                    if(timeline[currentTime][i].type == newTimelineInfo.type) timeline[currentTime][i] = {};
                }
                timeline[currentTime].push(newTimelineInfo);

                //Intern
                if (!events[data.name]) events[data.name] = [];
                for (var i = 0; i < events[data.name].length; i++) {
                    var thisEvent = events[data.name][i];
                    if(thisEvent.time == currentTime) {
                        thisEvent.data = data;
                        var timeExists = true;
                    }
                }
                if(!timeExists){
                    var newTimeStamp = {}
                    newTimeStamp.time = currentTime;
                    newTimeStamp.data = data;
                    events[data.name].push(newTimeStamp);
                }
                events[data.name].sort(compareTime);
            }
            //Events
            else if(section == 'fluids'){
                var fluids = $scope.data._2.fluids;
                if(event){
                    var newData = {};
                    newData.name = data;
                    newData.qty = parseFloat(event.target.value);
                    data = newData;
                    var fullFluidName = data.name;
                }
                else var fullFluidName = data.name + '(' + data.unit + ')';
                
                 //Timeline
                newTimelineInfo.type = fullFluidName;
                newTimelineInfo.info = data.qty;
                for(var i = 0;i < timeline[currentTime].length; i++){
                    if(timeline[currentTime][i].type == newTimelineInfo.type) timeline[currentTime][i] = {};
                }
                timeline[currentTime].push(newTimelineInfo);

                //Intern
                if (!fluids[fullFluidName]) fluids[fullFluidName] = [];
                for (var i = 0; i < fluids[fullFluidName].length; i++) {
                    var thisFluid = fluids[fullFluidName][i];
                    if(thisFluid.time == currentTime) {
                        thisFluid.data = data.qty;
                        var timeExists = true;
                    }
                }
                if(!timeExists){
                    var newTimeStamp = {}
                    newTimeStamp.time = currentTime;
                    newTimeStamp.data = data.qty;
                    fluids[fullFluidName].push(newTimeStamp);
                }
                fluids[fullFluidName].sort(compareTime);
            }
            else if(section == 'diuresis'){
                var diuresis = $scope.data._2.diuresis;
                
                if(event){
                    var newData = {};
                    newData.diuresis = parseFloat(event.target.value);
                    data = newData;
                }
                 //Timeline
                newTimelineInfo.type = 'Diuresis (ml)';
                newTimelineInfo.info = data.diuresis;
                for(var i = 0;i < timeline[currentTime].length; i++){
                    if(timeline[currentTime][i].type == newTimelineInfo.type) timeline[currentTime][i] = {};
                }
                timeline[currentTime].push(newTimelineInfo);

                //Intern
                for (var i = 0; i < diuresis.length; i++) {
                    if(diuresis[i].time == currentTime) {
                        diuresis[i].diuresis = data.diuresis;
                        var timeExists = true;
                    }
                }
                if(!timeExists){
                    var newTimeStamp = {}
                    newTimeStamp.time = currentTime;
                    newTimeStamp.diuresis = data.diuresis;
                    diuresis.push(newTimeStamp);
                }
                diuresis.sort(compareTime);
            }
            else if(section == 'perdida'){
                var perdida = $scope.data._2.perdida;
                
                if(event){
                    var newData = {};
                    newData.perdida = parseFloat(event.target.value);
                    data = newData;
                }
                 //Timeline
                newTimelineInfo.type = 'Pérdida Hemática (ml)';
                newTimelineInfo.info = data.perdida;
                for(var i = 0;i < timeline[currentTime].length; i++){
                    if(timeline[currentTime][i].type == newTimelineInfo.type) timeline[currentTime][i] = {};
                }
                timeline[currentTime].push(newTimelineInfo);

                //Intern
                for (var i = 0; i < perdida.length; i++) {
                    if(perdida[i].time == currentTime) {
                        perdida[i].perdida = data.perdida;
                        var timeExists = true;
                    }
                }
                if(!timeExists){
                    var newTimeStamp = {}
                    newTimeStamp.time = currentTime;
                    newTimeStamp.perdida = data.perdida;
                    perdida.push(newTimeStamp);
                }
                perdida.sort(compareTime);
            }
            
            $scope.persistData();
        }

        $scope.getFluidData = function(time, fluid){
            var thisFluid = $scope.data._2.fluids[fluid];
            if(fluid){
                for (var i = 0; i < thisFluid.length; i++) {
                    if(thisFluid[i].time == time) {
                        return thisFluid[i].data;
                    }
                }
                return ''
            }
        }

        $scope.getDiuresisData = function(time){
            var diuresis = $scope.data._2.diuresis;
            if(diuresis){
                for (var i = 0; i < diuresis.length; i++) {
                    if(diuresis[i].time == time) {
                        return diuresis[i].diuresis;
                    }
                }
                return ''
            }
        }
        $scope.getPerdidaData = function(time){
            var perdida = $scope.data._2.perdida;
            if(perdida){
                for (var i = 0; i < perdida.length; i++) {
                    if(perdida[i].time == time) {
                        return perdida[i].perdida;
                    }
                }
                return ''
            }
        }

        $scope.editTimelineData = function(currentTime, category){
            if(!$scope.data._2.procedures) $scope.data._2.procedures = {};
            var procedures = $scope.data._2.procedures;

            if(!$scope.data._2.timeline) $scope.data._2.timeline = {};
            var timeline = $scope.data._2.timeline;

            
            var thisCategory = $scope.data._2.procedures[category.id];
            if(thisCategory){
                for (var i = 0; i < thisCategory.length; i++) {
                    if(thisCategory[i].time == currentTime) {
                        thisCategory[i].data = $scope.procedureToEdit;
                        break;
                    }
                }
            }
            var newTimelineInfo = {};
            if ($scope.categoryPrep == true) newTimelineInfo.type = 'Preparación y chequeo';
            else if ($scope.categorySondas == true) newTimelineInfo.type = 'Sondas';
            else if ($scope.categoryAccesos == true) newTimelineInfo.type = 'Accesos vasculares';
            else if ($scope.categoryVias == true) newTimelineInfo.type = 'Vía aérea';
            else if ($scope.categoryPunciones == true) newTimelineInfo.type = 'Punciones anestésicas';
            
            newTimelineInfo.info = procedures[category.id];

            for(var i = 0;i < timeline[currentTime].length; i++){
                if(timeline[currentTime][i].type == newTimelineInfo.type) timeline[currentTime][i] = newTimelineInfo;
            }

            procedures[category.id].sort(compareTime);
            
            $scope.persistData();
        }

        $scope.checkProcedureData = function(time, category){
            var thisCategory = $scope.data._2.procedures[category];
            if(category){
                for (var i = 0; i < thisCategory.length; i++) {
                    if(thisCategory[i].time == time) {
                        return true;
                    }
                }
                return false
            }
        }
        $scope.checkEventData = function(time, event){
            var thisEvent = $scope.data._2.events[event];
            if(event){
                for (var i = 0; i < thisEvent.length; i++) {
                    if(thisEvent[i].time == time) {
                        return true;
                    }
                }
                return false
            }
        }

        $scope.getProcedureData = function(time, category){
            var thisCategory = $scope.data._2.procedures[category];
            if(thisCategory){
                for (var i = 0; i < thisCategory.length; i++) {
                    if(thisCategory[i].time == time) {
                        return thisCategory[i].data;
                    }
                }
                return false
            }
        }
        $scope.getEventData = function(time, event){
            var thisEvent = $scope.data._2.events[event];
            if(event){
                for (var i = 0; i < thisEvent.length; i++) {
                    if(thisEvent[i].time == time) {
                        return thisEvent[i].data;
                    }
                }
                return false
            }
        }

        $scope.getTempProcedureData = function(time, category){
            var thisCategory = $scope.data._2.procedures[category].slice();
            if(thisCategory){
                for (var i = 0; i < thisCategory.length; i++) {
                    if(thisCategory[i].time == time) {
                        return thisCategory[i].data;
                    }
                }
                return false
            }
        }

        $scope.checkProcedureCat = function(category){
            if($scope.data._2.procedures){
                if($scope.data._2.procedures[category]){
                    return true
                }
                else return false
            }
            else return false
        }

        $scope.getProcedureTitle = function(category){
            switch (category){
                case 'prep':
                    return 'Preparación y chequeo';
                case 'sondas':
                    return 'Sondas';
                case 'accesos':
                    return 'Accesos vasculares';
                case 'vias':
                    return 'Vía aérea';
                case 'punciones':
                    return 'Punciones anestésicas';
                default:
                    return ''
            }
        }


        
        $scope.openEditProcedureDialog = function(time, category){
            $scope.categoryPrep = false;
            $scope.categorySondas = false;
            $scope.categoryAccesos = false;
            $scope.categoryVias = false;
            $scope.categoryPunciones = false;
            var selectCategory = {}
            switch (category) {
                case 'prep':
                    $scope.categoryPrep = true;
                    selectCategory.id = 'prep';
                    break;
                case 'sondas':
                    $scope.categorySondas = true;
                    selectCategory.id = 'sondas';
                    break;
                case 'accesos':
                    $scope.categoryAccesos = true;
                    selectCategory.id = 'accesos';
                    break;
                case 'vias':
                    $scope.categoryVias = true;
                    selectCategory.id = 'vias';
                    break;
                case 'punciones':
                    $scope.categoryPunciones = true;
                    selectCategory.id = 'punciones';
                    break;
                default:
                    break;
            }
            $scope.selectCategory(selectCategory);

            setTableData();
            $scope.editedProcedureTime = time;
            procedureToEdit = $scope.getProcedureData(time, category);
            var newprocedureToEdit = [];
            var selectedEditAccesos = [];
            angular.copy(procedureToEdit, newprocedureToEdit);
            $scope.procedureToEdit = newprocedureToEdit;
            document.getElementById('editProcedureDialog').showModal();
        }

        $scope.openProcedureDialog = function(time, category){
            if(!category) $scope.categoriesStepOne = true;
            else $scope.categoriesStepOne = false;
            $scope.categoryPrep = false;
            $scope.categorySondas = false;
            $scope.categoryAccesos = false;
            $scope.categoryVias = false;
            $scope.categoryPunciones = false;

            for (var i = 0; i < $scope.data._2.table.procedures.categories.length; i++){
                if($scope.data._2.table.procedures.categories[i].id == category) var selectedCategory = $scope.data._2.table.procedures.categories[i];
            }
            if(selectedCategory) {
                $scope.selectCategory(selectedCategory);
                if(selectedCategory.id == 'accesos') {
                    $scope.data._2.selectedAccesos = [];
                    $scope.addEmptyAcceso();
                }
            }
            setTableData();
            $scope.editedProcedureTime = time;
            document.getElementById('addProcedureDialog').showModal();
        }

        $scope.openEventDialog = function(time, event){
            if(!event){
                $scope.selectedEventStep2 = false;
                $scope.selectedEventCategory = 'tiempos';
            }
            else{
                var oldData = {};
                angular.copy($scope.getEventData(time, event), oldData);
                $scope.selectedEvent = {};
                if(oldData.comments) $scope.selectedEvent = oldData;
                else {
                    $scope.selectedEvent.name = event;
                    if(event == 'Recuperación anestésica') $scope.selectedEvent.lavado = true;
                }
                $scope.selectedEventStep2 = true;
            };
            $scope.editedEventTime = time;
            document.getElementById('addEventDialog').showModal();
        }

        $scope.selectEvent = function (event){
            $scope.selectedEvent = event;
            if(event.name == 'Recuperación anestésica') $scope.selectedEvent.lavado = true;
            $scope.selectedEventStep2 = true;
        }

        $scope.addEvent = function(currentTime){
            if(!$scope.data._2.events) $scope.data._2.events = {};
            var events = $scope.data._2.events;

            if(!$scope.data._2.timeline) $scope.data._2.timeline = {};
            var timeline = $scope.data._2.timeline;
            if (!timeline[currentTime]) timeline[currentTime] = [];
            var newTimelineInfo = {};
            newTimelineInfo = $scope.selectedEvent;
            newTimelineInfo.time = currentTime;
            
            timeline[currentTime].push(newTimelineInfo);
            if (!events[newTimelineInfo.name]) events[newTimelineInfo.name] = [];
            events[newTimelineInfo.name].push(newTimelineInfo);

            $scope.persistData();
        }

        $scope.checkEvent = function(key){
            return $scope.data._2.events[key];
        }

        $scope.openFluidsDialog = function(time){
            $scope.editedFluidTime = time;
            $scope.newFluidToEdit = {};
            $scope.newFluidToEdit.name = '';
            $scope.newFluidToEdit.qty = 0;
            $scope.newFluidToEdit.unit = 'unidades';
            document.getElementById('addFluidDialog').showModal();
        }

        $scope.openDeleteProcedureData = function(time, category){
            $scope.timeToDelete = time;
            $scope.categoryToDelete = category;
            document.getElementById('deleteShownDataDialog').showModal();
        }
        
        $scope.openDeleteSingleData = function(time, data, section){
            $scope.timeToDelete = time;
            $scope.itemToDelete = data;
            $scope.sectionToDelete = section;
            document.getElementById('deleteSingleDataDialog').showModal();
        }
        $scope.deleteSingleData = function(){
            var item = $scope.data._2[$scope.sectionToDelete][$scope.itemToDelete];
            for (var i = 0; i < item.length; i++) {
                var thisItem = item[i];
                if(thisItem.time == $scope.timeToDelete) item.splice(i, 1);
            }
            var timeData = $scope.data._2.timeline[$scope.timeToDelete];
            for (var i = 0; i < timeData.length; i++) {
                var element = timeData[i];
                if(element.type == $scope.itemToDelete) timeData.splice(i, 1);
            }
            if(timeData.length == 0) delete $scope.data._2.timeline[$scope.timeToDelete];
            $scope.persistData();
        }

        $scope.deleteShownData = function(){
            var cat = $scope.data._2.procedures[$scope.categoryToDelete];
            for(var i = 0; i < cat.length; i++){
                if(cat[i].time == $scope.timeToDelete) cat.splice(i, 1);
            }
            var timeData = $scope.data._2.timeline[$scope.timeToDelete];
            for (var i = 0; i < timeData.length; i++) {
                var element = timeData[i];
                switch($scope.categoryToDelete){
                    case 'prep':
                        if(element.type == 'Preparación y chequeo') timeData.splice(i, 1);
                        break;
                    case 'sondas':
                        if(element.type == 'Sondas') timeData.splice(i, 1);
                        break;
                    case 'accesos':
                        if(element.type == 'Accesos vasculares') timeData.splice(i, 1);
                        break;
                    case 'vias':
                        if(element.type == 'Vía aérea') timeData.splice(i, 1);
                        break;
                    case 'punciones':
                        if(element.type == 'Punciones anestésicas') timeData.splice(i, 1);
                        break;
                } 
                if(timeData.length == 0) delete $scope.data._2.timeline[$scope.timeToDelete];
                $scope.persistData();
            }
        }

        $scope.openMonitorDialog = function(){
            tempControlMonitor = [];
            var monitorData = $scope.data._2.table.monitor.data;
            for (var i = 0; i < monitorData.length; i++) {
                var control = monitorData[i];
                if(control.visible) tempControlMonitor.push(control);
            }
            $scope.tempControlMonitor = tempControlMonitor;
            document.getElementById('addMonitorDataDialog').showModal();
        }

        $scope.selectNewMonitorData = function(e, event){
            if(event.target.checked)
                tempControlMonitor.push(e.control);
            else {
                for (var j = 0; j < tempControlMonitor.length; j++) {
                    var tempControl = tempControlMonitor[j];
                    if(e.control.id == tempControl.id){
                        var index = j;
                        break;
                    }
                }
                tempControlMonitor.splice(index, 1);
            } 
        }

        $scope.selectNewFluidData = function(e, event){
            if(event.target.checked)
                tempFluids.push(e.fluid);
            else {
                for (var i = 0; i < tempFluids.length; i++) {
                    var temp = tempFluids[i];
                    if((e.fluid.name == temp.name) && (e.fluid.unit == temp.unit)){
                        var index = i;
                        break;
                    }
                }
                tempFluids.splice(index, 1);
            } 
        }

        $scope.openDeleteMonitorData = function(control){
            $scope.dataToDelete = control;
            document.getElementById('deleteMonitorDataDialog').showModal();
        }

        $scope.deleteMonitorData = function(control){
            for(var i = 0; i < $scope.data._2.monitor.length; i++){
                var data = $scope.data._2.monitor[i];
                if(data[control.id]) data[control.id] = '';
            }
            control.visible = false;
            $scope.persistData();
        }

        $scope.openDeleteData = function(key, value, section){
            $scope.sectionToDelete = section;
            $scope.keyToDelete = key;
            $scope.valueToDelete = value;
            document.getElementById('deleteDataDialog').showModal();
        }

        $scope.deleteData = function(key, value, section){
            for(var time in $scope.data._2.timeline){
                var timeData = $scope.data._2.timeline[time];
                for (var i = 0; i < timeData.length; i++) {
                    var element = timeData[i];
                    switch(key){
                        case 'prep':
                            if(element.type == 'Preparación y chequeo') timeData.splice(i, 1);
                            break;
                        case 'sondas':
                            if(element.type == 'Sondas') timeData.splice(i, 1);
                            break;
                        case 'accesos':
                            if(element.type == 'Accesos vasculares') timeData.splice(i, 1);
                            break;
                        case 'vias':
                            if(element.type == 'Vía aérea') timeData.splice(i, 1);
                            break;
                        case 'punciones':
                            if(element.type == 'Punciones anestésicas') timeData.splice(i, 1);
                            break;
                        default:
                            if(element.type == key) timeData.splice(i, 1);
                            break;
                    } 
                }
                if(timeData.length == 0) delete $scope.data._2.timeline[time];
            }
            delete $scope.data._2[section][key];
            $scope.persistData();
        }


        $scope.openDeleteMonitorData = function(control){
            $scope.dataToDelete = control;
            document.getElementById('deleteMonitorDataDialog').showModal();
        }
        
        $scope.deleteMonitorData = function(control){
            for(var i = 0; i < $scope.data._2.monitor.length; i++){
                var data = $scope.data._2.monitor[i];
                if(data[control.id]) data[control.id] = '';
            }
            control.visible = false;
            $scope.persistData();
        }
        
        $scope.copyFluidData = function(time, key){
            var shownTimes = $scope.data._2.shownTimes;
            var timeIndex = shownTimes.indexOf(time);
            fluids = $scope.data._2.fluids;
            var value = $scope.getFluidData(shownTimes[timeIndex - 1],key);
            var event = {};
            event.target = {};
            event.target.value = value;
            $scope.setTimelineData(time, key, 'fluids', event);
        }

        $scope.copyDiuresisData = function(time){
            var shownTimes = $scope.data._2.shownTimes;
            var timeIndex = shownTimes.indexOf(time);
            var diuresis = $scope.data._2.diuresis;
            var value = $scope.getDiuresisData(shownTimes[timeIndex - 1]);
            var event = {};
            event.target = {};
            event.target.value = value;
            $scope.setTimelineData(time, null, 'diuresis', event);
        }

        $scope.copyPerdidaData = function(time){
            var shownTimes = $scope.data._2.shownTimes;
            var timeIndex = shownTimes.indexOf(time);
            var perdida = $scope.data._2.perdida;
            var value = $scope.getPerdidaData(shownTimes[timeIndex - 1]);
            var event = {};
            event.target = {};
            event.target.value = value;
            $scope.setTimelineData(time, null, 'perdida', event);
        }

        $scope.copyData = function(time, id){
            var timeExists = false;
            var shownTimes = $scope.data._2.shownTimes;
            var timeIndex = shownTimes.indexOf(time);
            var monitor = [];
            monitor = $scope.data._2.monitor;
            if(monitor){
                for (var i = 0; i < monitor.length; i++) {
                    if(monitor[i].time == time) {
                        if(id == 'pa'){
                            monitor[i].pa1 = monitor[i - 1].pa1;
                            monitor[i].pa2 = monitor[i - 1].pa2;
                        }
                        else if(id == 'pp'){
                            monitor[i].pp1 = monitor[i - 1].pp1;
                            monitor[i].pp2 = monitor[i - 1].pp2;
                        }
                        else monitor[i][id] = monitor[i - 1][id];
                        timeExists = true;
                    }
                }
                if(!timeExists){
                    for (var i = 0; i < monitor.length; i++) {
                        if(monitor[i].time == shownTimes[timeIndex - 1]) {
                            var newTimeStamp = {};
                            newTimeStamp.time = time;
                            if(id == 'pa'){
                                newTimeStamp.pa1 = monitor[i].pa1;
                                newTimeStamp.pa2 = monitor[i].pa2;
                            }
                            else if(id == 'pp'){
                                newTimeStamp.pp1 = monitor[i].pp1;
                                newTimeStamp.pp2 = monitor[i].pp2;
                            }
                            else newTimeStamp[id] = monitor[i][id];
                            monitor.push(newTimeStamp);
                        }
                    }
                }
                monitor.sort(compareTime);
                $scope.persistData();

                //Force redraw table
                var table = [];
                angular.copy($scope.data._2.table.monitor.data, table);
                $scope.data._2.table.monitor.data = [];
                angular.copy(table, $scope.data._2.table.monitor.data);
            }
        }
        
        $scope.hoverOnDiuresisData = function(time){
            var diuresis = $scope.data._2.diuresis;
            if(diuresis){
                for (var i = 0; i < diuresis.length; i++) {
                    if(diuresis[i].time == time){
                        var data = diuresis[i].diuresis;
                    } 
                }
                if (!data) this.showCopy = true;
            }
        }

        $scope.hoverOnPerdidaData = function(time){
            var perdida = $scope.data._2.perdida;
            if(perdida){
                for (var i = 0; i < perdida.length; i++) {
                    if(perdida[i].time == time){
                        var data = perdida[i].perdida;
                    } 
                }
                if (!data) this.showCopy = true;
            }
        }

        $scope.hoverOnFluidData = function(time, fluid){
            var fluids = $scope.data._2.fluids;
            if(fluids){
                for (var i = 0; i < fluids.length; i++) {
                    if(fluids[i].time == time){
                        var data = fluids[i][fluid.name+fluid.unit];
                    } 
                }
                if (!data) this.showCopy = true;
            }
        }

        $scope.hoverOnShowData = function(time, id){
            var monitor = $scope.data._2.monitor;
            if(monitor){
                for (var i = 0; i < monitor.length; i++) {
                    if(monitor[i].time == time){
                        if(id == 'pa'){
                            var data = monitor[i].pa1;
                        }
                        else if(id == 'pp'){
                            var data = monitor[i].pp1;
                        }
                        else var data = monitor[i][id];
                    } 
                }
                if (!data) this.showCopy = true;
            }
        }

        $scope.hoverOffShowData = function(time, id){
            this.showCopy = false;
        }


        //Procedures
        $scope.selectCategory = function(category){
            switch(category.id){
                case 'prep':
                    $scope.categoryPrep = true;
                    break;
                case 'sondas':
                    $scope.categorySondas = true;
                    break;
                case 'accesos':
                    $scope.categoryAccesos = true;
                    break;
                case 'vias':
                    $scope.categoryVias = true;
                    break;
                case 'punciones':
                    $scope.categoryPunciones = true;
                    break;
            }
            $scope.categoriesStepOne = false;
            $scope.selectedCategory = category;
        }

        $scope.checkCurrentTime = function(time){
            var roundMinutes = Math.round(moment().minute() / 5) * 5; //Redondea a los 5 minutos mas cercanos
            var now = moment().minute(roundMinutes).seconds(0).format();
            var shown = moment(time).format();
            return now == shown;            
        }

        $scope.startCheckClass = function() {
            $interval(checkTimeClass, 60000);
        };

        var checkTimeClass = function(){
            var shownData = angular.element('.shownData');
            var roundMinutes = Math.round(moment().minute() / 5) * 5; //Redondea a los 5 minutos mas cercanos
            var now = moment().minute(roundMinutes).seconds(0).format();
            for (var i = 0; i < shownData.length; i++) {
                var element = shownData[i];
                var shown = moment(parseInt(element.dataset.time)).format();
                element.classList.remove('shownTimeNow');
                if(now == shown) {
                    element.classList.add('shownTimeNow');
                }
            }
        }
        $scope.validateData = function(){
            var invalidElements = document.getElementsByClassName('invalidMonitorData');
            if(invalidElements.length == 0) $scope.exit();
            else {
                document.getElementById('warnInvalidDataDialog').showModal();
            }
        }
    }]);