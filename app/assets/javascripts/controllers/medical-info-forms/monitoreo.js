/// <reference path="../../lib/angular.d.ts" />
/// <reference path="../../lib/underscore.d.ts" />
/// <reference path="../../lib/bootstrap.d.ts" />
/// <reference path="../../services.d.ts" />
ehrControllers.controller('Monitoreo', ['$scope', '$routeParams', 'Problems', '$rootScope', 'EcgPractices', 'Alerts', '$timeout', 'MedicalInfo',
    function ($scope, $routeParams, Problems, $rootScope, EcgPractices, Alerts, $timeout, MedicalInfo) {
        $scope.steps = [
            { 'nbr': 1, 'title': 'Antecedentes patológicos' },
            { 'nbr': 2, 'title': 'Medicación en ambulatorio' },
            { 'nbr': 3, 'title': 'Evalución preparatoria y Riesgo' },
            { 'nbr': 4, 'title': 'Monitoreo cardiológico' },
            { 'nbr': 5, 'title': 'Condición postprocedimiento' }
        ];
        $scope.data.problems = [];
        $scope.ecgRhythms = EcgPractices.rhythms;
        $scope.min = EcgPractices.min;
        $scope.max = EcgPractices.max;
        $scope.data._3 = $scope.data._3 || {};
        $scope.$watchCollection('[data._3.heartFreq, data._3.qtInterval]', function (values) {
            if (values.length === 2 && values[0] && values[1]) {
                var freq = values[0], qt = values[1];
                var rr = (60 / freq);
                var temp = Math.pow(rr, 1 / 3);
                $scope.data._3.qtCorrected = Math.round(qt / temp);
            }
            else {
                $scope.data._3.qtCorrected = undefined;
            }
        });
        $scope.updateErcScore = function () {
            var r = $scope.data._3.erc;
            if (r._1 || r._2 || r._3 || r._4) {
                $scope.data._3.riesgo1Modal = 'ALTO';
            }
            else if (r._5 || r._6 || r._7 || r._8 || r._9) {
                $scope.data._3.riesgo1Modal = 'INTERMEDIO';
            }
            else {
                $scope.data._3.riesgo1Modal = 'BAJO';
            }
        };
        $scope.updateRpScore = function () {
            var r = $scope.data._3.rp;
            if (r._1 || r._2 || r._3 || r._4 || r._5 || r._6) {
                $scope.data._3.riesgo2Modal = 'ALTO';
            }
            else if (r._7 || r._8 || r._9 || r._10 || r._11 || r._12 || r._13) {
                $scope.data._3.riesgo2Modal = 'INTERMEDIO';
            }
            else {
                $scope.data._3.riesgo2Modal = 'BAJO';
            }
        };
        $scope.alertClass = function (risk) {
            var types = {
                'ALTO': 'alert-danger',
                'INTERMEDIO': 'alert-warning',
                'BAJO': 'alert-info'
            };
            return types[risk];
        };
        var save = function () {
            Alerts.clearLast();
            $scope.saving = true;
            var info = angular.copy($scope.info);
            info.value = JSON.stringify($scope.data);
            return MedicalInfo.save($routeParams.id, $routeParams.episodeId, info, $scope.patient.currentLocation).then(function (mi) {
                $scope.saving = false;
                $scope.info = mi;
            });
        };
        $scope.attachments = function (element) { return $scope.stepAttachments(element, $scope.data._3); };
        $scope.cancelAttachments = function () { return $scope.cancelStepAttachments($scope.data._3); };
        var initStep1 = function () {
            Problems.getProblemsAndAllergiesForPatient($routeParams.id).then(function (problems) {
                if (!$scope.data._1) {
                    $scope.data._1 = {};
                    $scope.data._1.createdProblems = _(problems).map(Problems.addDisplayAttrs);
                    $scope.setAllChecks($scope.data._1, true);
                }
            });
        };
        var initStep5 = function () {
            Problems.getProblemsAndAllergiesForPatient($routeParams.id).then(function (problems) {
                var existingProblems = _(problems).map(Problems.addDisplayAttrs);
                var procedure = Problems.getOrCreate('Monitoreo cardiológico intraoperatorio', 'procedure', existingProblems, $routeParams.id, $routeParams.episodeId);
                $scope.data.problems = [procedure];
            });
        };
        $scope.showNewProblemModal = function () {
            $rootScope.$broadcast('problem-reset');
            $('.new-problem-modal-ingreso').modal('show');
        };
        var unbindCreated = $rootScope.$on('problem.created', function (event, problem) {
            $('.modal').modal('hide');
            if ($scope.step === 1) {
                $scope.data._1 = $scope.data._1 || {};
                $scope.data._1.createdProblems = $scope.data._1.createdProblems || [];
                problem._selected = true;
                $scope.data._1.createdProblems.push(problem);
                $scope.changeMarkedProblems($scope.data._1);
            }
        });
        $scope.$on('$destroy', unbindCreated);
        $scope.$on('medical-info-before-step', function (ev, context) {
            if (context.step === 1 && $scope.info.status === 'open') {
                initStep1();
            }
            if (context.step === 5 && $scope.info.status === 'open') {
                initStep5();
            }
        });
        var step3HasECGData = function (s) {
            return s.ecgText.length > 0 || s.imagesText.length > 0 || s.comments;
        };
        var step3HasData = function (s) {
            return step3HasECGData(s) || s.ventricularText.length > 0 || s.otrosEstudios;
        };
        $scope.$on('medical-info-after-step', function (ev, context) {
            if (context.step === 3) {
                $scope.setECGTextForEvolution($scope.data._3);
                $scope.data._3.hasData = step3HasData($scope.data._3);
                if ($scope.data._3.files && $scope.data._3.files.length > 0) {
                    $scope.uploadStepFiles($scope.data._3);
                }
            }
        });
    }]);
