/// <reference path="../../lib/jquery.d.ts" />
/// <reference path="../../lib/underscore.d.ts" />
/// <reference path="../../lib/angular.d.ts" />
/// <reference path="../../model.d.ts" />
/// <reference path="../../services.d.ts" />
ehrControllers.controller('SampleForm', ['$scope', 'Problems', '$routeParams',
    function ($scope, Problems, $routeParams) {
        /*
          Los pasos de la ficha. Son requeridos.
    
          nbr = Número (number) del paso de la ficha.
          title = Título del paso de la ficha
        */
        $scope.steps = [
            { 'nbr': 1, 'title': 'Ejemplo Paso 1' },
            { 'nbr': 2, 'title': 'Ejemplo Paso 2' }
        ];
        function crearProblemaDeEjemplo() {
            var existingProblems = [];
            return Problems.getOrCreate('Problema de ejemplo', 'active', existingProblems, $routeParams.id, $routeParams.episodeId);
        }
        /*
          Toda ficha está asociada a uno o más problemas, en este caso se genera
          uno de ejemplo.
        */
        var problem = crearProblemaDeEjemplo();
        $scope.data.problems = [problem];
    }]);
