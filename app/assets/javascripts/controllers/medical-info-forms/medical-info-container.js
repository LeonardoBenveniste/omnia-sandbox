/// <reference path="../../lib/jquery.d.ts" />
/// <reference path="../../lib/underscore.d.ts" />
/// <reference path="../../lib/angular.d.ts" />
/// <reference path="../../lib/bootstrap.d.ts" />
/// <reference path="../../lib/moment.d.ts" />
/// <reference path="../../model.d.ts" />
/// <reference path="../../services.d.ts" />
ehrControllers.controller('MedicalInfoContainer', ['$scope', '$rootScope', '$routeParams', '$http', '$q',
    'Alerts', 'MedicalInfo', 'Problems', '$timeout', 'S3', function ($scope, $rootScope, $routeParams, $http, $q, Alerts, MedicalInfo, Problems, $timeout, S3) {
        'use strict';
        $scope.data = $scope.data || {};
        $scope.step = 1;
        $scope.saving = false;
        $scope.editing = false;
        var goToNextStep = function () {
            $scope.step++;
            $scope.$broadcast('medical-info-before-step', { 'step': $scope.step });
        };
        $scope.previousStep = function () {
            $scope.$broadcast('medical-info-after-step', { 'step': $scope.step });
            $scope.step--;
            $scope.$broadcast('medical-info-after-step', { 'step': $scope.step });
            $scope.returnToTop();
        };
        $scope._patientLoaded.then(function () {
            $scope.isCurrentEpisode = $scope.currentEpisodeId === +$routeParams.episodeId;
            $scope.isOutpatient = +$routeParams.episodeId === 0;
        });
        // needed for progress bar display width.
        var progress = function (current, total) {
            return Math.round((current / total) * 100);
        };
        $scope.progress = progress;
        $scope.initTemplate = function (template) {
            $scope.activeTemplate = template;
            $scope.info = $scope.info || {
                'kind': template.name,
                'status': 'open'
            };
        };
        $scope.templateNameForStep = function (step) {
            var name = $scope.activeTemplate.name;
            return name + "/" + name + "-" + step + ".html";
        };
        $scope.titleForStep = function (step, steps) {
            return _(steps).findWhere({ 'nbr': step }).title;
        };
        var modalShow = function (modalName, opts) {
            var $elt = $("#" + modalName + "-modal");
            var deferred = $q.defer();
            $("#" + modalName + "-modal .title").html(opts.title);
            $("#" + modalName + "-modal .message").html(opts.message);
            $("#" + modalName + "-modal-ok").html(opts.ok);
            $("#" + modalName + "-modal-nok").html(opts.nok);
            $("#" + modalName + "-modal-ok").removeClass('btn-success btn-danger');
            $("#" + modalName + "-modal-ok").addClass(opts.okClass);
            $elt.modal('show');
            var confirmed = false;
            $("#" + modalName + "-modal-ok").on('click', function () {
                confirmed = true;
            });
            $("#" + modalName + "-modal-nok").on('click', function () {
                confirmed = false;
            });
            $elt.on('hidden.bs.modal', function (e) {
                if (confirmed) {
                    deferred.resolve(confirmed);
                }
                else {
                    deferred.reject(confirmed);
                }
            });
            return deferred.promise;
        };
        var closeForm = function () {
            $scope.step = 1;
            $scope.$emit('close-medical-info-form');
        };
        var confirmStatusUpdate = function (modalOpts, newStatus, alertText) {
            modalShow('medical-info', modalOpts).then(function () {
                MedicalInfo.updateStatus($routeParams.id, $routeParams.episodeId, newStatus, $scope.info, $scope.patient.currentLocation).success(function () {
                    Alerts.success(alertText);
                    $rootScope.$emit('medicalInfo.' + newStatus);
                    closeForm();
                });
            })['catch'](function () {
                closeForm();
            });
        };
        $scope.next = function () {
            $scope.$broadcast('medical-info-after-step', { 'step': $scope.step });
            if ($scope.info.status === 'open') {
                save().then(goToNextStep);
            }
            else {
                goToNextStep();
            }
            $scope.returnToTop();
        };
        var hasChanges = function () {
            var empty = _($scope.data).keys().length === 1 && _($scope.data).keys()[0] === 'problems';
            return !empty && $scope.originalData !== JSON.stringify($scope.data);
        };
        $scope.finish = function () {
            $scope.$broadcast('medical-info-after-step', { 'step': $scope.step });
            if ($scope.info.status === 'open') {
                saveAndFinish();
            }
            else {
                if (hasChanges()) {
                    // Some data was changed, so we save changes to closed medical-info
                    save().then(function () {
                        $rootScope.$emit('medicalInfo.edited');
                        Alerts.success('Evolución editada');
                        Omnia.trackEvent('Ficha editada: ' + $scope.activeTemplate.label, $scope.info);
                        closeForm();
                    });
                }
                else {
                    closeForm();
                }
            }
        };
        var saveAndFinish = function () {
            save().then(function () {
                var opts = {
                    title: $scope.activeTemplate.label,
                    message: '¿Desea finalizar ficha y generar una evolución con los datos ingresados?<br/>(Una vez cerrada podrá modificarla editando la evolución generada)',
                    ok: 'Finalizar y Evolucionar',
                    nok: 'No, dejar abierta la ficha para continuar más tarde',
                    okClass: 'btn-success'
                };
                confirmStatusUpdate(opts, 'closed', 'Evolución creada');
                Omnia.trackEvent('Ficha cerrada: ' + $scope.activeTemplate.label, $scope.info);
            });
        };
        $scope.exit = function () {
            $scope.$broadcast('medical-info-after-step', { 'step': $scope.step });
            if ($scope.info.status === 'open') {
                saveAndExit();
            }
            else {
                closeForm();
            }
        };
        var saveAndExit = function () {
            save().then(function () {
                var opts = {
                    title: $scope.activeTemplate.label,
                    message: 'Está a punto de salir de esta ficha, ¿qué desea hacer con los datos ingresados?',
                    ok: 'Eliminar esta ficha',
                    nok: 'Dejar abierta la ficha para continuar más tarde',
                    okClass: 'btn-danger'
                };
                confirmStatusUpdate(opts, 'deleted', 'Ficha eliminada');
            });
        };
        var save = function () {
            Alerts.clearLast();
            $scope.saving = true;
            var info = angular.copy($scope.info);
            console.log('Guardando datos de ficha en paso ' + $scope.step);
            console.log($scope.data);
            info.value = angular.toJson($scope.data);
            return MedicalInfo.save($routeParams.id, $routeParams.episodeId, info, $scope.patient.currentLocation).then(function (mi) {
                $scope.saving = false;
                $scope.info = mi;
            });
        };
        $scope.persistData = function () {
            if ($scope.info.status === 'open') {
                save();
            }
        };
        $scope.setActiveForm = function (form) {
            $scope.activeStepForm = form;
        };
        $scope.getActiveForm = function () { return $scope.activeStepForm; };
        $scope.$on('edit-medical-info', function (ev, info) {
            $scope.data = angular.fromJson(info.value);
            $scope.info = info;
            $scope.originalData = info.value;
            $scope.editing = true;
        });
        Problems.getProblemsAndAllergiesForPatient($routeParams.id).then(function (problems) {
            $scope.allProblems = _(problems).map(Problems.addDisplayAttrs);
            $scope.activeProblems = _($scope.allProblems).filter(function (p) { return p.state === 'active'; });
            $scope.$broadcast('medical-info-before-step', { 'step': 1 });
        });
        $scope.selectableProblems = [];
        $scope.selectedProblems = [];
        $scope.selected = function (token) {
            return $scope.selectedProblems.indexOf(token) >= 0;
        };
        $scope.toggleProblem = function (token) {
            if ($scope.selected(token)) {
                $scope.selectedProblems = _($scope.selectedProblems).reject(function (it) { return it === token; });
            }
            else {
                $scope.selectedProblems.push(token);
            }
        };
        $scope.solveProblem = function (problem) {
            if (problem.state !== 'solved') {
                var json = angular.copy(problem);
                json.state = 'solved';
                json.ended = Date.now();
                return Problems.save(json, { 'patientId': $routeParams.id, 'episodeId': $routeParams.episodeId }).then(function () {
                    problem.state = 'solved';
                    return json;
                });
            }
        };
        $scope.tomorrow = function () {
            return moment().add('days', 1).format('DD-MM-YYYY');
        };
        $scope.conciliateProblem = function (problem) {
            Problems.conciliate($routeParams.id, problem.id, $routeParams.episodeId);
        };
        $scope.stepAttachments = function (element, stepData) {
            var files = (element.files || element);
            var MAX_SIZE = 1024 * 1024 * 2.5;
            if (files && files.length && files.length > 0) {
                Alerts.clearLast();
                if (files[0].size > MAX_SIZE) {
                    Alerts.error('El tamaño del archivo adjunto no puede superar los 2.5 Mb.');
                }
                else {
                    stepData.files = files;
                    stepData.fileNames = _(files).pluck('name');
                    stepData.attached = true;
                }
            }
            $timeout(function () {
                $scope.$apply();
            });
        };
        $scope.uploadStepFiles = function (stepData) {
            S3.upload(stepData.files[0]).then(function (response) {
                var url = response.headers('location');
                stepData.attachments = [];
                angular.forEach(stepData.files, function (file) {
                    stepData.attachments.push({ 'kind': 'ecg', 'name': file.name, 'url': url });
                    stepData.hasData = true;
                });
                save();
            })['catch'](function () {
                Alerts.error('Error adjuntando archivo');
            });
        };
        $scope.cancelStepAttachments = function (stepData) {
            stepData.attached = false;
            stepData.files = [];
            stepData.fileNames = [];
        };
        //ECG Evolution Helpers
        var ecgValues = [
            { name: 'rhythm', label: 'Ritmo', unit: '' },
            { name: 'heartFreq', label: 'Frec. cardíaca', unit: 'lpm' },
            { name: 'pWave', label: 'Duración onda P', unit: 'mseg' },
            { name: 'prInterval', label: 'Intervalo PR', unit: 'mseg' },
            { name: 'qrsDuration', label: 'Duración QRS', unit: 'mseg' },
            { name: 'qrsAxis', label: 'Eje QRS', unit: 'grados' },
            { name: 'qtInterval', label: 'Intervalo QT', unit: 'mseg' },
            { name: 'qtCorrected', label: 'QT Corregido', unit: 'mseg' }
        ];
        var imageLabels = [
            { name: 'bri', label: 'bloqueo de rama izquierda' },
            { name: 'brd', label: 'bloqueo de rama derecha' },
            { name: 'hai', label: 'hemibloqueo anterior izquierdo' },
            { name: 'hpi', label: 'hemibloqueo posterior izquierdo' }
        ];
        var getEcgText = function (ecg) {
            var items = [];
            _.each(ecgValues, function (v) {
                if (ecg[v.name]) {
                    items.push(v.label + ": " + ecg[v.name] + " " + v.unit);
                }
            });
            return items.join(', ');
        };
        var getImagesText = function (ecg) {
            var items = [];
            _.each(imageLabels, function (v) {
                if (ecg[v.name]) {
                    items.push(v.label);
                }
            });
            return items.join(', ');
        };
        var getVentricularText = function (v) {
            var text = v.fnVentricular && v.fnVentricular !== 'No evaluada' ? v.fnVentricular : '';
            text += v.evaluadaPor ? '. Evaluada por: ' + v.evaluadaPor : '';
            return text;
        };
        $scope.setECGTextForEvolution = function (stepData) {
            stepData.ecgText = getEcgText(stepData);
            stepData.imagesText = getImagesText(stepData);
            stepData.ventricularText = getVentricularText(stepData);
        };
        $scope.changeMarkedProblems = function (stepData) {
            stepData.markedProblems = _(stepData.createdProblems).filter(function (p) { return p._selected; });
        };
        $scope.setAllChecks = function (stepData, value) {
            angular.forEach(stepData.createdProblems, function (item) {
                item._selected = value;
            });
            $scope.changeMarkedProblems(stepData);
        };
    }]);
