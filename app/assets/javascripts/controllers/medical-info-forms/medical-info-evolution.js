/// <reference path="../../model.d.ts" />
/// <reference path="../../services.d.ts" />
ehrControllers.controller('MedicalInfoEvolution', ['$scope', '$interpolate', 'MedicalInfo', '$http', '$templateCache', '$element',
    function ($scope, $interpolate, MedicalInfo, $http, $templateCache, $element) {
        $scope.init = function (val) {
            $scope.data = JSON.parse(val);
            var baseUrl = '/assets/templates/medical-info-evolutions/';
            if ($scope.data.printableTemplate) {
                $http.get(baseUrl + $scope.data.printableTemplate, { cache: $templateCache }).then(function (response) {
                    var json = { 'data': $scope.data, 'helpers': $scope.helpers };
                    var html = $interpolate(response.data)(json);
                    $scope.printableVersion = html;
                });
            }
        };
        $scope.helpers = MedicalInfo.getEvolutionHelpers();
        $scope.print = function () {
            window.open('about:blank', 'pdf_form', 'menubar=0, scrollbar=0, location=0, status=0, toolbar=0, titlebar=0');
            $element.find('#openpdf').click();
        };
        $scope.renderChart = function (data, containerClass) {
            if (!data) {
                return;
            }
            $element.find("." + containerClass).highcharts(data);
        };
    }]);
