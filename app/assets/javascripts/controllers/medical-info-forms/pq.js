/// <reference path="../../lib/angular.d.ts" />
/// <reference path="../../lib/underscore.d.ts" />
/// <reference path="../../lib/bootstrap.d.ts" />
/// <reference path="../../services.d.ts" />
ehrControllers.controller('Pq', ['$scope', '$routeParams', 'Problems', '$rootScope',
    function ($scope, $routeParams, Problems, $rootScope) {
        $scope.steps = [
            { 'nbr': 1, 'title': 'Checklist de entrada (antes de la inducción anestésica)' },
            { 'nbr': 2, 'title': 'Equipo Quirúrgico' },
            { 'nbr': 3, 'title': 'Pre Operatorio' },
            { 'nbr': 4, 'title': 'Intervención' },
            { 'nbr': 5, 'title': 'Post operatorio' }
        ];
        $scope.data.problems = [];
        $scope.data.printableTemplate = 'pq.html';
        var initStep1 = function () {
            $scope.allergies = _($scope.allProblems).filter(function (p) { return p.state === 'allergy'; });
        };
        var unbind = $rootScope.$on('problem.created', function (event, problem) {
            $scope.data._4 = $scope.data._4 || {};
            if (problem.state === 'allergy') {
                $scope.allergies.push(problem);
            }
            else if (problem.state === 'procedure') {
                $scope.data._4.procedimientos = $scope.data._4.procedimientos || [];
                $scope.data._4.procedimientos.push(problem);
                $scope.data.problems.push(problem);
            }
            else if (problem.state === 'active') {
                $scope.data._4.diagnostico = $scope.data._4.diagnostico || [];
                $scope.data._4.diagnostico.push(problem);
            }
        });
        $scope.$on('$destroy', unbind);
        $scope.$on('medical-info-before-step', function (ev, context) {
            if (context.step === 1) {
                initStep1();
            }
        });
        $scope.showNewProblemModal = function (state) {
            $scope.pq4modalTitle = (state === 'procedure') ? 'Procedimiento' : 'Problema';
            $rootScope.$broadcast('problem-reset', { initialState: state });
            $('.new-problem-modal-pq').modal('show');
        };
        $scope.showNewMemberModal = function () {
            $('.new-member-modal-pq').modal('show');
        };
        $scope.data._2 = $scope.data._2 || {};
        $scope.data._2.team = [
            { role: 'Cirujano', name: '' },
            { role: 'Asistente', name: '' },
            { role: 'Instrumentación', name: '' },
            { role: 'Anestesiólogo', name: '' }
        ];
        $scope.closeNewMemberModal = function () {
            if ($scope.data._2) {
                $scope.data._2.newMemberRole = '';
                $scope.data._2.newMemberName = '';
            }
            $('.new-member-modal-pq').modal('hide');
        };
        $scope.addMember = function (role, name) {
            $scope.data._2.team.push({ 'role': role, 'name': name });
            $scope.closeNewMemberModal();
        };
    }]);
