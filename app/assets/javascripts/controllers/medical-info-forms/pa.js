/// <reference path="../../lib/angular.d.ts" />
/// <reference path="../../lib/underscore.d.ts" />
/// <reference path="../../lib/bootstrap.d.ts" />
/// <reference path="../../lib/moment.d.ts" />
/// <reference path="../../services.d.ts" />
ehrControllers.controller('Pa', ['$scope', '$routeParams', 'Problems', '$rootScope', 'EcgPractices', 'Practices', '$timeout', 'Confirm',
    function ($scope, $routeParams, Problems, $rootScope, EcgPractices, Practices, $timeout, Confirm) {
        $scope.steps = [
            { 'nbr': 1, 'title': 'Evaluación preanestésica - Antecedentes patológicos' },
            { 'nbr': 2, 'title': 'Evaluación preanestésica - Medicación habitual' },
            { 'nbr': 3, 'title': 'Evaluación preanestésica - Examen físico' },
            { 'nbr': 4, 'title': 'Evaluacion preanestésica - Estudios complementarios' },
            { 'nbr': 5, 'title': 'Preparación' },
            { 'nbr': 6, 'title': 'Intervención' },
            { 'nbr': 7, 'title': 'Post operatorio' }
        ];
        $scope.data.problems = [];
        $scope.data.printableTemplate = 'pa.html';
        $scope.$on('medical-info-before-step', function (ev, context) {
            if (context.step === 1 && $scope.info.status === 'open') {
                initStep1();
            }
            if (context.step === 4) {
                loadLabValues();
            }
            if (context.step === 7 && $scope.info.status === 'open') {
                initStep7();
            }
        });
        $scope.$on('medical-info-after-step', function (ev, context) {
            if (context.step === 4) {
                $scope.setECGTextForEvolution($scope.data._4);
                $scope.data._4.labAsJson = $scope.data_4_lab;
                $scope.data._4.labItems = $scope.labDescriptionItems;
                $scope.data._4.labAsText = _.isEmpty($scope.data._4.labAsText) ? labResultsToText($scope.data_4_lab) : $scope.data._4.labAsText;
                if ($scope.data._4.files && $scope.data._4.files.length > 0) {
                    $scope.uploadStepFiles($scope.data._4);
                }
            }
            if (context.step === 5) {
                step5Completed();
            }
            if (context.step === 6) {
                step6Completed();
            }
        });
        // Step 1 Begin
        var initStep1 = function () {
            Problems.getProblemsAndAllergiesForPatient($routeParams.id).then(function (problems) {
                if (!$scope.data._1) {
                    $scope.data._1 = {};
                    $scope.data._1.createdProblems = _(problems).map(Problems.addDisplayAttrs);
                    $scope.setAllChecks($scope.data._1, true);
                }
            });
        };
        $scope.showNewProblemModal = function () {
            $rootScope.$broadcast('problem-reset');
            $('.new-problem-modal-pa').modal('show');
        };
        var unbindCreated = $rootScope.$on('problem.created', function (event, problem) {
            $('.modal').modal('hide');
            if ($scope.step === 1) {
                $scope.data._1 = $scope.data._1 || {};
                $scope.data._1.createdProblems = $scope.data._1.createdProblems || [];
                problem._selected = true;
                $scope.data._1.createdProblems.push(problem);
                $scope.changeMarkedProblems($scope.data._1);
            }
        });
        $scope.$on('$destroy', unbindCreated);
        // Step 1 End
        // Step 2 and 3 don't have js controller logic
        // Step 4 Begin
        $scope.ecgRhythms = EcgPractices.rhythms;
        $scope.min = EcgPractices.min;
        $scope.max = EcgPractices.max;
        $scope.data._4 = $scope.data._4 || {};
        $scope.$watchCollection('[data._4.heartFreq, data._4.qtInterval]', function (values) {
            if (values.length === 2 && values[0] && values[1]) {
                var freq = values[0], qt = values[1];
                var rr = (60 / freq);
                var temp = Math.pow(rr, 1 / 3);
                $scope.data._4.qtCorrected = Math.round(qt / temp);
            }
            else {
                $scope.data._4.qtCorrected = undefined;
            }
        });
        $scope.attachments = function (element) { return $scope.stepAttachments(element, $scope.data._4); };
        $scope.cancelAttachments = function () { return $scope.cancelStepAttachments($scope.data._4); };
        var labResultsToText = function (json) {
            var fragments = [];
            for (var k in json) {
                var item = _.findWhere($scope.labDescriptionItems, { pid: k });
                fragments.push(item['name'] + " (" + item['extra'] + "): " + json[k]);
            }
            return fragments.join(', ');
        };
        var defaultLabDescriptionItems = [
            { 'name': 'Hematocrito', 'pid': '101', 'extra': 'en sangre' },
            { 'name': 'Hemoglobina', 'pid': '102', 'extra': 'en sangre' },
            { 'name': 'Plaquetas', 'pid': '106', 'extra': 'en sangre' },
            { 'name': 'Tiempo de trombina', 'pid': '407', 'extra': 'en sangre' },
            { 'name': 'Tiempo de tromboplastina parcial', 'pid': '403', 'extra': 'en sangre' }
        ];
        $scope.loadDefaults = function () {
            initLab();
            $scope.labDescriptionItems = defaultLabDescriptionItems;
        };
        var initLab = function () {
            $scope.data_4_lab = {};
        };
        var loadLabValues = function () {
            if ($scope.data._4.labItems && !_.isEmpty($scope.data._4.labAsJson)) {
                $scope.labDescriptionItems = $scope.data._4.labItems;
                $scope.data_4_lab = $scope.data._4.labAsJson;
                $scope.setResultFormat('json');
            }
            else if ($scope.data._4.labAsText && $scope.data._4.labAsText.length > 0) {
                $scope.setResultFormat('free');
                $scope.loadDefaults();
            }
            else {
                $scope.loadDefaults();
                $scope.setResultFormat('json');
            }
        };
        $scope.showLabItemModal = function () {
            $('#labitems-modal').modal('show');
        };
        var buildItems = function (practices) {
            return _(practices).map(function (p) {
                return { 'name': p.label, 'pid': p.pid, 'extra': p.extra };
            });
        };
        var setLabDescriptionItems = function (practices) {
            if (practices.length > 0) {
                // collect simple practices.
                var simple = practices.filter(function (p) { return !Practices.isGroup(p.pid); });
                var items = buildItems(simple);
                // retrieve grouped practices.
                var groups = practices.filter(function (p) { return Practices.isGroup(p.pid); });
                var grouped = groups.reduce(function (all, p) { return all.concat(Practices.getPracticesForGroup(p.pid)); }, []);
                if (grouped.length > 0) {
                    Practices.getPracticesByIds('lab', grouped).success(function (ps) {
                        var groupPractices = buildItems(ps);
                        var all = groupPractices.concat(items);
                        $scope.labDescriptionItems = $scope.labDescriptionItems.concat(all);
                    });
                }
                else {
                    $scope.labDescriptionItems = $scope.labDescriptionItems.concat(items);
                }
            }
        };
        $scope.addLabItems = function () {
            var practices = $('#additional-lab-practices')['select2']('data');
            setLabDescriptionItems(practices);
            $('#additional-lab-practices')['select2']('val', '');
        };
        $scope.clear = function () {
            if (confirm('¿Quitar practicas cargadas?')) {
                initLab();
                $scope.labDescriptionItems = [];
            }
        };
        $scope.setResultFormat = function (format) {
            $scope.resultFormat = format;
            if (format === 'json') {
                $scope.data._4.labAsText = '';
            }
            else {
                $scope.data_4_lab = {};
            }
        };
        $scope.removeLabItem = function (pid, event) {
            var e = event.target;
            var item = $(e).parents('.description-item');
            $(item).addClass('removing-item');
            $timeout(function (e) {
                $scope.labDescriptionItems = _($scope.labDescriptionItems).reject(function (item) { return item.pid === pid; });
                delete $scope.data_4_lab[pid];
            }, 300);
        };
        // Step 4 End
        // Step 5 Begin
        $scope.showNewAccessModal = function () {
            $scope.pa_5_accesos = [{ 'tipo': '', 'localizacion': '' }];
            $('.new-access-modal').modal('show');
        };
        $scope.addAccessRow = function (accessList, index) {
            if ($scope.canAddAccessRow(accessList[index])) {
                accessList.push({ 'tipo': '', 'localizacion': '' });
            }
        };
        $scope.removeAccessRow = function (accessList, index) {
            accessList.splice(index, 1);
        };
        $scope.canAddAccessRow = function (access) {
            return access && (!_.isEmpty(access.tipo) && !_.isEmpty(access.localizacion));
        };
        $scope.data._5 = $scope.data._5 || {};
        $scope.data._5.accesos = $scope.data._5.accesos || [];
        $scope.saveAccess = function (accessList) {
            $scope.data._5.accesos = $scope.data._5.accesos.concat(accessList);
        };
        var anesthesiaEvolutionText = function (data) {
            var miniFormAsText = function (form) { return _.chain(form).keys().reject(function (k) { return k === 'checked'; }).map(function (k) { return (k + ": " + form[k]); }).value().join(', '); };
            var lines = [];
            var types = ['local', 'regional', 'peridural', 'subaracnoidea', 'plexual'];
            _.each(types, function (t) {
                if (data[t] && data[t].checked) {
                    lines.push({ tipo: t, val: miniFormAsText(data[t]) });
                }
            });
            return lines;
        };
        var sedationEvolutionText = function (data) {
            var types = {
                'canula': 'cánula nasal',
                'mascarillaFacial': 'mascarilla facial'
            };
            var using = _.chain(types).keys().filter(function (k) { return data[k]; }).map(function (k) { return types[k]; }).value();
            return using.length > 0 ? ", utilizando " + using.join(', ') : '';
        };
        var generalEvolutionText = function (data) {
            var types = {
                'circular': 'circuito circular',
                'asistencia': 'asistencia ventilatoria mecánica'
            };
            var using = _.chain(types).keys().filter(function (k) { return data[k]; }).map(function (k) { return types[k]; }).value();
            return using.length > 0 ? ", utilizando " + using.join(', ') : '';
        };
        var assistanceEvolutionText = function (data) {
            var withNumber = function (n) { return data[n] ? "(n\u00FAmero: " + data[n] + ")" : ''; };
            var checks = {
                'preoxigenacion': 'se realizó preoxigenación',
                'secuencia': 'se realizó secuencia rápida de intubación con presión cricoidea',
                'tuboComun': "se utiliz\u00F3 tubo endotraqueal com\u00FAn " + withNumber('tuboComunNro'),
                'tuboEspiralado': "se utiliz\u00F3 tubo endotraqueal espiralado " + withNumber('tuboEspiraladoNro'),
                'tuboLumen': "se utiliz\u00F3 tubo endotraqueal doble l\u00FAmen " + withNumber('tuboLumenNro'),
                'generalMascara': "se utiliz\u00F3 tubo m\u00E1scara lar\u00EDngea " + withNumber('generalMascaraNro')
            };
            return _.chain(checks).keys().filter(function (k) { return data[k]; }).map(function (k) { return checks[k]; }).value().join(', ');
        };
        var settingsEvolutionText = function (data) {
            var settings = {
                'modoVentilatorio': 'Modo ventilatorio',
                'volumenCorriente': ' Volumen corriente',
                'fiO2': 'Fracción inspirada de O2',
                'peep': 'PEEP'
            };
            return _.chain(settings).keys().filter(function (k) { return data[k]; }).map(function (k) { return (settings[k] + ": " + data[k]); }).value().join(', ');
        };
        var step5Completed = function () {
            $scope.data._5.anestesia = anesthesiaEvolutionText(angular.copy($scope.data._5));
            $scope.data._5.sedacionUtilizando = sedationEvolutionText(angular.copy($scope.data._5));
            $scope.data._5.generalUtilizando = generalEvolutionText(angular.copy($scope.data._5));
            $scope.data._5.generalAsistencia = assistanceEvolutionText(angular.copy($scope.data._5));
            $scope.data._5.seteoVentilador = settingsEvolutionText(angular.copy($scope.data._5));
        };
        $scope.removeAccess = function (index) {
            $scope.data._5.accesos.splice(index, 1);
        };
        // Step 5 End
        // Step 6 Begin
        var defaultData = { 'fecha': moment().format('DD/MM/YYYY'), 'hora': moment().format('HH:mm') };
        $scope.data._6 = $scope.data._6 || defaultData;
        $scope.launchModal = function (type, multiValues) {
            $scope[type] = $scope[type] || { momento: 0 };
            if (multiValues) {
                $scope[type].list = [{ name: '', value: 0, unit: '', other: '' }];
            }
            $("#" + type + "_modal").modal('show');
        };
        $scope.saveMonitorModal = function (data) { return saveModalData(data, 'monitoreo'); };
        $scope.formatMonitorRow = function (json) {
            var values = [];
            if (json.fc)
                values.push("FC " + json.fc + " lpm");
            if (json.fr)
                values.push("FR " + json.fr + " rpm");
            if (json.pa1 && json.pa2)
                values.push("PA " + json.pa1 + "/" + json.pa2 + " mmHg");
            if (json.saturacion)
                values.push("Sat " + json.saturacion + " %");
            if (json.et)
                values.push("ETCO2 " + json.et + " mmHg");
            return values.join(', ');
        };
        $scope.modalHasValues = function (json) {
            return json && _.keys(json).length > 1 && json.momento >= 0;
        };
        $scope.addDose = function (dosis, index) {
            var dose = dosis[index];
            if ($scope.canAddDose(dose)) {
                dosis.push({ name: '', value: 0, unit: '', other: '' });
            }
        };
        $scope.removeDose = function (dosis, index) {
            dosis.splice(index, 1);
        };
        $scope.canAddDose = function (dose) {
            return dose && (!_.isEmpty(dose.name) && !_.isEmpty(dose.unit)) || !_.isEmpty(dose.other);
        };
        $scope.momentAsNumber = function (data) {
            data.momento = parseInt(data.momento);
        };
        var addDataRow = function (col, newItem) {
            col.push(angular.copy(newItem));
            col = _(col).sortBy('momento');
            $scope.persistData();
            return col;
        };
        var saveModalData = function (data, kind) {
            $scope.data._6 = $scope.data._6 || {};
            $scope.data._6[kind] = $scope.data._6[kind] || [];
            var oldEntry = _($scope.data._6[kind]).find(function (x) { return x.momento === data.momento; });
            if (oldEntry) {
                Confirm.show({ 'message': "Ya existen valores cargados para el minuto " + data.momento + " <br/>\u00BFDesea reemplazar los valores cargados anteriormente?" }).then(function () {
                    $scope.data._6[kind] = _($scope.data._6[kind]).reject(function (x) { return x.momento === data.momento; });
                    $scope.data._6[kind] = addDataRow($scope.data._6[kind], data);
                });
            }
            else {
                $scope.data._6[kind] = addDataRow($scope.data._6[kind], data);
            }
        };
        $scope.setActiveDrugModal = function (modalName) {
            $scope.activeDrugModal = modalName;
        };
        $scope.saveDrugs = function (data) { return saveModalData(data, $scope.activeDrugModal); };
        $scope.saveFluids = function (data) { return saveModalData(data, 'fluidos'); };
        $scope.canSaveDosis = function (d) { return d && d.list && d.list.length > 0 && d.momento >= 0 && $scope.canAddDose(d.list[d.list.length - 1]); };
        $scope.formatDosisRow = function (list) { return list.map(function (l) { return l.name === 'Otro' ? l.other : l.name + " " + l.value + " " + l.unit; }).join(', '); };
        $scope.saveOutcomeModal = function (data) { return saveModalData(data, 'egresos'); };
        $scope.formatOutcomeRow = function (json) {
            var values = [];
            if (json.diuresis)
                values.push("Diuresis " + json.diuresis + " ml");
            if (json.perdida)
                values.push("P\u00E9rdida hem\u00E1tica " + json.perdida + " ml");
            return values.join(', ');
        };
        $scope.removeItem = function (items, index) {
            items.splice(index, 1);
        };
        var multiValueAsText = function (multiValue) {
            if (_.isEmpty(multiValue)) {
                return null;
            }
            var formatValue = function (v) { return v.name === 'Otro' ? v.other : v.name + " " + v.value + " " + v.unit; };
            var formatLine = function (item) { return ("\n - " + item.momento + "min: " + item.list.map(formatValue).join(', ')); };
            var lines = _(multiValue).map(formatLine);
            return lines.join('');
        };
        var monitoreoAsText = function (monitoreo) {
            var text = _(monitoreo).map(function (e) { return ("\n - " + e.momento + "min: " + $scope.formatMonitorRow(e)); });
            return text.join('');
        };
        var egresosAsText = function (egresos) {
            if (_.isEmpty(egresos)) {
                return null;
            }
            var text = _(egresos).map(function (e) { return ("\n - " + e.momento + "min: " + $scope.formatOutcomeRow(e)); });
            return text.join('');
        };
        var buildSeriesData = function (values, type) {
            if (type === 'pa') {
                var serie1 = [], serie2 = [];
                _.each(values, function (v) {
                    if (v['pa1'] && v['pa2']) {
                        serie1.push([v['momento'], v['pa1']]);
                        serie2.push([v['momento'], v['pa2']]);
                    }
                });
                return [{ name: 'sistólica', data: serie1 }, { name: 'diastólica', data: serie2 }];
            }
            else {
                var serie = [];
                _.each(values, function (v) {
                    if (v[type]) {
                        serie.push([v['momento'], v[type]]);
                    }
                });
                return [{ name: 'serie', data: serie }];
            }
        };
        var buildChart = function (values, type, title, xTitle, yTitle, unit) {
            // check that there's at least one value point to build the graph
            if (!_.some(values, function (v) { return v[type] || (type === 'pa' && v['pa1'] && v['pa2']); })) {
                return null;
            }
            var extraFormat = type === 'pa' ? '({series.name})' : '';
            return {
                chart: {
                    type: 'line',
                    width: 480,
                    height: 300
                },
                title: { text: null },
                subtitle: { text: title },
                tooltip: {
                    headerFormat: "<span style=\"font-size: 10px\">" + xTitle + ": {point.key}</span><br/>",
                    pointFormat: "<span style=\"color:{point.color}\">\u25CF</span> " + title + " " + extraFormat + ": <b>{point.y} " + unit + "</b><br/>"
                },
                xAxis: {
                    title: { text: xTitle },
                    labels: { enabled: false },
                    min: 0,
                    max: _.max(values, function (v) { return v['momento']; })['momento']
                },
                yAxis: {
                    title: { text: yTitle },
                    labels: { enabled: false }
                },
                legend: { enabled: type === 'pa' },
                series: buildSeriesData(values, type),
                credits: { enabled: false }
            };
        };
        var allDrugs = ['Adrenalina', 'Amiodarona', 'Amrinona', 'Atracurio', 'Atropina', 'Bupivacaína', 'Cefalotina', 'Cefazolina', 'Cefepime', 'Ceftazidime', 'Ceftriaxona', 'Ciprofloxacina', 'Dexametasona', 'Diclofenac', 'Dipirona', 'Dobutamina', 'Dopamina', 'Efedrina', 'Ergonovina', 'Etilefrina', 'Fentanilo', 'Flumazenil', 'Imipenem', 'Isoflurano', 'Ketorolac', 'Levofloxacina', 'Levosimendan', 'Lidocaína', 'Linezolid', 'Meropenem', 'Metoclopramida', 'Metronidazol', 'Midazolam', 'Milrinona', 'Morfina', 'N2O', 'Neostigmina', 'Nitroglicerina', 'Nitroprusiato sódico', 'Noradrenalina', 'Nubaína', 'Ocitocina', 'Omeprazol', 'Ondasetron', 'Pancuronio', 'Propofol', 'Ranitidina', 'Remifentanilo', 'Ropivacaína', 'Sevoflurano', 'Succnilcolina', 'Vancomicina', 'Vasopresina', 'Vecuronio'];
        var inductionDrugs = ['Fentanilo', 'Midazolam', 'Propofol', 'Remifentanilo', 'Sevofluorano', 'Atracurio', 'Vecuronio', 'Rocuronio', 'Dexmedetomidina', 'Diacepam', 'Morfina', 'Meperidina', 'Tiopental', 'Pancuronio', 'Succinilcolina', 'Ketamina'];
        var maintenanceDrugs = ['Sevofluorano', 'Isofluorano', 'Desfluorano', 'Remifentanilo', 'Propofol', 'Dexmedetomidina', 'Midazolam', 'Atracurio'];
        $scope.drugPanels = [
            { name: 'farmacos1', title: 'Fármacos utilizados en la inducción' },
            { name: 'farmacos2', title: 'Fármacos utilizados en el mantenimiento' },
            { name: 'farmacos3', title: 'Otros fármacos...' }
        ];
        $scope.drugLists = {
            'farmacos1': inductionDrugs,
            'farmacos2': maintenanceDrugs,
            'farmacos3': allDrugs
        };
        var step6Completed = function () {
            $scope.data._6 = $scope.data._6 || {};
            if (!_.isEmpty($scope.data._6.monitoreo)) {
                $scope.data._6.chartData = {};
                $scope.data._6.chartData.fc = buildChart($scope.data._6.monitoreo, 'fc', 'Frecuencia Cardíaca', 'Minuto', 'Latidos por minuto', 'lpm');
                $scope.data._6.chartData.fr = buildChart($scope.data._6.monitoreo, 'fr', 'Frecuencia Respiratoria', 'Minuto', 'Respiraciones por minuto', 'rpm');
                $scope.data._6.chartData.saturacion = buildChart($scope.data._6.monitoreo, 'saturacion', 'Saturación', 'Minuto', 'Saturación (%)', '%');
                $scope.data._6.chartData.et = buildChart($scope.data._6.monitoreo, 'et', 'ET CO2', 'Minuto', 'ET CO2 (mmHg)', 'mmHg');
                $scope.data._6.chartData.pa = buildChart($scope.data._6.monitoreo, 'pa', 'Presión Arterial', 'Minuto', 'Presión (mmHg)', 'mmHg');
                $scope.data._6.monitoreoText = monitoreoAsText($scope.data._6.monitoreo);
            }
            $scope.data._6.farmacos1Text = multiValueAsText($scope.data._6.farmacos1);
            $scope.data._6.farmacos2Text = multiValueAsText($scope.data._6.farmacos2);
            $scope.data._6.farmacos3Text = multiValueAsText($scope.data._6.farmacos3);
            $scope.data._6.fluidosText = multiValueAsText($scope.data._6.fluidos);
            $scope.data._6.egresosText = egresosAsText($scope.data._6.egresos);
        };
        $scope.validatePressure = function (data, form) {
            var bothEmpty = !_.isNumber(data.pa1) && !_.isNumber(data.pa2);
            var validSystolic = bothEmpty || (data.pa1 > 0 && data.pa1 <= 300 && _.isNumber(data.pa2));
            var validDiastolic = bothEmpty || (data.pa2 > 0 && data.pa2 <= 150 && data.pa1 >= data.pa2);
            form.apSystolic.$setValidity('apSystolic', validSystolic);
            form.apDiastolic.$setValidity('apDiastolic', validDiastolic);
        };
        var counterStep = 5;
        $scope.increaseCounter = function (modal) {
            modal.momento = +modal.momento + counterStep;
        };
        $scope.decreaseCounter = function (modal) {
            var value = +modal.momento - counterStep;
            modal.momento = value >= 0 ? value : 0;
        };
        $scope.monitoreo_data_rows = true;
        $scope.fluidos_data_rows = true;
        $scope.egresos_data_rows = true;
        $scope.toggleDataRows = function (kind) {
            $scope[kind + '_data_rows'] = !$scope[kind + '_data_rows'];
        };
        $scope.farmacos_data_rows = { 'farmacos1': true, 'farmacos2': true, 'farmacos3': true };
        $scope.toggleDrugDataRows = function (name) {
            $scope.farmacos_data_rows[name] = !$scope.farmacos_data_rows[name];
        };
        // Step 6 End
        // Step 7 Begin
        $scope.data._7 = $scope.data._7 || {};
        $scope.$watchCollection('[data._7.presion, data._7.actividad, data._7.conciencia, data._7.respiracion, data._7.saturacion]', function (values) {
            $scope.data._7.score = _(values).reduce(function (memo, value) {
                return memo + (value ? +value : 0);
            }, 0);
        });
        $scope.canSave = function (data) {
            var values = [data.presion, data.actividad, data.conciencia, data.respiracion, data.saturacion];
            return _(values).every(function (v) { return !_.isUndefined(v); });
        };
        $scope.openPdfLink = function () {
            var pdfUrl = 'https://s3-sa-east-1.amazonaws.com/omniasalud/static/Aldrete_Score.pdf';
            window.open(pdfUrl, 'pdf_info', 'menubar=0, scrollbar=0, location=0, status=0, toolbar=0, titlebar=0');
        };
        var initStep7 = function () {
            Problems.getProblemsAndAllergiesForPatient($routeParams.id).then(function (problems) {
                var existingProblems = _(problems).map(Problems.addDisplayAttrs);
                var procedure = Problems.getOrCreate('Evaluación preanestésica y cuidados intraoperatorios', 'procedure', existingProblems, $routeParams.id, $routeParams.episodeId);
                $scope.data.problems = [procedure];
            });
        };
        // Step 7 End
    }]);
