/// <reference path="../timeline-services.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../lib/angular.d.ts" />
/// <reference path="../services.d.ts" />
ehrControllers.controller('Timeline', ['$scope', '$routeParams', '$rootScope', '$q', 'Evolutions', 'Indication', 'Effects', 'Alerts', '$timeout', 'VitalSigns', 'FlatTimeline', 'TimelineUtils', 'MedicalInfo', 'Confirm',
    function ($scope, $routeParams, $rootScope, $q, Evolutions, Indication, Effects, Alerts, $timeout, VitalSigns, FlatTimeline, TimelineUtils, MedicalInfo, Confirm) {
        Omnia.trackEvent('Ingreso a Modulo: Evoluciones', { patient: $routeParams.id });
        $scope.editEvolution = function (evolution) {
            $rootScope.$emit('edit.evolution', angular.copy(evolution));
            $scope.returnToTop();
        };
        $scope.deleteEvolution = function (evolution) {
            Confirm.show({ 'message': '¿Desea eliminar la evolución?' }).then(function () {
                Evolutions.remove(evolution).success(function (stuff) {
                    Alerts.success('evolución eliminada');
                });
            });
        };
        $scope.medicalInfoKindLabel = function (kind) {
            return MedicalInfo.getTitle(kind);
        };
        $scope.parseValue = function (jsonString) {
            return JSON.parse(jsonString);
        };
        $scope.redraw = function () {
            $scope.loading.timeline = true;
            FlatTimeline.getByEpisode($routeParams.id, $routeParams.episodeId).then(function (data) {
                $scope.sessions = timelineFormat(data);
            })['finally'](function () {
                $scope.loading.timeline = false;
            });
        };
        $scope.filters = {};
        $scope.anyFilter = function () {
            return Object.keys($scope.filters).length > 0;
        };
        $scope.cleanFilters = function () {
            $rootScope.$emit('timeline-filters-apply', {});
            $scope.hideFilters();
        };
        var unbind = $rootScope.$on('timeline-filters-apply', function (event, data) {
            $scope.filters = data;
            $scope.redraw();
        });
        $scope.$on('destroy', unbind);
        var unixDay = TimelineUtils.unixDay;
        $scope.splitBackgroundDrugs = function (indications) {
            var backgroundDrug = function (it) {
                var isDrug = it.indication.treatment.kind === 'drug';
                var isBackground = !it.indication.starts || unixDay(it.indication.starts) < unixDay(it.indication.created);
                return isDrug && isBackground;
            };
            return [_(indications).filter(backgroundDrug), _(indications).reject(backgroundDrug)];
        };
        $scope.formatBackgroundDrug = function (it) {
            var stateLabels = { 'active': 'activo', 'suspended': 'suspendido' };
            return it.treatment.name + ' (' + stateLabels[it.state] + ')';
        };
        var events = ['problem.created', 'evolution.created', 'vitalsign.created', 'indication.created',
            'evolution.deleted', 'evolution.overriden', 'medicalInfo.closed', 'result.created'];
        _(events).each(function (event) {
            var un = $rootScope.$on(event, function () {
                $scope.redraw();
            });
            $scope.$on('destroy', un);
        });
        var handleProblemLikeEvents = function (timeline) {
            return _(timeline).map(function (it) {
                // all background.created are in fact problems.
                // we mark them differently to be able to pick them in the future, if necessary.
                if (it.kind === 'background.created') {
                    it.kind = 'problem.created';
                }
                return it;
            });
        };
        var filterOutEpisodicEvents = function (data) {
            var inEpisode = function (item, episode) {
                return item.created >= episode.starts && item.created <= episode.ends;
            };
            var inAnyEpisode = function (it) {
                var f = _.partial(inEpisode, it);
                return _(data.episodes).any(f);
            };
            return function (timeline) {
                return _(timeline).reject(inAnyEpisode);
            };
        };
        var appendEpisodeLinks = function (data) {
            var toTimelineEvent = function (ep) {
                return {
                    'created': +ep.starts,
                    'humanDate': TimelineUtils.humanDate(ep.starts),
                    'kind': 'nested.timeline',
                    'name': ep.kind,
                    'id': ep.id };
            };
            return function (timeline) {
                var epEvents = _(data.episodes).map(toTimelineEvent);
                return timeline.concat(epEvents);
            };
        };
        var createSessions = function (timeline) {
            var day = function (entry) { return entry.day; };
            var kind = function (entry) { return entry.kind; };
            var groupInSessions = function (values, day) {
                var entriesByKind = _.chain(values).groupBy(kind).map(function (values, kind) {
                    return { 'kind': kind, 'items': values };
                }).value();
                return {
                    'day': +day,
                    'entries': entriesByKind,
                    'created': values[0].created,
                    'humanDate': TimelineUtils.humanDate(values[0].created),
                    'author': values[0].author
                };
            };
            return _.chain(timeline).groupBy(day).map(groupInSessions).value();
        };
        var findById = function (thingsWithId, id) {
            return _(thingsWithId).findWhere({ 'id': +id });
        };
        var latestVersion = function (item, allItems) {
            if (!item.editedBy) {
                return item;
            }
            else {
                var newerVersionId = item.editedBy;
                var newerVersion = findById(allItems, newerVersionId);
                // if the latest version is not found, assume it's been deleted.
                if (!newerVersion)
                    return false;
                return latestVersion(newerVersion, allItems);
            }
        };
        var findLatestVersions = function (data) {
            return function (timeline) {
                _(timeline).each(function (it) {
                    if (it.kind === 'result.created') {
                        it.result = latestVersion(it.result, data.results);
                        if (!it.result || it.result.category.value === 'deleted') {
                            it.dirty = true;
                        }
                    }
                    if (it.kind === 'problem.created') {
                        var latest = latestVersion(it.problem, data.problems);
                        if (latest.state === 'deleted') {
                            it.dirty = true;
                        }
                    }
                    if (it.kind === 'evolution.created') {
                        it.evolution = latestVersion(it.evolution, data.evolutions);
                        if (!it.evolution) {
                            it.dirty = true;
                        }
                    }
                });
                return timeline;
            };
        };
        var handleSolvedProblems = function (data) {
            return function (timeline) {
                return _(timeline).map(function (it) {
                    if (it.kind === 'problem.edited') {
                        var latest = latestVersion(it.current, data.problems);
                        if (latest.state !== 'deleted' && it.current.state === 'solved' && it.old.state !== 'solved') {
                            it.kind = 'problem.solved';
                        }
                        else {
                            it.dirty = true;
                        }
                    }
                    return it;
                });
            };
        };
        var orderByDay = function (sessions) {
            return _(sessions).sortBy(function (session) { return -1 * session.day; });
        };
        var renderableEntries = ['evolution.created', 'background.created', 'problem.created', 'vitalsign.created', 'result.created',
            'indication.created', 'indication.suspended', 'indication.active', 'medicalInfo.closed', 'problem.edited'];
        var timelineFormat = function (data) {
            var f = _.compose(TimelineUtils.filterByDateRange($scope.filters.start, $scope.filters.end), orderByDay, createSessions, appendEpisodeLinks(data), filterOutEpisodicEvents(data), TimelineUtils.filterByEvolutionProblem($scope.filters.problem), TimelineUtils.filterByAuthor($scope.filters.author), TimelineUtils.filterPristine, handleSolvedProblems(data), findLatestVersions(data), handleProblemLikeEvents, TimelineUtils.renderables(renderableEntries), TimelineUtils.filterPristine);
            return f(data.timeline);
        };
        // display timeline for the first time.
        $scope.redraw();
    }]);
