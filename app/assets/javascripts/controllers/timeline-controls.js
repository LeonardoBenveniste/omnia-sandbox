/// <reference path="../lib/jquery.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../lib/angular.d.ts" />
/// <reference path="../model.d.ts" />
/// <reference path="../services.d.ts" />
ehrControllers.controller('TimelineControls', ['$scope', '$routeParams', '$rootScope', '$http', 'Scroll', 'MedicalInfo',
    function ($scope, $routeParams, $rootScope, $http, Scroll, MedicalInfo) {
        'use strict';
        Omnia.trackEvent('Ingreso a Modulo: Evoluciones', { patient: $routeParams.id });
        $scope._show_modal = function () {
            $('#_ep_modal').modal();
        };
        var loadMedicalInfoTemplates = function () {
            $scope.openRecordsCount = 0;
            $scope.medicalTemplates = [];
            MedicalInfo.getAvailableTemplates().then(function (templates) {
                MedicalInfo.forEpisodeAndStatus($routeParams.id, $routeParams.episodeId, 'open').then(function (response) {
                    var medicalRecords = response.data;
                    $scope.medicalTemplates = templates.map(function (t) {
                        var found = _(medicalRecords).findWhere({ kind: t.name });
                        if (found) {
                            t.openRecord = found;
                            $scope.openRecordsCount++;
                        }
                        else {
                            delete t.openRecord;
                        }
                        return t;
                    });
                });
            });
        };
        loadMedicalInfoTemplates();
        $scope._patientLoaded.then(function () {
            $scope.isCurrentEpisode = $scope.currentEpisodeId === +$routeParams.episodeId;
            $scope.isOutpatient = +$routeParams.episodeId === 0;
        });
        $scope.displayMedicalInfoForm = function (t) {
            $scope.displayForm({ 'type': t.name, 'reset': !t.openRecord });
            if (t.openRecord) {
                $scope.$broadcast('edit-medical-info', t.openRecord);
            }
        };
        var unbindNew = $rootScope.$on('new.medicalInfo', function (ev, kind) {
            $scope.displayForm({ 'type': kind, 'reset': true });
        });
        $scope.$on('$destroy', unbindNew);
        var unbindEdit = $rootScope.$on('edit.medicalInfo', function (ev, info) {
            $scope.displayForm({ 'type': info.kind, 'reset': false });
            $scope.$broadcast('edit-medical-info', info);
        });
        $scope.$on('$destroy', unbindEdit);
        // form navigation.
        $scope.activeForm = 'none';
        $scope.displayForm = function (options) {
            $scope.hideFilters();
            $scope.activeForm = options.type;
            if (options.reset) {
                $scope.$broadcast(options.type + '-reset');
            }
        };
        $scope.$on('timeline.display-form', function (event, data) {
            $scope.displayForm(data);
        });
        var closeAndScrollTop = function () {
            $scope.activeForm = 'none';
            Scroll.toTop();
        };
        $scope.$on('close-evolution-form', function (event, data) {
            closeAndScrollTop();
        });
        $scope.$on('close-problem-form', function (event, data) {
            if ($scope.activeForm === 'problem') {
                closeAndScrollTop();
            }
        });
        $scope.$on('close-background-form', function (event, data) {
            closeAndScrollTop();
        });
        $scope.$on('close-allergy-form', function (event, data) {
            closeAndScrollTop();
        });
        $scope.$on('close-vital-sign-form', function (event, data) {
            closeAndScrollTop();
        });
        $scope.$on('close-indication-form', function (event, data) {
            closeAndScrollTop();
        });
        $scope.$on('close-result-form', function (event, data) {
            closeAndScrollTop();
        });
        $scope.$on('close-medical-info-form', function (event, data) {
            loadMedicalInfoTemplates();
            closeAndScrollTop();
        });
        var goToForm = window.location.hash.substring(1);
        if (goToForm.length > 0) {
            if (goToForm === 'background') {
                Omnia.trackEvent('atc: abierto desde fichas link');
            }
            $scope.displayForm({ 'type': goToForm, 'reset': true });
        }
        $scope.openTemplate = function (t) {
            // TODO: if medical-info record is open, fill form with alredy loaded data.
            $scope.displayForm({ 'type': t.name });
        };
    }]);
