/// <reference path="../lib/moment.d.ts" />
/// <reference path="../lib/jquery.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../lib/angular.d.ts" />
/// <reference path="../model.d.ts" />
/// <reference path="../services.d.ts" />
ehrControllers.controller('Patient', ['$scope', '$routeParams', 'Patients', 'Problems',
    '$rootScope', '$location', function ($scope, $routeParams, Patients, Problems, $rootScope, $location) {
        $scope.basePatientUrl = '/patients/' + $routeParams.id + '/episodes/' + $routeParams.episodeId;
        $scope.cardToHide = '';
        $scope.timeline_type = 'flat';
        $scope.$on('hide-card', function (event, data) {
            $scope.cardToHide = data;
        });
        $scope._allProblems = Problems.getProblemsForPatient($routeParams.id);
        $scope.patient = $rootScope['selectedPatient'];
        $scope._patientLoaded = Patients.get($routeParams.id).then(function (patient) {
            var bday = moment(patient.fechaNacimiento, 'DD-MM-YYYY');
            patient.age = Patients.getAgeString(patient.fechaNacimiento);
            patient.ageInMonths = moment().diff(bday, 'months');
            patient.ageInYears = moment().diff(bday, 'years');
            // set in scope.
            $scope.patient = patient;
            $rootScope['selectedPatient'] = patient;
            var isActive = function (ep) { return !ep.state || ep.state === 'EN_LA_CAMA'; };
            var currentEp = _(patient.episodes).find(function (ep) { return !ep.end && isActive(ep); });
            $scope.currentEpisodeId = currentEp ? currentEp.id : -1;
            $rootScope.$emit('active.patient', { 'patient': patient });
        }).catch(function () {
            $scope.patient = { 'id': -1 };
            $location.path("/patients/" + $routeParams.id + "/episodes/" + $routeParams.episodeId + "/log");
        });
        $scope.returnToTop = function () {
            $('body,html').animate({ scrollTop: 0 }, 300);
        };
        $(window).scroll(function (e) {
            if ($(this).scrollTop() >= 50) {
                $('#return-to-top').addClass('appear');
            }
            else {
                $('#return-to-top').removeClass('appear');
            }
        });
    }]);
