/// <reference path="../lib/jquery.d.ts" />
/// <reference path="../lib/bootstrap.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../lib/angular.d.ts" />
/// <reference path="../services.d.ts" />
ehrControllers.controller('Parent', ['$scope', '$rootScope', '$location', '$window', '$http', 'UserInfo',
    function ($scope, $rootScope, $location, $window, $http, UserInfo) {
        $scope.loading = { 'timeline': true };
        // delegate calls to data-toggle="lightbox"
        // this is required for attachment and image previews.
        $(document).delegate('*[data-toggle="lightbox"]', 'click', function (event) {
            event.preventDefault();
            return $(this)['ekkoLightbox']({
                always_show_close: true
            });
        });
        $scope.openFileViewer = function (event, url, name) {
            if (navigator.platform != 'iPad') {
                event.preventDefault();
                $('#doc-viewer').on('shown.bs.modal', function () {
                    $('#doc-viewer iframe').attr('src', url);
                    $('#doc-viewer h4').text(name);
                });
                $('#doc-viewer').modal({ show: true });
            }
        };
        $scope.getFileType = function (fileName) {
            if (!fileName)
                return 'invalid';
            var extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
            if (extension.indexOf('jp') === 0 || extension == 'png') {
                return 'image';
            }
            else if (extension == 'pdf') {
                return 'document';
            }
            else {
                return 'invalid';
            }
        };
        // timeline filters show/hide.
        $scope.filtersVisible = false;
        $scope.showFilters = function () { return $scope.filtersVisible = true; };
        $scope.hideFilters = function () {
            $scope.filtersVisible = false;
            $rootScope.$emit('timeline-filters-close');
        };
        $rootScope.__offline_status = 0;
        function updateNetworkStatus(state) {
            if (state.id !== $rootScope.__offline_status) {
                $rootScope.$apply(function () {
                    $rootScope.__offline_status = state.id;
                });
            }
        }
        $http.defaults.headers.common = { 'X-ClientVersion': '2.0.85' };
    }]);
