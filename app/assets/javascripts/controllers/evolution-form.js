/// <reference path="../lib/jquery.d.ts" />
/// <reference path="../lib/bootstrap.d.ts" />
/// <reference path="../lib/angular.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../services.d.ts" />
ehrControllers.controller('EvolutionForm', ['$rootScope', '$scope', '$routeParams', 'Evolutions',
    'Alerts', 'S3', '$timeout', 'Problems', function ($rootScope, $scope, $routeParams, Evolutions, Alerts, S3, $timeout, Problems) {
        $scope.formTitle = 'Nueva Evolución';
        $scope.selectedProblems = [];
        $scope.showDate = false;
        $scope.attached = false;
        $scope.saving = false;
        var sessionStorageEvKey = "evolution.tmp--" + $routeParams.id + "--" + Omnia.currentUserId;
        var addToken = function (problem) {
            problem._token = problem.name + '|' + problem.state;
            return problem;
        };
        $scope._allProblems.then(function (problems) {
            $scope.problems = _(Problems.asSelectableList(problems)).map(addToken);
        });
        $scope._patientLoaded.then(function () {
            $scope.isCurrentEpisode = $scope.currentEpisodeId === +$routeParams.episodeId || +$routeParams.episodeId === 0;
        });
        var selected = function (token) {
            return $scope.selectedProblems.indexOf(token) >= 0;
        };
        $scope.selected = selected;
        $scope.toggleProblem = function (token) {
            // problems are like checkboxes but not really so we need to dirty the form manually.
            $scope.evolutionForm.$setDirty();
            if (selected(token)) {
                $scope.selectedProblems = _($scope.selectedProblems).reject(function (it) { return it === token; });
            }
            else {
                $scope.selectedProblems.push(token);
            }
        };
        var findSelectedProblems = function (tokens) {
            var problems = _($scope.problems).filter(function (p) { return tokens.indexOf(p._token) >= 0; });
            return angular.copy(problems);
        };
        $scope.save = function (evolution) {
            var hasFilesForS3 = function () {
                return ($scope.files && $scope.files.length > 0);
            };
            var saveEvolution = function () {
                // if there are no fileNames, means there are no attachments, or the
                // attachments were removed via editing. Remove them.
                // Note that there may be `fileNames` (listed) and no `files` (already uploaded).
                if ($scope.fileNames === undefined || $scope.fileNames.length < 1) {
                    evolution.attachments = [];
                }
                var problems = findSelectedProblems($scope.selectedProblems);
                var episodeId = $routeParams.episodeId;
                if (!episodeId) {
                    console.warn('episode id not defined in route params');
                }
                Evolutions.save(evolution, problems, episodeId, $scope.patient.currentLocation)
                    .then(function () {
                    Alerts.success('Evolución guardada con éxito');
                    window.sessionStorage.removeItem(sessionStorageEvKey);
                })['catch'](function () {
                    Alerts.error('Error guardando evolución');
                })['finally'](function () {
                    $scope.saving = false;
                    $scope.cancel();
                });
            };
            $scope.saving = true;
            evolution.patient = +$routeParams.id;
            var promises = [];
            if (hasFilesForS3()) {
                S3.upload($scope.files[0]).then(function (response) {
                    var url = response.headers('location');
                    evolution.attachments = [];
                    angular.forEach($scope.files, function (file) {
                        evolution.attachments.push({ 'kind': 'finding', 'name': file.name, 'url': url });
                    });
                    Omnia.trackEvent('Subir Archivo a Evolucion', evolution.attachments);
                    saveEvolution();
                })['catch'](function () {
                    Alerts.error('Error adjuntando archivo');
                    $scope.saving = false;
                });
            }
            else {
                saveEvolution();
            }
        };
        // attachment support.
        // accepts an element with 'files' or the files themselves (for drop support [see directive])
        $scope.attachments = function (element) {
            var files = (element.files || element), MAX_SIZE = 1024 * 1024 * 2.5;
            if (files && files.length && files.length > 0) {
                Alerts.clearLast();
                if (files[0].size > MAX_SIZE) {
                    Alerts.error('El tamaño del archivo adjunto no puede superar los 2.5 Mb.');
                    return;
                }
                else {
                    $scope.files = files;
                    $scope.fileNames = _.pluck(files, 'name');
                    $scope.attached = true;
                }
            }
            $timeout(function () {
                $scope.evolutionForm.$setDirty();
                $scope.$apply();
            });
        };
        $scope.cancelAttachments = function () {
            $scope.attached = false;
            $scope.files = [];
            $scope.fileNames = [];
            $scope.evolutionForm.$setDirty();
        };
        $scope.reset = function () {
            $scope.showDate = false;
            $scope.selectedProblems = [];
            $scope.evolution = { 'description': window.sessionStorage.getItem(sessionStorageEvKey) };
            $scope.cancelAttachments();
            $scope.evolutionForm.$setPristine();
        };
        $scope.cancel = function () {
            window.sessionStorage.removeItem(sessionStorageEvKey);
            $scope.reset();
            $scope.$emit('close-evolution-form');
        };
        $scope.addNewProblem = function () {
            $rootScope.$broadcast('problem-reset');
            $('.new-problem-modal').modal('show');
        };
        var unbindCreated = $rootScope.$on('problem.created', function (_, problem) {
            $('.new-problem-modal').modal('hide');
            var fullProblem = Problems.addDisplayAttrs(problem);
            $scope.selectedProblems.push(fullProblem._token);
            $scope.problems.push(fullProblem);
            $scope.evolutionForm.$setDirty();
        });
        $scope.$watch('evolution.description', function (evolution) {
            if (evolution && evolution.length) {
                window.sessionStorage.setItem(sessionStorageEvKey, evolution);
            }
        });
        var unbindEditEvolution = $rootScope.$on('edit.evolution', function (event, evolution) {
            $scope.formTitle = 'Editar Evolución';
            $scope.evolution = evolution;
            if (evolution.attachments && evolution.attachments.length > 0) {
                $scope.fileNames = _.pluck(evolution.attachments, 'name');
                $scope.attached = true;
            }
            $scope.displayForm({ 'type': 'evolution', 'reset': false });
            $scope.evolutionForm.$setPristine();
        });
        $scope.$on('evolution-reset', function () {
            $scope.reset();
        });
        $scope.$on('$destroy', unbindEditEvolution);
        $scope.$on('$destroy', unbindCreated);
    }]);
