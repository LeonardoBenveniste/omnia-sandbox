ehrControllers.controller('TimelineFilters', ['$scope', '$http', '$routeParams', '$rootScope',
  function($scope, $http, $routeParams, $rootScope) {
    'use strict';

    $scope.filter = {};

    var addFullName = function(author) {
      return {id: author.id, fullName: author.firstName + ' ' + author.lastName};
    };

    var unbindInit = $rootScope.$on('timeline-filters-init', function(event, data) {
      $scope.filterProblems = data.problems;
      $scope.filterAuthors = data.evAuthors.map(addFullName);

      // If there's only 1 author on TL, automatically select it.
      if ($scope.filterAuthors.length === 1) {
        $scope.filter.author = $scope.filterAuthors[0];
      }
    });
    $scope.$on('$destroy', unbindInit);

    var unbindClose = $rootScope.$on('timeline-filters-close', function(event, data) {
      $scope.filter = {};
      $rootScope.$emit('timeline-filters-apply', {});
    });
    $scope.$on('$destroy', unbindClose);

    $scope.applyFilters = function(filter) {
      var json = angular.copy(filter);
      json.author = json.author ? +json.author : 0;
      json.start = json.start ? window.moment(json.start, 'DD/MM/YYYY').toDate().getTime() : 0;
      json.end = json.end ? window.moment(json.end, 'DD/MM/YYYY').toDate().getTime() : Date.now();
      $rootScope.$emit('timeline-filters-apply', json);
      Omnia.trackEvent('Filtros Aplicados', json);
    };

    $scope.cleanFilters = function() {
      $scope.filter = {};
      $rootScope.$emit('timeline-filters-apply', {});
      $scope.hideFilters();
    };
}]);