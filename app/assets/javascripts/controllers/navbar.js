/// <reference path="../services.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../lib/jquery.d.ts" />
/// <reference path="../lib/bootstrap.d.ts" />
ehrControllers.controller('NavBar', ['$scope', '$rootScope', '$http', 'Alerts', '$route', '$location', '$timeout', 'UserInfo', 'Confirm', '$routeParams',
    function ($scope, $rootScope, $http, Alerts, $route, $location, $timeout, UserInfo, Confirm, $routeParams) {
        // DOM stuff.
        $('nav.navbar span.with-tooltip').tooltip();
        var patientMenu = {
            'id': 0, 'url': '/patients', 'label': 'Pacientes', 'regex': /\/patients$|search-patients/,
            'subitems': [{ 'url': function () { return '/patients'; }, 'label': 'Pacientes internados', 'nopatient': true },
                { 'url': function () { return '/search-patients'; }, 'label': 'Buscar Pacientes', 'nopatient': true }]
        };
        var toolsMenu = [{ 'url': function () { return $scope.patientEpisodeUrl + '/log'; }, 'label': 'Registro de acciones' }];
        var readOnlySubitems = [{ 'url': function () { return $scope.patientEpisodeUrl + '/log'; }, 'label': 'Registro de acciones', 'regex': 'patients.*[^contingency]$' }];
        if (UserInfo.hasContingencyConciliation()) {
            toolsMenu.push({ 'url': function () { return $scope.patientEpisodeUrl + '/contingency'; }, 'label': 'Reconciliar Contingencia' });
            readOnlySubitems.push({ 'url': function () { return $scope.patientEpisodeUrl + '/contingency'; }, 'label': 'Reconciliar Contingencia', 'regex': 'patients.*contingency$' });
        }
        var items = [patientMenu,
            {
                'id': 1, 'url': '', 'label': 'Historia Clínica', 'regex': /\/patients\/\d+\/.+/, 'onlyWhenSelected': true,
                'subitems': []
            }
        ];
        var readOnlyItems = [patientMenu,
            {
                'id': 1, 'url': '', 'label': 'Historia Clínica', 'regex': /\/patients\/\d+\/.+/, 'onlyWhenSelected': true,
                'subitems': readOnlySubitems
            }
        ];
        var getPatientEpisodeUrl = function (patient) {
            var episodeId = $routeParams.episodeId;
            return "/patients/" + patient.id + "/episodes/" + episodeId;
        };
        $scope.navigation = UserInfo.hasWriteAccess() ? items : readOnlyItems;
        $rootScope.$on('active.patient', function (_, data) {
            $scope.activePatient = data.patient;
            $scope.patientEpisodeUrl = getPatientEpisodeUrl(data.patient);
        });
        $scope.pick = function (id) {
            var picked = _($scope.navigation).find(function (it) { return it.id === id; });
            if (picked.newTab) {
                window.open(picked.url, 'help_page', 'menubar=0, scrollbar=0, location=0, status=0, toolbar=0, titlebar=0');
            }
            else {
                $scope.picked = picked;
            }
        };
        $scope.$watch('picked', function (current, old) {
            if (current && current !== old) {
                $location.url(current.url);
            }
        });
        var findItemByUrl = function (url) {
            return _($scope.navigation).find(function (it) { return !!url.match(it.regex); });
        };
        $scope.promptExit = function () {
            Confirm.show({ 'message': '¿Desea salir de Omnia Salud?' }).then(function () {
                window.location.href = '/logout';
            });
        };
        $scope.location = $location;
        $scope.$watch('location.url()', function (path, old) {
            if (!path)
                return;
            $scope.picked = findItemByUrl(path);
        });
    }]);
