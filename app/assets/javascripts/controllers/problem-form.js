/// <reference path="../lib/moment.d.ts" />
/// <reference path="../lib/jquery.d.ts" />
/// <reference path="../lib/bootstrap.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../lib/angular.d.ts" />
/// <reference path="../services.d.ts" />
/// <reference path="../model.d.ts" />
ehrControllers.controller('ProblemForm', ['$rootScope', '$scope', '$routeParams', 'Problems',
    'Alerts', 'Preferences', 'Diacritics', '$timeout', function ($rootScope, $scope, $routeParams, Problems, Alerts, Preferences, Diacritics, $timeout) {
        $scope.familyRelations = Problems.familyRelations;
        $scope.problem = { state: 'active' };
        $scope.pickTerms = false;
        $scope.reset = function (initialState) {
            $scope.problemForm.$setPristine();
            $scope.problem = { state: initialState ? initialState : 'active' };
            $scope.pickTerms = false;
            $scope.firstTerms = [];
            $scope.restTerms = [];
            $scope.showMore = false;
            $scope.terms = [];
        };
        $scope._patientLoaded.then(function () {
            $scope.isCurrentEpisode = $scope.currentEpisodeId === +$routeParams.episodeId || +$routeParams.episodeId === 0;
        });
        // validate started-ended range.
        var validateRange = function () {
            if ($scope.problem.started && $scope.problem.ended) {
                var started = moment($scope.problem.started, 'DD/MM/YYYY');
                var ended = moment($scope.problem.ended, 'DD/MM/YYYY');
                var valid = started.isValid() && ended.isValid() && ended.isAfter(started);
                $scope.problemForm.ended.$setValidity('ended', valid);
            }
        };
        $scope.$watch('problem.started + problem.ended', validateRange);
        $scope.$watch('problem.state', function () {
            delete ($scope.problem.started);
            delete ($scope.problem.ended);
            delete ($scope.problem.familyRelation);
            delete ($scope.problem.severity);
            delete ($scope.problem.note);
        });
        $scope.cancel = function () {
            $scope.reset();
            $scope.$emit('close-problem-form');
            $('.modal').modal('hide');
        };
        $scope.isProblemCategory = function (category) {
            var problemCategories = ['active', 'solved', 'family', 'procedure'];
            return problemCategories.indexOf(category) >= 0;
        };
        $scope.showStartDate = function (category) { return category !== 'family'; };
        $scope.showEndDate = function (category) { return category === 'solved'; };
        $scope.showFamilyRelations = function (category) { return category === 'family'; };
        $scope.getPlaceholderLabel = function (category) {
            var labels = {
                'active': 'Ingrese un problema',
                'solved': 'Ingrese un problema',
                'family': 'Ingrese un problema',
                'procedure': 'Ingrese un procedimiento',
                'allergy': 'Ingrese una sustancia, fármaco o alimento',
                'adverse-effect': 'Ingrese un fármaco'
            };
            return labels[category];
        };
        $scope.getStartDateLabel = function (category) {
            var labels = {
                'active': 'Fecha de inicio',
                'solved': 'Fecha de inicio',
                'procedure': 'Fecha de realización',
                'allergy': 'Fecha en que ocurrió',
                'adverse-effect': 'Fecha en que ocurrió'
            };
            return labels[category];
        };
        $scope._allProblems.then(function (problems) {
            // TODO: find out why these 2 are necessary.
            $scope.allProblems = problems;
            $scope.patientProblems = problems;
        });
        var noAccentsAndCase = Diacritics.removeAccentsAndCase;
        var filterResults = function (term, callback) {
            var problems = _($scope.patientProblems).filter(function (item) {
                var termMatch = noAccentsAndCase(item.name).indexOf(noAccentsAndCase(term)) >= 0;
                var notAllergy = $scope.problem.state !== 'allergy' && $scope.problem.state !== 'adverse-effect';
                return termMatch && notAllergy;
            });
            problems = _(problems).uniq(function (p) { return noAccentsAndCase(p.name); });
            var problemNames = _(problems).map(function (p) { return p.name; });
            callback(problemNames);
        };
        $scope.problemTypeaheadOptions = {
            hint: false,
            highlight: true,
            minLength: 2
        };
        $scope.problemDataset = {
            displayKey: function (name) { return name; },
            source: filterResults
        };
        $scope.findMatches = function () {
            $scope.terms = [];
            var term = $scope.problem.name;
            var findPerfectMatch = function (matches) {
                var loCaseTerm = term.toLowerCase();
                var exact = (matches.length === 1 && matches[0].description.toLowerCase() === loCaseTerm);
                return (exact) ? matches[0] : undefined;
            };
            var processResponse = function (response) {
                var firstTermSize = 5;
                var matches = response.data;
                var valid = (response.status >= 200 && response.status < 300) && (matches && matches.length > 0);
                if (!valid)
                    return [];
                var perfectMatch = findPerfectMatch(matches);
                if (perfectMatch) {
                    $scope.problem.code = perfectMatch.id;
                    $scope.problem.name = perfectMatch.description;
                    return [];
                }
                $scope.firstTerms = _.first(matches, firstTermSize);
                $scope.restTerms = _.rest(matches, firstTermSize);
                return matches;
            };
            return Problems.search(term).then(processResponse, function (data) {
                return [];
            });
        };
        $scope.suggestionsEnabled = false;
        $scope.cancelPickingTerms = function () {
            $scope.pickTerms = false;
            $scope.firstTerms = [];
            $scope.restTerms = [];
            $scope.showMore = false;
            $scope.terms = [];
        };
        $scope.saveWithTerms = function (problem) {
            var isEdit = (problem.id && problem.id > 0);
            var suggest = !!$scope.suggestionsEnabled && !isEdit;
            if ($scope.isProblemCategory(problem.state) && suggest) {
                $scope.saving = true;
                $scope.findMatches().then(function (terms) {
                    $scope.pickTerms = true;
                })['finally'](function () {
                    $scope.saving = false;
                });
            }
            else {
                $scope.save(problem);
            }
        };
        $scope.save = function (problem) {
            var json = angular.copy(problem);
            if (json.familyRelation) {
                json.familyRelation = json.familyRelation.value;
            }
            var convertDateField = function (field) {
                if (problem[field] && problem[field] !== 'desconocida') {
                    json[field] = getTimestamp(problem[field]);
                }
                else {
                    delete (json[field]);
                }
            };
            var getTimestamp = function (date) {
                // handle two-digit-year dates (not picked but inputted).
                if (date.split('/').length === 3 && date.split('/')[2].length <= 2) {
                    var lastSlash = date.lastIndexOf('/');
                    var firstPart = date.substring(0, lastSlash);
                    var years = +(date.substring(lastSlash + 1));
                    /// UNSAFE (getYear exists but it's deprecated)
                    var currentYear = (new Date()['getYear']() - 100);
                    var fullYears = years + (years > currentYear ? 1900 : 2000);
                    date = firstPart + '/' + fullYears;
                }
                return moment(date, 'DD/MM/YYYY').toDate().getTime();
            };
            convertDateField('started');
            convertDateField('ended');
            $scope.saving = true;
            var patient = $routeParams.id;
            var episode = $routeParams.episodeId;
            if (!episode) {
                console.warn('epsiode is undefined in route');
            }
            var startTime = Date.now();
            Problems.save(json, { patientId: patient, episodeId: episode }).then(function (problem) {
                Alerts.success('Problema guardado con éxito');
                $rootScope.$emit('add-evolution-problem', { problem: problem });
                $scope.reset();
                $scope.$emit('close-problem-form');
            })['finally'](function () {
                $scope.saving = false;
                // TODO: remove once latency issue is detected and fixed.
                // Track on mixpanel when saving takes more than 5 seconds.
                var diff = Date.now() - startTime;
                if (diff > 5000) {
                    Omnia.trackEvent('Slow problem save', { 'latency': diff, 'problem': JSON.stringify(problem) });
                }
            });
        };
        $scope.updateAndSave = function (term) {
            if ($scope.saving)
                return;
            $scope.problem.code = term.id;
            $scope.problem.name = term.description;
            $scope.save($scope.problem);
        };
        $('.btn-severity').popover({
            animation: true,
            placement: 'bottom',
            trigger: 'hover'
        });
        $scope.$on('edit-problem', function (e, problem) {
            problem.familyRelation = Problems.findFamilyRelation(problem.familyRelation);
            $scope.problem = problem;
            var timestampToStringDate = function (field) {
                if (problem[field] && problem[field] !== 'desconocida') {
                    $scope.problem[field] = moment(problem[field]).format('DD/MM/YYYY');
                }
            };
            timestampToStringDate('started');
            timestampToStringDate('ended');
            $scope.returnToTop();
        });
        var unbind = $rootScope.$on('problem-reset', function (ev, options) {
            if (options) {
                $scope.reset(options.initialState);
            }
            else {
                $scope.reset();
            }
        });
        $scope.$on('$destroy', unbind);
    }]);
