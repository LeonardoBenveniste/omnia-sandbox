/// <reference path="lib/moment.d.ts" />
/// <reference path="lib/bootstrap.d.ts" />
/// <reference path="lib/underscore.d.ts" />
/// <reference path="lib/angular.d.ts" />
/// <reference path="services.d.ts" />
var omniaServices = angular.module('services', []);
omniaServices.factory('Alerts', ['$rootScope', function ($rootScope) {
        var defaults = { 'positionClass': 'toast-top-mid' };
        var that = {};
        function inform(kind) {
            // curry some stuff just for the fun of it.
            return function (message, options) {
                $rootScope.$broadcast(kind, { 'message': message });
                var opts = angular.extend({}, defaults, options);
                clearLast();
                window['$$lastToast'] = toastr[kind](message, undefined, opts);
            };
        }
        function clearAll() {
            toastr.clear();
        }
        ;
        function clearLast() {
            toastr.clear(window['$$lastToast']);
        }
        ;
        return {
            info: inform('info'),
            error: inform('error'),
            warn: inform('warning'),
            success: inform('success'),
            clearAll: clearAll,
            clearLast: clearLast
        };
    }]);
omniaServices.service('OptionLists', function () {
    'use strict';
    this.personIdTypes = [
        { 'value': 'dni-arg', 'label': 'DNI', 'pattern': /^[0-9]{7,8}$/, 'maxLength': 8 },
        { 'value': 'pasaporte-arg', 'label': 'Pasaporte', 'pattern': /^\w+$/, 'maxLength': 12 }
    ];
    this.genders = [
        { 'value': 'fem', 'label': 'Femenino' },
        { 'value': 'masc', 'label': 'Masculino' },
        { 'value': 'other', 'label': 'Otro' }
    ];
    this.specialties = ['Alergología', 'Anatomía patológica', 'Anestesiología y reanimación', 'Angiología y cirugía vascular', 'Bioquímica clínica', 'Cardioangiología intervencionista', 'Cardiología clínica', 'Cirugía cardiovascular', 'Cirugía general', 'Cirugía oral y maxilofacial', 'Cirugía ortopédica y traumatología', 'Cirugía pediátrica', 'Cirugía plástica, estética y reparadora', 'Cirugía torácica', 'Dermatología', 'Diagnóstico por imágenes', 'Endocrinología', 'Estomatología', 'Farmacología clínica', 'Gastroenterología', 'Genética médica', 'Geriatría', 'Ginecología y obstetricia o tocología', 'Hematología', 'Infectología', 'Inmunología', 'Medicina de urgencias', 'Medicina del deporte', 'Medicina del trabajo', 'Medicina familiar y comunitaria', 'Medicina intensiva', 'Medicina interna', 'Medicina legal y forense', 'Medicina nuclear', 'Medicina preventiva y salud pública', 'Medicina transfusional', 'Microbiología y parasitología', 'Nefrología', 'Neumonología', 'Neurocirugía', 'Neurofisiología clínica', 'Neurología', 'Nutriología', 'Oftalmología', 'Oncología médica', 'Oncología radioterápica', 'Ortopedia y Traumatología', 'Otorrinolaringología', 'Pediatría', 'Proctología', 'Psiquiatría', 'Rehabilitación', 'Reumatología', 'Toxicología', 'Traumatología', 'Urología'];
    this.findPersonIdByValue = function (name) {
        return _(this.personIdTypes).find(function (e) { return e.value === name; });
    };
    this.defaultPersonId = this.personIdTypes[0];
    this.defaultGender = this.genders[0];
});
omniaServices.factory('Problems', ['$http', '$rootScope', '$q', 'Alerts', function ($http, $rootScope, $q, Alerts) {
        'use strict';
        var statuses = [
            { 'value': 'active', 'label': 'Activo' },
            { 'value': 'solved', 'label': 'Resuelto' },
            { 'value': 'family', 'label': 'Antecedente familiar' },
            { 'value': 'procedure', 'label': 'Procedimiento' },
            { 'value': 'allergy', 'label': 'Alergia' },
            { 'value': 'adverse-effect', 'label': 'Efecto adverso a fármaco' },
            { 'value': 'deleted', 'label': 'Borrado' }
        ];
        var familyRelations = [
            { 'value': 'dad', 'label': 'Padre' },
            { 'value': 'mom', 'label': 'Madre' },
            { 'value': 'brother', 'label': 'Hermano/a' },
            { 'value': 'f-gp', 'label': 'Abuelo/a paterno/a' },
            { 'value': 'm-gp', 'label': 'Abuelo/a materno/a' },
            { 'value': 'son', 'label': 'Hijo/a' },
            { 'value': 'uncle', 'label': 'Tío/a' },
            { 'value': 'nephew', 'label': 'Sobrino/a' },
            { 'value': 'niece', 'label': 'Nieto/a ' }
        ];
        var severityLabels = {
            mild: 'Leve',
            moderate: 'Modereada',
            severe: 'Grave'
        };
        var findFamilyRelation = function (value) {
            return _(familyRelations).find(function (rel) { return rel.value === value; });
        };
        var save = function (problem, params) {
            var deferred = $q.defer();
            // problem / allergy disambiguation.
            var isAllergy = (problem.state === 'allergy' || problem.state === 'adverse-effect');
            var duplicateWarn = isAllergy ? 'La alergia ya existe.' : 'El problema ya existe.';
            var hasEvolutionsWarn = isAllergy ? 'La alergia tiene evoluciones y no puede borrarse.' : 'El problema tiene evoluciones y no puede borrarse.';
            var errorMessage = isAllergy ? 'Error guardando alergia.' : 'Error guardando problema.';
            var mpEdit = isAllergy ? 'Alergia Editada' : 'Problema Editado';
            var mpNew = isAllergy ? 'Alergia Creada' : 'Problema Creado';
            var errorHandler = function (err) {
                if (err.cause === 'duplicate-problem') {
                    Alerts.warn(duplicateWarn);
                }
                else if (err.cause === 'problem-has-evolutions') {
                    Alerts.warn(hasEvolutionsWarn);
                }
                else {
                    Alerts.error(errorMessage);
                }
                deferred.reject(err);
            };
            if (_.isNumber(problem.id)) {
                $http.put("/api/v1/patients/" + params.patientId + "/episodes/" + params.episodeId + "/problems/" + problem.id, problem)
                    .success(function (data) {
                    $rootScope.$emit('problem.edited', problem, data);
                    Omnia.trackEvent(mpEdit, data);
                    deferred.resolve(data);
                })
                    .error(errorHandler);
            }
            else {
                $http.post("/api/v1/patients/" + params.patientId + "/episodes/" + params.episodeId + "/problems", problem)
                    .success(function (data) {
                    var result = angular.extend(problem, data);
                    $rootScope.$emit('problem.created', result);
                    deferred.resolve(result);
                })
                    .error(errorHandler);
            }
            return deferred.promise;
        };
        var getProblemsForPatient = function (patientId) {
            return $http.get("/api/v1/patients/" + patientId + "/problems").then(function (response) { return response.data; });
        };
        var getProblemsAndAllergiesForPatient = function (patientId) {
            return $http.get("/api/v1/patients/" + patientId + "/problemsWithAllergies").then(function (response) { return response.data; });
        };
        var getAllergiesForPatient = function (patientId) {
            return $http.get("/api/v1/patients/" + patientId + "/allergies");
        };
        var search = function (term) {
            return $http.get('/api/v1/search/problems?term=' + term);
        };
        var updateBackground = function (patientId, episodeId, problems) {
            var deferred = $q.defer();
            $http.post("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/problems/bulk", problems).success(function (problemsWithId) {
                _(problemsWithId).each(function (problem) {
                    $rootScope.$emit('problem.created', problem);
                });
                deferred.resolve(problemsWithId);
            }).error(function (err) {
                Alerts.error('Error guardando problema');
                deferred.reject(err);
            });
            return deferred.promise;
        };
        var status = function (problem) {
            var found = _(statuses).find(function (p) { return p.value === problem.state; });
            return found ? found.label : 'Desconocido';
        };
        var evolutionName = function (problem) {
            switch (problem.state) {
                case 'allergy':
                    return 'alergia a ' + problem.name;
                case 'adverse-effect':
                    return 'efecto adverso a ' + problem.name;
                default:
                    return problem.name;
            }
        };
        var _appendTooltipInfo = function (problem) {
            var lines = [];
            var s = moment(problem.started).format('DD/MM/YYYY');
            if (problem.state !== 'allergy' && problem.state !== 'adverse-effect') {
                lines.push(problem._humanState);
            }
            if (problem.started && problem.ended) {
                var e = moment(problem.ended).format('DD/MM/YYYY');
                lines.push(s + ' - ' + e);
            }
            else if (problem.started) {
                lines.push(s);
            }
            if (problem.note) {
                lines.push('Nota: ' + problem.note);
            }
            problem._tooltip = lines.join('<br>');
        };
        var addDisplayAttrs = function (_problem) {
            // necesary cast to 'augment' problem.
            var problem = _problem;
            problem._label = evolutionName(problem);
            switch (problem.state) {
                case 'active':
                    problem._ordinal = 1;
                    problem._humanState = 'Problema Activo';
                    break;
                case 'procedure':
                    problem._ordinal = 2;
                    problem._humanState = 'Procedimiento';
                    break;
                case 'allergy':
                    problem._ordinal = 3;
                    problem._humanState = 'Alergia';
                    break;
                case 'adverse-effect':
                    problem._ordinal = 4;
                    problem._humanState = 'Efecto Adverso';
                    break;
                case 'solved':
                    problem._ordinal = 5;
                    problem._humanState = 'Problema Resuelto';
                    break;
                case 'family':
                    problem._ordinal = 6;
                    problem._humanState = 'Antecedente Familiar';
                    break;
            }
            _appendTooltipInfo(problem);
            problem._token = problem.name + '|' + problem.state;
            return problem;
        };
        var asSelectableList = function (problems) {
            var deletedOrEdited = function (problem) {
                return problem.state === 'deleted' || (problem.editedBy && problem.editedBy > 0);
            };
            return _.chain(problems)
                .reject(deletedOrEdited)
                .uniq(function (p) { return p.state + p.name; })
                .map(addDisplayAttrs)
                .sortBy(function (p) { return p._ordinal; })
                .value();
        };
        var conciliate = function (patientId, problemId, episodeId) {
            return $http.post("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/problems/" + problemId + "/conciliate", {});
        };
        var byNameAndState = function (problemName, problemState, patientProblems) {
            var nameAndState = function (name, state) {
                return function (problem) {
                    return (problem.name.toLowerCase() === name.toLowerCase() && problem.state === state);
                };
            };
            return _(patientProblems).filter(nameAndState(problemName, problemState));
        };
        var getOrCreate = function (name, state, existingProblems, patientId, episodeId) {
            var found = byNameAndState(name, state, existingProblems);
            if (found.length > 0) {
                return found[0];
            }
            else {
                var p = {
                    'name': name,
                    'started': Date.now(),
                    'created': Date.now(),
                    'state': state
                };
                this.save(p, { 'patientId': patientId, 'episodeId': episodeId });
                return p;
            }
        };
        return {
            statuses: statuses,
            familyRelations: familyRelations,
            findFamilyRelation: findFamilyRelation,
            getAllergiesForPatient: getAllergiesForPatient,
            getProblemsForPatient: getProblemsForPatient,
            getProblemsAndAllergiesForPatient: getProblemsAndAllergiesForPatient,
            search: search,
            updateBackground: updateBackground,
            status: status,
            evolutionName: evolutionName,
            addDisplayAttrs: addDisplayAttrs,
            asSelectableList: asSelectableList,
            severityLabels: severityLabels,
            save: save,
            conciliate: conciliate,
            getOrCreate: getOrCreate
        };
    }]);
omniaServices.factory('Evolutions', ['$http', '$rootScope', '$q', 'Alerts',
    function ($http, $rootScope, $q, Alerts) {
        var _normalizeDate = function (date) {
            if (typeof date === "string") {
                // date is a string, it comes from the evolution form.
                return moment(date, 'DD/MM/YYYY').valueOf();
            }
            else if (typeof date === "number") {
                // date is a number, it comes from an existent evolution, keep it.
                return date;
            }
            else {
                // date is not specified, defaults to actual.
                return Date.now();
            }
        };
        var save = function (evolution, evolutionProblems, episodeId, location) {
            var ev = angular.copy(evolution);
            // we send only the ids to the backend, we don't need the whole object.
            ev.problemsIds = _(evolutionProblems).pluck('id');
            ev.date = _normalizeDate(ev.date);
            ev.location = location;
            // it's an override ?
            if (_.isNumber(ev.id)) {
                return $http.put('/api/v1/evolutions/' + ev.id, ev).success(function (data) {
                    // we do need the whole problem objects here, for display purposes.
                    ev.problems = evolutionProblems;
                    ev.id = data.id;
                    ev.editedId = ev.id;
                    $rootScope.$emit('evolution.overriden', ev, ev.id);
                    Omnia.trackEvent('Evolucion Editada', ev);
                });
            }
            else {
                return $http.post("/api/v1/evolutions/" + episodeId, ev).success(function (data) {
                    // we do need the whole problem objects here, for display purposes.
                    ev.problems = evolutionProblems;
                    ev.id = data.id;
                    $rootScope.$emit('evolution.created', ev);
                });
            }
        };
        var remove = function (evolution) {
            return $http.delete('/api/v1/evolutions/' + evolution.id).success(function () {
                $rootScope.$emit('evolution.deleted', evolution);
            });
        };
        return {
            save: save,
            remove: remove,
        };
    }]);
omniaServices.service('Effects', ['$timeout', function ($timeout) {
        var defaults = { 'color': '#ffa', 'delay': 1000 };
        this.highlight = function (elementId, options) {
            options = angular.extend(angular.copy(defaults), options);
            var elt = $(elementId);
            var oldBackgroundColor = elt.css('background-color');
            elt.css({ 'transition': 'background-color 1s', 'background-color': options.color });
            $timeout(function () {
                elt.css('background-color', oldBackgroundColor);
            }, options.delay);
        };
        this.highlightProblem = function (problem) {
            this.highlight('#problem-' + problem.id);
        };
        this.highlightEvolution = function (evolution) {
            this.highlight('#ev-' + evolution.id);
        };
        this.highlightAllergy = function (allergy) {
            this.highlight('#allergy-' + allergy.id);
        };
        this.highlightIndication = function (indication) {
            this.highlight('#indication-' + indication.id);
        };
    }]);
omniaServices.service('Treatment', ['$http', '$q', '$rootScope', 'Alerts', function ($http, $q, $rootScope, Alerts) {
        this.save = function (treatment, patientId) {
            var deferred = $q.defer();
            treatment.fav = !!treatment.fav;
            $http.post('/api/v1/treatments', treatment).success(function (data) {
                treatment.id = data.id;
                $rootScope.$emit('treatment.created', treatment);
                deferred.resolve(treatment);
            }).error(function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
        this.frequent = function (type) {
            var deferred = $q.defer();
            $http.get('/api/v1/treatments?top=5&kind=' + type).success(function (data) {
                deferred.resolve(data);
            })['catch'](function (error) {
                window.console.warn('Tratamientos frecuentes no disponibles en este momento.');
                deferred.resolve([]);
            });
            return deferred.promise;
        };
    }]);
omniaServices.service('Indication', ['$http', '$q', '$rootScope', '$routeParams', function ($http, $q, $rootScope, $routeParams) {
        var titles = {
            'drug': 'Fármaco',
            'diet': 'Dieta',
            'vaccine': 'Inmunización',
            'recommendation': 'Recomendación'
        };
        this.save = function (indication) {
            var deferred = $q.defer();
            var patientId = $routeParams.id;
            if (indication.starts === 'desconocida') {
                delete indication.starts;
            }
            if (indication.starts) {
                indication.starts = moment(indication.starts, 'DD/MM/YYYY').toDate().getTime();
            }
            $http.post('/api/v1/patients/' + patientId + '/indications', indication).success(function (data) {
                indication.id = data.id;
                deferred.resolve(indication);
            }).error(function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
        this.edit = function (indication) {
            var deferred = $q.defer();
            var patientId = $routeParams.id;
            $http.put('/api/v1/patients/' + patientId + '/indications/' + indication.id, indication).success(function (data) {
                $rootScope.$emit('indicaiton.suspended', indication);
                Omnia.trackEvent('Indicacion Suspendida', indication);
                deferred.resolve(indication);
            }).error(function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
        this.forPatient = function (patientId) {
            return $http.get('/api/v1/patients/' + patientId + '/indications');
        };
        this.remind = function (patientId, indication) {
            return $http.post('/api/v1/patients/' + patientId + '/indications/' + indication.id + '/reminder', {});
        };
        this.formatHuman = function (indication) {
            return titles[indication.treatment.kind] + ': ' + indication.treatment.name;
        };
    }]);
omniaServices.factory('S3', ['$http', '$q', function ($http, $q) {
        var log = new Log('S3');
        function generateUUID() {
            var mask = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
            return mask.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
        var upload = function (file) {
            var guid = generateUUID();
            var data = new FormData();
            var folder = CryptoJS.MD5(Omnia.currentUserId.toString());
            var path = "uploads/" + folder + "/" + guid + "'.'" + file.name;
            data.append('key', path);
            data.append('AWSAccessKeyId', Config.aws.accessKey);
            data.append('acl', 'public-read');
            data.append('policy', Config.aws.policy);
            data.append('signature', Config.aws.signature);
            data.append('Content-Type', file.type);
            data.append('file', file);
            log.info("uploading file " + file.name + " to bucket " + Config.aws.bucket + " ...");
            var url = "https://" + Config.aws.bucket + ".s3-sa-east-1.amazonaws.com/";
            return $http({ method: 'POST', url: url, data: data, headers: { 'Content-Type': undefined }, transformRequest: angular.identity });
        };
        var extractUrls = function (responses) {
            return responses.map(function (r) { return r.headers('location'); });
        };
        var uploadMulti = function (files) {
            var promises = files.map(function (f) { return upload(f); });
            return $q.all(promises).then(extractUrls);
        };
        return {
            upload: upload,
            uploadMulti: uploadMulti
        };
    }]);
omniaServices.factory('Patients', ['$http', '$q', '$cacheFactory', 'UserInfo', function ($http, $q, $cacheFactory, UserInfo) {
        var _cache = $cacheFactory('profile-cache');
        var getTimestamp = function (dateString, inputFormat) {
            return moment(dateString, inputFormat).valueOf();
        };
        var byInverseAdmissionDate = function (first, second) {
            if (!first.end || first.end > second.end) {
                return -1;
            }
            else if (!second.end || first.end < second.end) {
                return 1;
            }
            else {
                return 0;
            }
        };
        var getEpisodeId = function (admission) {
            return UserInfo.useInternalDbId() ? admission.internacionId : admission.admision;
        };
        var admissionsToPatient = function (admissions) {
            var patient = admissions[0].paciente;
            patient.episodes = admissions.map(function (a) {
                var episode = { id: getEpisodeId(a), start: getTimestamp(a.ingreso, 'DD-MM-YYYY HH:mm'), admissionId: a.admision, state: a.estado };
                if (a.fechaAlta) {
                    episode.end = getTimestamp(a.fechaAlta, 'DD-MM-YYYY HH:mm');
                }
                else if (a.estado != 'ANULADA') {
                    patient.currentLocation = a.habitacion + " (" + a.cama + ")";
                }
                if (a.cobertura) {
                    patient.healthCare = a.cobertura;
                }
                if (a.condicionIva) {
                    patient.iva = a.condicionIva;
                }
                return episode;
            }).sort(byInverseAdmissionDate);
            return patient;
        };
        var get = function (patientId) {
            return $http.get('/api/v1/patients/' + patientId).then(function (response) {
                if (response.status >= 200 && response.status < 400) {
                    return admissionsToPatient(response.data);
                }
            });
        };
        var all = function () {
            return $http.get('/api/v1/patients');
        };
        var search = function (params) {
            return $http({ url: '/api/v1/patients/search', method: 'GET', params: params });
        };
        var fromNow = function (timestamp, unit) {
            var now = moment();
            var then = moment(timestamp);
            return now.diff(then, unit);
        };
        var getAgeString = function (date) {
            var yearsFromNow = function (birthday) { return fromNow(moment(birthday, 'DD-MM-YYYY').valueOf(), 'years'); };
            var monthsFromNow = function (birthday) { return fromNow(moment(birthday, 'DD-MM-YYYY').valueOf(), 'months'); };
            var daysFromNow = function (birthday) { return fromNow(moment(birthday, 'DD-MM-YYYY').valueOf(), 'days'); };
            var years = yearsFromNow(date);
            if (years > 0) {
                return years + ((years === 1) ? ' año' : ' años');
            }
            var months = monthsFromNow(date);
            if (months > 0) {
                return months + ((months === 1) ? ' mes' : ' meses');
            }
            var days = daysFromNow(date);
            return days + ((days === 1) ? ' día' : ' días');
        };
        var getOutPatients = function () {
            return $http.get('/api/v1/outpatients');
        };
        return {
            get: get,
            all: all,
            search: search,
            admissionsToPatient: admissionsToPatient,
            getAgeString: getAgeString,
            fromNow: fromNow,
            getOutPatients: getOutPatients
        };
    }]);
omniaServices.factory('Results', ['$http', '$q', '$rootScope', 'Alerts',
    function ($http, $q, $rootScope, Alerts) {
        var categories = [
            { value: 'lab', label: 'Análisis de laboratorio', order: 1 },
            { value: 'images', label: 'Imágenes', order: 2 },
            { value: 'eco', label: 'Ecografía', order: 3 },
            { value: 'ecg', label: 'Electrocardiograma', order: 4 },
            { value: 'pathology', label: 'Anatomía Patológica', order: 5 },
            { value: 'microbiology', label: 'Microbiología', order: 6 },
            { value: 'transfusional', label: 'Medicina Transfusional', order: 6 },
            { value: 'practices', label: 'Otros', order: 7 },
            { value: 'deleted', label: 'Eliminados', order: 8 }
        ];
        function getCategory(name) {
            return _(this.categories).find(function (c) {
                return c.value == name;
            });
        }
        ;
        function deleteResult(resultId, patientId, episodeId) {
            return $http.delete("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/results/" + resultId);
        }
        ;
        function save(result, patientId, episodeId) {
            var deferred = $q.defer();
            result.patient = +patientId;
            var errorHandler = function (err) {
                Alerts.error('Error guardando resultado');
                deferred.reject(err);
            };
            var resultId = result.id;
            if (_.isNumber(resultId)) {
                $http.put("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/results/" + resultId, result)
                    .success(function (data) {
                    var response = angular.extend(result, data);
                    $rootScope.$emit('result.edited', { newResult: response, oldId: resultId });
                    Omnia.trackEvent('Resultado Editado', response);
                    deferred.resolve(response);
                })
                    .error(errorHandler);
            }
            else {
                result.created = Date.now();
                $http.post("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/results", result)
                    .success(function (data) {
                    var response = angular.extend(result, data);
                    $rootScope.$emit('result.created', response);
                    Omnia.trackEvent('Resultado Creado', response);
                    deferred.resolve(response);
                })
                    .error(errorHandler);
            }
            return deferred.promise;
        }
        ;
        function forPatient(patientId, episodeId) {
            var results = $http.get("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/results");
            var attachments = $http.get("/api/v1/patients/" + patientId + "/attachments");
            return $q.all([results, attachments]).then(function (responses) {
                var results = responses[0].data.results, solicitations = responses[0].data.solicitations, authors = responses[0].data.authors, attachments = responses[1].data;
                _(attachments).each(function (att) {
                    var found = _(results).find(function (res) { return res.id === att.parentId && att.kind == 'result'; });
                    if (found) {
                        found.attachments = [att];
                    }
                });
                results = _(results).map(function (r) {
                    r.solicitation = _(solicitations).findWhere({ id: r.solicitationId });
                    return r;
                });
                return { 'results': results, 'authors': authors };
            });
        }
        ;
        return {
            categories: categories,
            getCategory: getCategory,
            forPatient: forPatient,
            deleteResult: deleteResult,
            save: save
        };
    }]);
omniaServices.service('Preferences', ['$http', function ($http) {
        'use strict';
        var labels = { 'terminologyDisabled': 'No sugerir problemas' };
        var getOrElseEmpty = function (promise) {
            return promise.then(function (response) {
                return response.data;
            }, function (error) {
                return {};
            });
        };
        this.values = function (id) {
            return getOrElseEmpty($http.get('/api/v1/profile/' + id + '/preferences/ehr'));
        };
        this.update = function (id, newPrefs) {
            return this.values(id).then(function (prefs) {
                angular.extend(prefs, newPrefs);
                return getOrElseEmpty($http.post('/api/v1/profile/' + id + '/preferences/ehr', prefs));
            });
        };
        //      \    /\
        //       )  ( ')
        //      (  /  )
        // tico  \(__)|
        // ^TP1207890;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;V7\778M8\\\\\\\\\\\\\\\\\\\\\CQ  Œ™
        this.labelFor = function (key) {
            return labels[key];
        };
    }]);
omniaServices.service('VitalSigns', [function () {
        'use strict';
        this.signTypes = {
            'heartFreq': { label: 'Frecuencia Cardíaca', unit: 'lpm', collapsed: false },
            'ap': { label: 'Presión Arterial', unit: 'mm Hg', collapsed: false },
            'breathFreq': { label: 'Frecuencia Respiratoria', unit: 'rpm', collapsed: false },
            'temp': { label: 'Temperatura', unit: '℃', collapsed: false },
            'weight': { label: 'Peso', unit: 'kg', collapsed: false },
            'height': { label: 'Talla', unit: 'cm', collapsed: false }
        };
        this.format = function (type, value) {
            switch (type) {
                case 'ap': return value.systolic + ' / ' + value.diastolic;
                case 'temp': return value.value + ' (' + value.location + ')';
                default: return value;
            }
        };
        this.formatHuman = function (vitalSign) {
            var data = JSON.parse(vitalSign.value);
            var type = Object.keys(data)[0];
            var signValue = _.isNumber(data[type]) ? data[type] : this.format(type, data[type]);
            return this.signTypes[type].label + ': ' + signValue + ' ' + this.signTypes[type].unit;
        };
    }]);
omniaServices.factory('MedicalInfo', ['$rootScope', '$http', '$q', '$templateCache',
    function ($rootScope, $http, $q, $templateCache) {
        var _templates = [
            { 'name': 'vital-signs', 'label': 'Signos Vitales', 'controller': 'VitalSigns' },
            { 'name': 'anestesia', 'label': 'Nuevo Parte Anestésico', 'controller': 'Anestesia' },
            { 'name': 'sample-form', 'label': 'Ficha de Prueba', 'controller': 'SampleForm' },
            { 'name': 'ingreso', 'label': 'Ficha de ingreso', 'controller': 'Ingreso', 'evolutionTemplate': 'ingreso' },
            { 'name': 'egreso', 'label': 'Ficha de egreso', 'controller': 'Egreso', 'evolutionTemplate': 'egreso' },
            { 'name': 'pq', 'label': 'Parte Quirúrgico', 'controller': 'Pq', 'evolutionTemplate': 'pq' },
            { 'name': 'pa', 'label': 'Parte Anestésico', 'controller': 'Pa', 'evolutionTemplate': 'pa' },
            { 'name': 'monitoreo', 'label': 'Monitoreo Intraoperatorio', 'controller': 'Monitoreo', 'evolutionTemplate': 'monitoreo' }
        ];
        var getEvolutionTemplates = function () {
            var names = _.chain(_templates).filter(function (t) { return _.isString(t.evolutionTemplate); }).map(function (t) { return t.evolutionTemplate; }).value();
            var baseUrl = '/assets/templates/medical-info-evolutions/';
            var templates = names.map(function (n) {
                return {
                    'name': n,
                    'promise': $http.get(baseUrl + n + '.html', { cache: $templateCache })
                };
            });
            return $q.all(templates.map(function (t) { return t.promise; })).then(function (responses) {
                var ret = {};
                for (var r in responses) {
                    ret[templates[r].name] = responses[r].data;
                }
                return ret;
            });
        };
        var helpers = {
            showIf: function (condition) { return condition ? '' : 'hidden'; },
            hideIf: function (condition) { return condition ? 'hidden' : ''; },
            asList: function (collection, separator, field, auxField) {
                var aux = function (obj) { return auxField && obj[auxField] && obj[auxField].length > 0 ? "(" + obj[auxField] + ")" : ''; };
                var items = _(collection).map(function (item) { return (item[field] + " " + aux(item)); });
                return items.join(separator);
            },
            printTeam: function (collection) {
                var members = _(collection).filter(function (m) { return m.name.length > 0; }).map(function (m) { return (m.role + ": " + m.name); });
                return members.join('\n');
            }
        };
        var getEvolutionHelpers = function () {
            return helpers;
        };
        var save = function (patientId, episode, info, location) {
            info.location = location;
            if (info.id) {
                // Edit existing record.
                info.updated = moment().valueOf();
                return $http.put("/api/v1/patients/" + patientId + "/episodes/" + episode + "/medical-info/" + info.id, info).then(function (response) {
                    var _info = angular.copy(info);
                    $rootScope.$emit('medical-info-edited', _info);
                    return _info;
                });
            }
            else {
                // Create new record
                return $http.post("/api/v1/patients/" + patientId + "/episodes/" + episode + "/medical-info", info).then(function (response) {
                    var _info = angular.copy(info);
                    _info.id = response.data.id;
                    _info.created = moment().valueOf();
                    $rootScope.$emit('medical-info-added', _info);
                    return _info;
                });
            }
        };
        var forEpisodeAndStatus = function (patientId, episodeId, status) {
            return $http.get("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/medical-info?status=" + status);
        };
        var forEpisodeAndKind = function (patientId, episodeId, kind) {
            return $http.get("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/medical-info?kind=" + kind);
        };
        var updateStatus = function (patientId, episodeId, newStatus, info, location) {
            var json = { status: newStatus, updated: Date.now(), 'kind': info.kind, 'location': location };
            return $http.put("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/medical-info/" + info.id + "/status", json);
        };
        var remove = function (patientId, episodeId, info) {
            var json = { 'kind': info.kind };
            return $http['delete']("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/medical-info/" + info.id, json);
        };
        var getAvailableTemplates = function () {
            return $http.get('/api/v1/medical-info/available').then(function (response) {
                // fill in labels and controllers on 'templates'.
                return _(response.data).map(function (k) {
                    return _(_templates).findWhere({ 'name': k });
                });
            });
        };
        var getTitle = function (name) {
            return _(_templates).findWhere({ 'name': name }).label;
        };
        return {
            save: save,
            forEpisodeAndStatus: forEpisodeAndStatus,
            forEpisodeAndKind: forEpisodeAndKind,
            updateStatus: updateStatus,
            remove: remove,
            getAvailableTemplates: getAvailableTemplates,
            getTitle: getTitle,
            getEvolutionTemplates: getEvolutionTemplates,
            getEvolutionHelpers: getEvolutionHelpers
        };
    }]);
omniaServices.service('Diacritics', function () {
    // This is only for spanish lang.
    // We should use localeCompare() but it's still not supported by Safari.
    var noAccentsAndCase = function (word) {
        return word.toLowerCase()
            .replace(/á/g, 'a')
            .replace(/é/g, 'e')
            .replace(/í/g, 'i')
            .replace(/ó/g, 'o')
            .replace(/ú/g, 'u');
    };
    this.removeAccentsAndCase = noAccentsAndCase;
    this.noAccentsComparator = function (actual, expected) {
        if (_.isString(actual)) {
            return noAccentsAndCase(actual).indexOf(noAccentsAndCase(expected)) >= 0;
        }
        else {
            return angular.equals(actual, expected);
        }
    };
});
omniaServices.service('DrugControls', function () {
    this.stepToValue = function (step) {
        var fractions = ['3/4', '1/2', '1/4'];
        return (step < 1) ? fractions[-step] : step.toString();
    };
    this.adjustAndformatFreq = function (frequencyInHours) {
        var week = 24 * 7;
        var month = week * 4;
        var year = month * 12;
        var hours, text;
        // ups
        if (frequencyInHours == 13) {
            frequencyInHours = 24;
        }
        if (frequencyInHours == 25) {
            frequencyInHours = week;
        }
        if (frequencyInHours == week + 1) {
            frequencyInHours = month;
        }
        if (frequencyInHours == month + 1) {
            frequencyInHours = year;
        }
        // downs
        if (frequencyInHours == 23) {
            frequencyInHours = 12;
        }
        if (frequencyInHours == (week - 1)) {
            frequencyInHours = 24;
        }
        if (frequencyInHours == (month - 1)) {
            frequencyInHours = week;
        }
        if (frequencyInHours == (year - 1)) {
            frequencyInHours = month;
        }
        // text
        if (frequencyInHours == week) {
            text = '1 semana';
        }
        else if (frequencyInHours == month) {
            text = '1 mes';
        }
        else if (frequencyInHours == year) {
            text = '1 año';
        }
        else {
            var suffix = frequencyInHours == 1 ? ' hora' : ' horas';
            text = frequencyInHours + suffix;
        }
        return { 'hours': frequencyInHours, 'text': text };
    };
});
omniaServices.service('CalendarClient', ['$rootScope', '$q', '$http', '$routeParams', function ($rootScope, $q, $http, $routeParams) {
        this.getAppointments = function (ownerId) {
            return $http.get('/api/v1/appointments/' + ownerId);
        };
        this.createAppointment = function (event, ownerId) {
            return $http.post('/api/v1/appointments/' + ownerId, event);
        };
        this.deleteAppointment = function (id) {
            return $http['delete']('/api/v1/appointments/' + id);
        };
        this.getTodayPatients = function () {
            return $http.get('/api/v1/appointments?today');
        };
        this.getFutureAppointments = function (ownerId) {
            var ts = Date.now();
            return $http.get('/api/v1/appointments/' + ownerId + '?since=' + ts);
        };
        this.getPrefs = function (id) {
            return $http.get('/api/v1/profile/' + id + '/preferences/calendar').then(function (response) {
                return response.data;
            });
        };
        this.savePrefs = function (id, prefs) {
            return $http.post('/api/v1/profile/' + id + '/preferences/calendar', prefs).success(function () {
                return $http['delete']('/api/v1/appointments-future');
            });
        };
    }]);
omniaServices.service('Scroll', ['$timeout', function ($timeout) {
        var defaults = {
            'speed': 300,
            'top': 75,
            'delay': 0
        };
        this.to = function (id, opts) {
            var options = angular.extend({}, defaults, opts);
            var doScroll = function () {
                var elt = $('#' + id);
                if (elt.length > 0) {
                    var top = elt.offset().top - options.top;
                    $('body,html').animate({ scrollTop: options.top }, options.speed);
                }
            };
            $timeout(doScroll, options.delay);
        };
        this.toTop = function () {
            $('body,html').animate({ scrollTop: 0 }, 300);
        };
    }]);
omniaServices.factory('Confirm', ['$timeout', '$q', function ($timeout, $q) {
        var defaults = {
            title: 'Advertencia',
            message: '¿Está seguro?'
        };
        var $elt = $('#confirm-modal');
        var confirmed = false;
        $('#confirm-modal-ok').on('click', function () {
            confirmed = true;
        });
        $('#confirm-modal-nok').on('click', function () {
            confirmed = false;
        });
        var show = function (opts) {
            var deferred = $q.defer();
            var options = angular.extend({}, defaults, opts);
            $('#confirm-modal .title').html(options.title);
            $('#confirm-modal .message').html(options.message);
            $elt.modal('show');
            $elt.on('hidden.bs.modal', function (e) {
                if (confirmed) {
                    deferred.resolve(confirmed);
                }
                else {
                    deferred.reject(confirmed);
                }
            });
            return deferred.promise;
        };
        return {
            show: show
        };
    }]);
omniaServices.factory('Warning', ['$timeout', '$q', function ($timeout, $q) {
        var defaults = {
            title: 'Advertencia',
            message: 'Advertencia'
        };
        var $elt = $('#warning-modal');
        var show = function (opts) {
            var deferred = $q.defer();
            var options = angular.extend({}, defaults, opts);
            $('#warning-modal .title').html(options.title);
            $('#warning-modal .message').html(options.message);
            $elt.modal('show');
            $elt.on('hidden.bs.modal', function (e) {
                deferred.resolve(true);
            });
            return deferred.promise;
        };
        return {
            show: show
        };
    }]);
omniaServices.factory('UserInfo', [function () {
        var WRITE_ACCESS_ROLE = 1, PRINT_ACCESS_ROLE = 2, EXTERNAL_ACCESS_ROLE = 3, DISABLED_AUTOLOGOFF_ROLE = 4, CONTINGENCY_CONCILIATION = 5;
        function hasAccess(roleId) {
            var info = getUserInfo();
            if (info) {
                return !!_(info.roles).findWhere({ id: roleId });
            }
            return false;
        }
        ;
        function hasWriteAccess() {
            return hasAccess(WRITE_ACCESS_ROLE);
        }
        ;
        function hasPrintAccess() {
            return hasAccess(PRINT_ACCESS_ROLE);
        }
        ;
        function hasDisabledAutoLogoff() {
            return hasAccess(DISABLED_AUTOLOGOFF_ROLE);
        }
        ;
        function hasContingencyConciliation() {
            return hasAccess(CONTINGENCY_CONCILIATION);
        }
        ;
        function getUserInfo() {
            return { "id": 10, "nombre": "Alejandro", "apellido": "Bologna", "centros": [{ "id": 15, "nombre": "Sanatorio+Trinidad+Palermo", "codigo": "TRIPA", "udnDto": { "id": 3, "codigo": "SA", "nombre": "Sanatorio+de+la+Trinidad+Palermo" } }, { "id": 21, "nombre": "Sanatorio+Trinidad+Ramos+Mejia", "codigo": "TRM", "udnDto": { "id": 23, "codigo": "TRM", "nombre": "Trinidad+Ramos+Mejia" } }], "subEspecialidades": [{ "id": 24, "descripcion": "CLINICA+MEDICA", "descripcionLarga": "CLINICA+MEDICA", "especialidad": { "id": 176, "codigo": "ALE", "nombre": "ALERGIA", "descripcion": "ALERGIA" } }, { "id": 77, "descripcion": "TERAPIA+INTENSIVA", "descripcionLarga": "TERAPIA+INTENSIVA", "especialidad": { "id": 176, "codigo": "ALE", "nombre": "ALERGIA", "descripcion": "ALERGIA" } }], "roles": [{ "id": 1, "codigo": "Escribir", "descripcion": "Escribir" }, { "id": 2, "codigo": "Imprimir", "descripcion": "Imprimir" }, { "id": 3, "codigo": "Acceso+Externo", "descripcion": "Acceso+Externo" }, { "id": 4, "codigo": "Desactivar+Autologoff", "descripcion": "Desactivar+Autologoff" }], "matricula": { "id": 22, "tipoMatricula": "NACIONAL", "numeroMatricula": "120587" } };
        }
        function getChosenCenter() {
            return 15;
        }
        function useInternalDbId() {
            return false;
        }
        function isOutPatient() {
            return false;
        }
        return {
            getUserInfo: getUserInfo,
            hasWriteAccess: hasWriteAccess,
            hasPrintAccess: hasPrintAccess,
            hasDisabledAutoLogoff: hasDisabledAutoLogoff,
            hasContingencyConciliation: hasContingencyConciliation,
            isOutPatient: isOutPatient,
            useInternalDbId: useInternalDbId
        };
    }]);
omniaServices.factory('Solicitations', ['$http', function ($http) {
        function save(patientId, episodeId, solicitation) {
            return $http.post("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/solicitations", solicitation);
        }
        function forPatientAndEpisode(patientId, episodeId) {
            return $http.get("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/solicitations");
        }
        function updateStatus(patientId, episodeId, sId, newStatus) {
            var json = { 'status': newStatus };
            return $http.put("/api/v1/patients/" + patientId + "/episodes/" + episodeId + "/solicitations/" + sId + "/status", json);
        }
        ;
        var conditionsMoment = {
            'urgent': { 'style': 'alert-danger', 'label': 'Urgente', 'text': 'Al realizar la solicitud como URGENTE, se recomienda que utilice otra vía de comunicación para asegurarse la recepción más prontamente posible de la misma' },
            'next_round': { 'style': 'alert-info', 'label': 'Próxima ronda', 'text': 'La solicitud será procesada durante la siguiente ronda' },
            'routine': { 'style': 'alert-info', 'label': 'Rutina', 'text': 'La solicitud será procesada en la primera ronda del día' },
            'within6hours': { 'style': 'alert-info', 'label': 'Dentro de las 6 hs', 'text': 'Se realizarán pruebas de compatibilidad completas' },
            'emergency': { 'style': 'alert-danger', 'label': 'Emergencia', 'text': '<strong>No se realizarán pruebas de compatibilidad. El solicitante es co-responsable de la solicitud.</strong> Al realizar la solicitud como EMERGENCIA, se recomienda que utilice otra vía de comunicación para asegurarse la recepción más prontamente posible de la misma' },
            'datetime': { 'style': '', 'label': 'Fecha y hora específica', 'text': '' },
            'day': { 'style': '', 'label': 'Durante el día', 'text': '' }
        };
        var kinds = [
            { 'name': 'lab', 'label': 'Laboratorio' },
            { 'name': 'images', 'label': 'Diagnóstico por imágenes' },
            { 'name': 'eco', 'label': 'Ecografía' },
            { 'name': 'resonance', 'label': 'Resonancia Magnética' },
            { 'name': 'microbiology', 'label': 'Microbiología' },
            { 'name': 'pathology', 'label': 'Anatomía patológica' },
            { 'name': 'nuclear', 'label': 'Medicina nuclear' },
            { 'name': 'transfusional', 'label': 'Medicina transfusional' },
            { 'name': 'practices', 'label': 'Interconsultas y prácticas' },
        ];
        var conditionsSample = {
            'not_extracted': 'Realizar extracción',
            'already_extracted': 'Muestra ya remitida'
        };
        var conditionsTransport = {
            'chair': 'Silla',
            'little_bed': 'Camilla',
            'bed': 'Cama',
            'do_in_bed': 'Realizar en lecho'
        };
        var conditionsYesNo = {
            'yes': 'Si',
            'no': 'No',
            'unknown': 'Desconoce'
        };
        return {
            save: save,
            updateStatus: updateStatus,
            forPatientAndEpisode: forPatientAndEpisode,
            kinds: kinds,
            conditionsMoment: conditionsMoment,
            conditionsSample: conditionsSample,
            conditionsTransport: conditionsTransport,
            conditionsYesNo: conditionsYesNo
        };
    }]);
omniaServices.factory('Practices', ['$http', function ($http) {
        var groups = [
            { "id": "G-1", "practices": [100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 122] },
            { "id": "G-2", "practices": [6000, 6001, 6002, 6003, 6004, 6005, 6006, 6007, 6008, 6009, 6010, 6011, 6012, 6013, 6014, 6015, 6016, 6017, 6018, 6019, 6020, 6021, 6022, 6023, 6024, 6025, 6026, 6027, 6028, 6029, 6030, 6031] },
            { "id": "G-3", "practices": [106, 398, 400, 401, 403] },
            { "id": "G-4", "practices": [630, 641, 643, 644, 645, 647, 801] },
            { "id": "G-5", "practices": [1400, 1401, 1402] },
            { "id": "G-6", "practices": [1421, 1422, 1423, 1424, 1425, 1426] },
            { "id": "G-9", "practices": [2231, 2232, 2236, 2238, 2239, 2240, 2241] },
            { "id": "G-10", "practices": [398, 430, 431, 432, 433, 434, 435, 436, 437, 438] },
            { "id": "G-11", "practices": [601, 602] },
            { "id": "G-12", "practices": [1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008] },
            { "id": "G-13", "practices": [1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019] },
            { "id": "G-14", "practices": [1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031] },
            { "id": "G-15", "practices": [4110, 4111, 4112] },
            { "id": "G-16", "practices": [140, 141, 142] },
            { "id": "G-17", "practices": [1628, 1629] },
            { "id": "G-18", "practices": [1740, 1741, 1742, 1743, 1744, 1745, 1746] },
            { "id": "G-19", "practices": [1835, 1836, 1837, 1838] },
            { "id": "G-20", "practices": [801, 1843, 1844, 1845, 1846, 1847, 1848] },
            { "id": "G-21", "practices": [6100, 6101, 6102, 6103, 6104, 6105, 6106, 6107] },
            { "id": "G-22", "practices": [3600, 3601, 3602, 3603, 3604, 3605, 3606, 3607, 3608, 3609, 3610, 3611, 3612, 3613, 3614, 3615, 3616, 3617, 3618, 3619, 3621, 3622, 3623, 3624, 3625, 3626, 3627, 3628, 3629] },
            { "id": "G-23", "practices": [1860, 1861, 1862, 1863] },
            { "id": "G-24", "practices": [160, 161, 162, 163, 164] },
            { "id": "G-25", "practices": [2248, 2249, 2250, 2251] },
            { "id": "G-26", "practices": [2248, 2249, 2252, 2253] },
            { "id": "G-27", "practices": [2248, 2249, 2254, 2255] },
            { "id": "G-28", "practices": [2301, 2302] },
            { "id": "G-29", "practices": [1901, 1902, 1903, 1904] },
            { "id": "G-30", "practices": [628, 629] },
            { "id": "G-31", "practices": [621, 622] },
            { "id": "G-32", "practices": [606, 607] },
            { "id": "G-33", "practices": [722, 723] },
            { "id": "G-34", "practices": [729, 730] },
            { "id": "G-35", "practices": [738, 739] },
            { "id": "G-36", "practices": [1404, 1405, 1407, 1408, 1410, 1411] },
            { "id": "G-38", "practices": [1849, 1850, 1851, 1852, 1853, 1854, 1855, 1856] },
            { "id": "G-39", "practices": [625, 626] },
            { "id": "G-40", "practices": [1201, 1202] },
            { "id": "G-41", "practices": [1205, 1206] },
            { "id": "G-42", "practices": [3227, 3228, 3229] },
            { "id": "G-43", "practices": [6012, 6013, 6014, 6015, 6016, 6017, 6018, 6019] },
            { "id": "G-44", "practices": [6200, 6201, 6202, 6203, 6204, 6205, 6206, 6207, 6208] },
            { "id": "G-45", "practices": [2248, 2249, 2260, 2261] },
            { "id": "G-46", "practices": [2248, 2249, 2262, 2263] },
            { "id": "G-47", "practices": [2248, 2249, 2264, 2265] },
            { "id": "G-48", "practices": [250, 251] },
            { "id": "G-49", "practices": [614, 615] },
            { "id": "G-50", "practices": [732, 733] },
            { "id": "G-51", "practices": [734, 735] },
            { "id": "G-52", "practices": [667, 668, 669, 670, 671] },
            { "id": "G-57", "practices": [2248, 2249, 2266, 2267] },
            { "id": "G-59", "practices": [5015, 5016, 5017, 5018, 5019, 5051, 5052, 5053, 5054, 5055] }
        ];
        function search(kind, term) {
            return $http.get("/api/v1/practices/search/" + kind + "?term=" + term);
        }
        function isGroup(pid) {
            return groups.some(function (g) { return g.id === pid; });
        }
        function getPracticesForGroup(pid) {
            return groups.filter(function (g) { return g.id === pid; })[0].practices.map(function (n) { return n.toString(); });
        }
        function getPracticesByIds(kind, pids) {
            return $http.get("/api/v1/practices?kind=" + kind + "&ids=" + pids.join(','));
        }
        function getSelectedPractices(jsonString) {
            var practices = JSON.parse(jsonString);
            if (!practices) {
                return [];
            }
            return _(practices).map(function (practice) {
                if (practice.id === -1) {
                    return { id: -1, label: 'Estudio Anatomopatológico' };
                }
                else {
                    return practice;
                }
            });
        }
        ;
        return {
            search: search,
            getPracticesForGroup: getPracticesForGroup,
            isGroup: isGroup,
            getPracticesByIds: getPracticesByIds,
            getSelectedPractices: getSelectedPractices
        };
    }]);
omniaServices.factory('EcgPractices', [function () {
        var rhythms = ['Sinusal', 'Fibrilación auricular', 'Aleteo auricular',
            'Ritmo auricular caótico', 'Taquicardia auricular', 'Bloqueo AV completo', 'Ritmo de la unión',
            'Taquicardia paroxística supraventricular', 'RIVA', 'Taquicardia ventricular'];
        // Intervalo QT: va de 200 a 1200. Para el valor normal hay q hacer la cuenta de la
        // fórmula corregida: QT / intervalo RR (1 / 3).Siendo que RR = 60 / Frecuencia cardiaca * 1000.
        // Normal de 300 a 450
        var ranges = {
            'pWave': { 'min': 40, 'max': 200, 'softMin': 40, 'softMax': 100 },
            'prInterval': { 'min': 40, 'max': 1000, 'softMin': 120, 'softMax': 200 },
            'qrsDuration': { 'min': 60, 'max': 600, 'softMin': 60, 'softMax': 100 },
            'qrsAxis': { 'min': -180, 'max': 180, 'softMin': -30, 'softMax': 120 },
            'qtInterval': { 'min': 200, 'max': 1200, 'softMin': 300, 'softMax': 450 },
            'heartFreq': { 'min': 0, 'max': 300, 'softMin': 0, 'softMax': 300 },
        };
        function inRange(key, value) {
            var range = ranges[key];
            return value > range.min && value < range.max;
        }
        function inSoftRange(key, value) {
            var range = ranges[key];
            return value > range.softMin && value < range.softMax;
        }
        var min = function (key) { return ranges[key].min; };
        var max = function (key) { return ranges[key].max; };
        return {
            rhythms: rhythms,
            min: min,
            max: max,
            inRange: inRange,
            inSoftRange: inSoftRange
        };
    }]);
omniaServices.factory('ContingencyService', ['$http', function ($http) {
        var log = new Log("contingency-service");
        var kinds = Object.freeze([
            { 'value': 'evolution', 'label': 'Evolución' },
            { 'value': 'pq', 'label': 'Parte quirúrgico' },
            { 'value': 'pa', 'label': 'Parte anestésico' },
            { 'value': 'monitoreo', 'label': 'Monitoreo cardiológico' }
        ]);
        function save(patientId, episode, contingency) {
            var json = angular.copy(contingency);
            return $http.post("/api/v1/patients/" + patientId + "/episodes/" + episode + "/contingency", json);
        }
        function getByFqe(patientId, episode) {
            return $http.get("/api/v1/patients/" + patientId + "/episodes/" + episode + "/contingency");
        }
        function remove(patientId, episode, contingencyId) {
            return $http.delete("/api/v1/patients/" + patientId + "/episodes/" + episode + "/contingency/" + contingencyId);
        }
        function humanKind(kind) {
            var found = kinds.filter(function (k) { return k.value === kind; });
            if (found.length !== 1) {
                log.warn("there should be exactly one contingency 'kind' with value " + kind + ", found = " + found.length);
                return "desconcido";
            }
            return found[0].label;
        }
        function getKinds() {
            return angular.copy(kinds);
        }
        return {
            save: save,
            getByFqe: getByFqe,
            remove: remove,
            humanKind: humanKind,
            getKinds: getKinds
        };
    }]);
function Log(namespace) {
    var prodDomain = 'galeno.omniasalud.com';
    var isProduction = function () { return window.document.domain.toLowerCase() === prodDomain; };
    var noop = function (message) { };
    var format = function (message) {
        return "[" + namespace + "] " + message;
    };
    var info = function (message) {
        console.log(format(message));
    };
    var warn = function (message) {
        console.warn(format(message));
    };
    var error = function (message) {
        console.error(format(message));
    };
    // TODO: implement circular buffer & error reporting.
    if (isProduction()) {
        this.info = noop;
        this.warn = noop;
        this.error = noop;
    }
    else {
        this.info = info;
        this.warn = warn;
        this.error = error;
    }
}
