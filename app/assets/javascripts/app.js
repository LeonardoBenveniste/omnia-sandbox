/// <reference path="lib/jquery.d.ts" />
/// <reference path="lib/moment.d.ts" />
/// <reference path="lib/underscore.d.ts" />
/// <reference path="lib/bootstrap.d.ts" />
/// <reference path="lib/angular.d.ts" />
/// <reference path="model.d.ts" />
/// <reference path="services.d.ts" />
var ehrApp = angular.module('ehrApp', [
    'ngRoute',
    'ngSanitize',
    'services',
    'siyfion.sfTypeahead',
    'ehrControllers'
]);
// TODO: remove 'any' change all 'declare var ehrControllers' to be
// of type angular.IModule
var ehrControllers = angular.module('ehrControllers', []);
var theRouterProvider;
var defaultRouting = function () {
    theRouterProvider.
        when('/patients/:id/episodes/:episodeId/evolutions', {
        templateUrl: '/assets/templates/patient/evolutions.html',
        controller: 'Patient'
    }).
        otherwise({
        redirectTo: '/patients/7431/episodes/418/evolutions'
    });
};
ehrApp.run(['UserInfo', function (UserInfo) {
        defaultRouting();
    }]);
ehrApp.config(['$routeProvider', '$locationProvider', '$provide', '$httpProvider',
    function ($routeProvider, $locationProvider, $provide, $httpProvider) {
        'use strict';
        theRouterProvider = $routeProvider;
        // Use history api.
        $locationProvider.html5Mode(true);
        // Decorate exception handling to send js errors as MixPanel events.
        $provide.decorator('$exceptionHandler', ['$delegate', function ($delegate) {
                return function (exception, cause) {
                    try {
                        $delegate(exception, cause);
                        var data = {};
                        data.message = exception.message;
                        data.cause = cause;
                        // printStackTrace is part of stacktrace.js
                        data.stacktrace = window['printStackTrace']({ e: exception });
                        $.ajax({
                            url: '/jslogger',
                            method: 'POST',
                            data: JSON.stringify(data),
                            contentType: 'application/json'
                        });
                        if (typeof Omnia !== 'undefined') {
                            Omnia.trackEvent('javascript exception', data);
                        }
                    }
                    catch (e) {
                        console.error(e);
                    }
                };
            }]);
        $provide.factory('authHttpInterceptor', function ($q) {
            return {
                'responseError': function (rejection) {
                    // on unauth redirect to login.
                    if (rejection.status === 401) {
                        window.location.href = '/login';
                    }
                    return $q.reject(rejection);
                }
            };
        });
        $httpProvider.interceptors.push('authHttpInterceptor');
    }]);
ehrApp.directive('selectedClass', ['$location', function (location) {
        'use strict';
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var clazz = attrs.selectedClass;
                var path = attrs.href;
                scope.location = location;
                scope.$watch('location.url()', function (newPath) {
                    if (path === newPath || (attrs.selectedRegexp && newPath.match(attrs.selectedRegexp))) {
                        element.addClass(clazz);
                    }
                    else {
                        element.removeClass(clazz);
                    }
                });
            }
        };
    }]);
ehrApp.directive('informErrors', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var field = attrs.informErrors;
            var message = attrs.errorMessage || 'error';
            var $formGroup = $(element[0]).parents('.form-group');
            if (!$formGroup) {
                window.console.warn('element', element, 'does not have an enclosing \'form-group\'');
                return;
            }
            scope.$watch(field + '.$dirty && ' + field + '.$invalid', function (errors) {
                $formGroup.removeClass('has-error has-feedback');
                $formGroup.find('label.error').remove();
                if (errors) {
                    var $errorLabel = $('<label for="' + attrs.name + '" class="error">' + message + '</label>');
                    $formGroup.addClass('has-error has-feedback');
                    $formGroup.append($errorLabel);
                }
            });
        }
    };
});
ehrApp.directive('warnCapsLock', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var message = 'Mayúsculas activado! Desactive BloqMayus';
            var $formGroup = $(element[0]).parents('.form-group');
            if (!$formGroup) {
                window.console.warn('element', element, 'does not have an enclosing \'form-group\'');
                return;
            }
            element.bind('keypress', function (e) {
                var kc = (e.keyCode) ? e.keyCode : e.which;
                var isUp = kc >= 65 && kc <= 90;
                var isLow = kc >= 97 && kc <= 122;
                $formGroup.removeClass('has-error has-feedback');
                $formGroup.find('label.error').remove();
                if ((isUp && !e.shiftKey) || (isLow && e.shiftKey)) {
                    var $warn = $('<label class="error">' + message + '</label>');
                    $formGroup.addClass('has-error has-feedback');
                    $formGroup.append($warn);
                }
            });
        }
    };
});
/*
 * Creates a bootstrap tooltip. Note that you can't put a tooltip on a disabled button or input
 * so you need to wrap the element with a <div> or <span>.
 */
ehrApp.directive('tooltip', function () {
    return {
        restrict: 'A',
        link: function ($scope, element, attrs) {
            var title = attrs.tooltipTitle || 'el formulario tiene campos incompletos o no válidos';
            var placement = attrs.placement || 'right';
            var html = !!attrs.tooltipHtml;
            element.attr('data-toggle', 'tooltip');
            element.attr('data-placement', placement);
            element.attr('data-html', html);
            element.attr('title', title);
            $(element).tooltip();
            if (attrs.tooltipDisplayOn) {
                $scope.$watch(attrs.tooltipDisplayOn, function (display) {
                    if (display) {
                        $(element).tooltip();
                    }
                    else {
                        $(element).tooltip('destroy');
                    }
                });
            }
        }
    };
});
ehrApp.directive('datePicker', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var view = attrs.datePickerStartView || 'month';
            var minView = attrs.datePickerMinView || 'month';
            var format = attrs.datePickerFormat || 'dd/mm/yyyy';
            var endDate = attrs.endDate || new Date();
            if (attrs.startDate === 'today') {
                attrs.startDate = new Date();
            }
            var startDate = attrs.startDate || '01-01-1900';
            $(element)['datetimepicker']({
                format: format,
                endDate: endDate,
                startDate: startDate,
                autoclose: true,
                startView: view,
                minView: minView,
                language: 'es',
                todayBtn: attrs.unknownDate });
            // set input elements as readonly, so you cannot input free text.
            if (attrs.setReadonly) {
                $(element).find('input').attr('readonly', 'true');
            }
        }
    };
});
ehrApp.directive('timePicker', function () {
    return {
        scope: {
            model: '=ngModel'
        },
        restrict: 'A',
        link: function (scope, element, attrs) {
            var step = attrs.minuteStep || 5;
            $(element).siblings().first().click(function () {
                $(element)['timepicker']('showWidget');
            });
            scope.$watch('model', function (actual, old) {
                if (actual) {
                    $(element)['timepicker']({
                        showMeridian: false,
                        defaultTime: actual,
                        minuteStep: +step
                    });
                }
            });
        }
    };
});
ehrApp.filter('as', ['$parse', function ($parse) {
        return function (value, path) {
            return $parse(path).assign(this, value);
        };
    }]);
ehrApp.directive('dropzone', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var dropzone = element[0], intoParent = false, deep = false;
            dropzone.addEventListener('dragenter', function (e) {
                if (intoParent) {
                    deep = true;
                }
                else {
                    $(element).addClass('hover');
                    intoParent = true;
                }
                e.stopPropagation();
                e.preventDefault();
            }, false);
            dropzone.addEventListener('dragover', function (e) {
                $(element).addClass('hover');
                e.stopPropagation();
                e.preventDefault();
            });
            dropzone.addEventListener('dragleave', function (e) {
                if (deep) {
                    deep = false;
                }
                else if (intoParent) {
                    intoParent = false;
                }
                if (!intoParent && !deep) {
                    $(element).removeClass('hover');
                }
            }, false);
            dropzone.addEventListener('drop', function (e) {
                $(element).removeClass('hover');
                var dt = e.dataTransfer;
                var files = dt.files;
                scope['attachments'](files);
                e.stopPropagation();
                e.preventDefault();
            }, false);
        }
    };
});
ehrApp.directive('staticInclude', ['$http', '$templateCache', '$compile', function ($http, $templateCache, $compile) {
        return function (scope, element, attrs) {
            var templatePath = attrs.staticInclude;
            $http.get(templatePath, { cache: $templateCache }).success(function (response) {
                var contents = element.html(response).contents();
                $compile(contents)(scope);
            });
        };
    }]);
ehrApp.directive('isolateForm', [function () {
        return {
            restrict: 'A',
            require: '?form',
            link: function (scope, elm, attrs, ctrl) {
                if (!ctrl)
                    return;
                var ctrlCopy = {};
                angular.copy(ctrl, ctrlCopy);
                var parent = elm.parent().controller('form');
                if (!parent)
                    return;
                parent.$removeControl(ctrl);
                // Replace form controller with an isolated form
                var isolatedFormCtrl = {
                    $setValidity: function (validationToken, isValid, control) {
                        ctrlCopy.$setValidity(validationToken, isValid, control);
                        parent.$setValidity(validationToken, true, ctrl);
                    },
                    $setDirty: function () {
                        elm.removeClass('ng-pristine').addClass('ng-dirty');
                        ctrl.$dirty = true;
                        ctrl.$pristine = false;
                    }
                };
                angular.extend(ctrl, isolatedFormCtrl);
            }
        };
    }]);
ehrApp.config(['$logProvider', function ($logProvider) {
        $logProvider.debugEnabled(!!Config.logEnabled);
    }]);
/*
 * Optionally don't trigger reload actions when doing a location change.
 * Usage:
 *  location.skipReload().path('/thing/' + id).replace();
 *
 * see: https://github.com/angular/angular.js/issues/1699#issuecomment-22511464
 */
ehrApp.factory('location', ['$location', '$route', '$rootScope',
    function ($location, $route, $rootScope) {
        $location.skipReload = function () {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
            return $location;
        };
        return $location;
    }
]);
ehrApp.directive('datePicker', function () {
    /*jshint multistr: true */
    return {
        scope: {
            model: '=ngModel',
            required: '@'
        },
        restrict: 'E',
        template: "<ng-form name=\"date_picker\">\n      <div class=\"form-group\" ng-class=\"{'has-error': date_picker.$dirty && __invalid}\">\n        <div class=\"row\">\n          <div class=\"col-xs-3\">\n            <select name=\"day\" class=\"form-control\" ng-model=\"day\" ng-options=\"day for day in allDays\" ng-required={{required}}>\n              <option value=\"\">D\u00EDa</option>\n            </select>\n          </div>\n          <div class=\"col-xs-6\">\n            <select name=\"month\" class=\"form-control\" ng-model=\"month\" ng-required=\"{{required}}\">\n              <option value=\"\">Mes</option>\n              <option value=\"01\">Enero</option>\n              <option value=\"02\">Febrero</option>\n              <option value=\"03\">Marzo</option>\n              <option value=\"04\">Abril</option>\n              <option value=\"05\">Mayo</option>\n              <option value=\"06\">Junio</option>\n              <option value=\"07\">Julio</option>\n              <option value=\"08\">Agosto</option>\n              <option value=\"09\">Septiembre</option>\n              <option value=\"10\">Octubre</option>\n              <option value=\"11\">Noviembre</option>\n              <option value=\"12\">Diciembre</option>\n            </select>\n          </div>\n          <div class=\"col-xs-3\">\n            <select name=\"year\" class=\"form-control\" ng-model=\"year\" ng-options=\"year for year in allYears\" ng-required={{required}}>\n              <option value=\"\">A\u00F1o</option>\n            </select>\n          </div>\n        </div>\n        <label for=\"day\" class=\"error\" ng-show=\"date_picker.$dirty && __invalid\">Fecha no v\u00E1lida</label>\n      </div>\n      </ng-form>",
        link: function (scope, elem, attrs, ctrl) {
            var i = 1, thisYear = (new Date().getFullYear());
            var validDate = function (date) {
                var now = moment();
                return date.isBefore(now) && date.isValid();
            };
            // fill in days.
            scope.allDays = [];
            while (i <= 31) {
                scope.allDays.push(i);
                i++;
            }
            // fill in years.
            scope.allYears = [];
            i = 1900;
            while (i <= thisYear) {
                scope.allYears.push(i);
                i++;
            }
            scope.$watch('model', function (actual, old) {
                if (actual && actual.constructor !== Date) {
                    var m = moment(actual, 'DD/MM/YYYY');
                    scope.day = m.date();
                    scope.year = m.year();
                    var month = m.month() + 1; // momentjs months are zero based.
                    scope.month = month < 10 ? '0' + month : month.toString();
                }
            });
            scope.day = scope.month = undefined;
            // start at a 'common' birthday year to prevent long scrolling.
            scope.year = 1980;
            elem.on('change', function () {
                var completeInfo = (scope.day && scope.month && scope.year);
                if (completeInfo) {
                    var m = moment(scope.day + '/' + scope.month + '/' + scope.year, 'DD/MM/YYYY');
                    if (validDate(m)) {
                        scope.$apply(function () {
                            scope.model = m.toDate();
                            scope.__invalid = false;
                        });
                    }
                    else {
                        scope.$apply(function () {
                            scope.__invalid = true;
                        });
                    }
                }
            });
        }
    };
});
ehrApp.directive('suggestionBox', ['$http', 'Alerts', function ($http, Alerts) {
        var sendSuggestion = function () {
            var comments = $('#suggestions-modal-comments').val();
            var email = $('#suggestions-modal-email').val();
            var regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/i;
            if (!email || !regex.test(email)) {
                Alerts.error('Debe ingresar un email válido');
                return;
            }
            if (comments && comments.length > 0) {
                $http.post('/api/v1/email/suggestions', { 'message': comments, 'email': email }).then(function () {
                    Alerts.info('Sugerencia enviada. Gracias!');
                });
                $('#suggestions-modal-comments').val('');
                $('#suggestions-modal-email').val('');
            }
            Omnia.setUserEmail(email);
            $('#suggestions-modal').modal('hide');
        };
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                element.on('click', function () {
                    $('#suggestions-modal').modal('show');
                });
                $('#suggestions-modal-confirm').on('click', sendSuggestion);
            }
        };
    }]);
ehrApp.directive('selectPractice', ['$timeout', 'Practices', function ($timeout, Practices) {
        return {
            restrict: 'A',
            scope: {
                ngModel: '@',
                kind: '@'
            },
            link: function ($scope, element, attrs) {
                function createResults(practice) {
                    practice.text = practice.label;
                    if (practice.extra) {
                        practice.text = practice.text + (" (" + practice.extra + ")");
                    }
                    return practice;
                }
                ;
                function groupResults(results) {
                    if (results.length <= 0) {
                        return [];
                    }
                    var common = { 'text': 'Prácticas', 'children': [] };
                    var grouped = { 'text': 'Agrupadores', 'children': [] };
                    var displayable = _(results).filter(function (r) { return r.display; });
                    _(displayable).forEach(function (result) {
                        if (result.pid.substr(0, 2) === 'G-') {
                            grouped.children.push(result);
                        }
                        else {
                            common.children.push(result);
                        }
                    });
                    var all = [];
                    if (common.children.length > 0) {
                        all.push(common);
                    }
                    if (grouped.children.length > 0) {
                        all.push(grouped);
                    }
                    return all;
                }
                var opts = {
                    'formatNoMatches': function () { return 'No hay resultados'; },
                    'formatInputTooShort': function (_, min) { return ("ingrese al menos " + min + " caracteres"); },
                    'minimumInputLength': 3,
                    'multiple': !!attrs.multiple,
                    'query': function (query) {
                        Practices.search($scope.kind, query.term).success(function (ps) {
                            var rs = _(ps).map(createResults);
                            var needsGrouping = function (kind) { return kind === 'lab'; };
                            if (needsGrouping($scope.kind)) {
                                var gs = groupResults(rs);
                                query.callback({ 'results': gs });
                            }
                            else {
                                query.callback({ 'results': rs });
                            }
                        });
                    }
                };
                element['select2'](opts);
            }
        };
    }]);
ehrApp.directive('ngDynamicController', ['$compile', '$parse', function ($compile, $parse) {
        return {
            restrict: 'A',
            terminal: true,
            priority: 100000,
            link: function (scope, elem) {
                var name = $parse(elem.attr('ng-dynamic-controller'))(scope);
                elem.removeAttr('ng-dynamic-controller');
                elem.attr('ng-controller', name);
                $compile(elem)(scope);
            }
        };
    }]);
/*
The position is relative to acestor and the four values (poitop, poileft, poiright, poibottom) are optional.
poiorientation might be one of: right (default), left, bottom, top.
Example: (from pa-1.html#32)
<div class="poi" pointofinterest poiorientation="bottom" poititle="Medicación habitual"
poitop="-5px" poileft="-18px"
poidesc="Si el paciente tiene una medicación habitual, escribala debajo. Si no, debe marcar 'No refiere medicación habitual'"></div>
*/
ehrApp.directive('pointofinterest', function () {
    return {
        restrict: 'A',
        link: function ($scope, element, attrs) {
            var li = $(document.createElement('li')).addClass("help-point").addClass("hide");
            li.css("top", attrs.poitop);
            li.css("left", attrs.poileft);
            li.css("bottom", attrs.poibottom);
            li.css("right", attrs.poiright);
            var a = $(document.createElement('a')).addClass("poi-img-replace").attr("href", "").on('click', function () {
                var selectedPoint = $(this).parent('li');
                if (selectedPoint.hasClass('is-open')) {
                    selectedPoint.removeClass('is-open').addClass('visited');
                }
                else {
                    selectedPoint.addClass('is-open').siblings('.help-point.is-open').removeClass('is-open').addClass('visited');
                }
            });
            li.append(a);
            // orientation defaults to right
            var orientation = "poi-" + (attrs.poiorientation || "right");
            var div = $(document.createElement('div')).addClass("poi-more-info").addClass(orientation);
            var h2 = $(document.createElement('h2')).text(attrs.poititle);
            var p = $(document.createElement('p')).text(attrs.poidesc);
            div.append(h2, p);
            li.append(div);
            element.append(li);
        }
    };
});
function toggleHelp() {
    if ($('.help-point').hasClass('hide')) {
        $('.help-point').removeClass('hide');
    }
    else {
        $('.help-point').addClass('hide');
    }
}
