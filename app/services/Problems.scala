package services

import models.Problem
import scala.slick.driver.H2Driver.simple._
import scala.slick.jdbc.{StaticQuery => Q}
import scala.concurrent.Future

import _root_.util.ContextAwareExecutor.Implicits._
import play.api.Play.current
import play.api.cache.Cache
import play.api.Logger

class ProblemService  {

  private val defaultProblem = Problem(Some(1),418,"Esguince",None,7431,"active",14531,None,None,None,None,None,None)

  private def addProblem(problem: Problem) = {
    val problems = Cache.getAs[List[Problem]]("problems").getOrElse(List())
    Cache.set("problems", problem :: problems)
  }

  private def problems = Cache.getAs[List[Problem]]("problems").getOrElse(List())

  def save(problem: Problem): Future[Long] =  {
    val next = problems.size + 1
    addProblem(problem.copy(id = Some(next)))
    Future.successful(next)
  }

  def getByPatientId(id: Long) =  {
    Future.successful(problems.filter(_.editedBy.isEmpty))
  }

  def getByPatientIdWithAllergies(id: Long) =  {
    Future.successful(problems.filter(_.editedBy.isEmpty))
  }

  def getAllByPatientId(id: Long) = {
    Future.successful(problems)
  }

}
