package services

import scala.slick.driver.H2Driver.simple._
import models.{MedicalInfo, VitalSigns, Episode}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.slick.jdbc.{StaticQuery => Q, GetResult}
import play.api.Logger

import play.api.cache.Cache
import play.api.Play.current


class MedicalInfoService {

  val ENABLED_TEMPLATES = List("ingreso", "egreso", "pa", "monitoreo", "pq", "sample-form", "anestesia")

  private def addInfo(info: MedicalInfo) = {
    val infos = Cache.getAs[List[MedicalInfo]]("infos").getOrElse(List())
    Cache.set("infos", info :: infos)
  }

  private def infos = Cache.getAs[List[MedicalInfo]]("infos").getOrElse(List())

  def getById(id: Long): Future[MedicalInfo] = {
    Future.successful(infos.filter(_.id.get == id).head)
  }

  def getByEpisodeId(episode: Episode): Future[List[MedicalInfo]] = {
    Future.successful(infos)
  }

  def getByStatusAndEpisodeId(status: String, patientId: Long, episodeId: Long): Future[List[MedicalInfo]] =  {
    Future.successful(infos.filter(mi => mi.status == status))
  }

  def getByKindAndEpisodeId(kind: String, patientId: Long, episodeId: Long): Future[List[MedicalInfo]] = {
    Future.successful(infos.filter(mi => mi.kind == kind))
  }

  def save(mi: MedicalInfo): Future[Long] = {
    val next = infos.size + 1
    addInfo(mi.copy(id = Some(next)))
    Future.successful(next)
  }

  def edit(mi: MedicalInfo): Future[Long] = {
    val updated = infos.filterNot(_.id == mi.id)
    Cache.set("infos", mi :: updated)
    Future.successful(mi.id.get)
  }

  def overrides(mi: MedicalInfo): Future[Long] = {
    val next = infos.size + 1
    addInfo(mi.copy(id = Some(next)))
    val updated = infos.map { info =>
      if (mi.id == info.id) {
        info.copy(updatedBy = Some(next), updated = Some(System.currentTimeMillis))
      } else {
        info
      }
    }
    Cache.set("infos", updated)
    Future.successful(next)
  }

  def updateStatus(id: Long, newStatus: String, updatedWhen: Long): Future[Long] = {
    val updated = infos.map { mi =>
      if (mi.id.get == id) {
        mi.copy(status = newStatus, updated = Some(updatedWhen))
      } else {
        mi
      }
    }
    Cache.set("infos", updated)
    Future.successful(id)
  }

  def delete(id: Long) = {
    val updated = infos.map { mi =>
      if (mi.id.get == id) {
        mi.copy(status = "deleted", updated = Some(System.currentTimeMillis), updatedBy = Some(-1))
      } else {
        mi
      }
    }
    Cache.set("infos", updated)
    Future.successful(id)
  }

  def getAvailableTemplates(speciality: Long): Future[List[String]] =  {
    Future.successful(ENABLED_TEMPLATES)
  }

}
