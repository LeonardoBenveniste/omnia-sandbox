package services

import models.{TimelineEntry, Episode}
import scala.slick.driver.H2Driver.simple._
import play.api.cache.Cache
import play.api.Play.current
import scala.concurrent.Future


class TimelineLog {

  def addEntry(t: TimelineEntry) = {
    val tl = Cache.getAs[List[TimelineEntry]]("tl").getOrElse(List())
    Cache.set("tl", t :: tl)
  }

  def tl = Cache.getAs[List[TimelineEntry]]("tl").getOrElse(List())

  def append(t: TimelineEntry) = {
    addEntry(t)
    Future.successful(1L)
  }

  def getByEpisode(patient: Long, episode: Episode) = {
    Future.successful(tl)
  }
}