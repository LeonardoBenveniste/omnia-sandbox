package services

import play.api._
import play.api.Play.current
import play.api.libs.concurrent.Akka
import akka.actor._
import models.TimelineEntry

/*
 Note: Events are too timeline specific, maybe a more general approach is needed?
 e.g.

  sealed class OmniaEvent
  case class AllergyCreated(allergy: Allergy, owner: Long) extends OmniaEvent
  case class ProblemCreated(problem: Problem) extends OmniaEvent
  case class EvolutionCreated(evolution: Evolution) extends OmniaEvent
  case class IndicationCreated(indication: Indication) extends OmniaEvent
  case class ResultCreated(result: Result) extends OmniaEvent

*/

trait EventSupport {
  private def emit(t: TimelineEntry) = Akka.system.eventStream.publish(t)

  def emitNewProblem(id: Long, owner: Long, patient: Long, episode: Long, specialty: String, udn: String) = {
    val event = TimelineEntry.newProblem(id, owner, patient, episode, specialty, udn)
    emit(event)
  }

  def emitNewBackground(id: Long, owner: Long, patient: Long, episode: Long, specialty: String, udn: String) = {
    val event = TimelineEntry.newBackground(id, owner, patient, episode, specialty, udn)
    emit(event)
  }

  def emitEditProblem(oldId: Long, id: Long, owner: Long, patient: Long, episode: Long, specialty: String, udn: String) = {
    val event = TimelineEntry.editProblem(oldId, id, owner, patient, episode, specialty, udn)
    emit(event)
  }

  def emitNewEvolution(id: Long, owner: Long, patient: Long, when: Long, specialty: String, episode: Long, udn: String, location: Option[String]) = {
    val event = TimelineEntry.newEvolution(id, owner, patient, when, specialty, episode, udn, location)
    emit(event)
  }

  def emitEvolutionDeleted(id: Long, owner: Long, patient: Long, specialty: String, episode: Long, udn: String) = {
    val event = TimelineEntry.deleteEvolution(id, owner, patient, specialty, episode, udn)
    emit(event)
  }

  def emitEvolutionEdited(id: Long, owner: Long, patient: Long, overridenBy: Long, specialty: String, episode: Long, udn: String, location: Option[String]) = {
    val event = TimelineEntry.editEvolution(id, owner, patient, overridenBy, specialty, episode, udn, location)
    emit(event)
  }

  def emitNewVitalSign(id: Long, owner: Long, patient: Long, specialty: String, episode: Long, udn: String) = {
    val event = TimelineEntry.newVitalSign(id, owner, patient, specialty, episode, udn)
    emit(event)
  }

  def emitDeletedVitalSign(id: Long, owner: Long, patient: Long, specialty: String, episode: Long, udn: String) = {
    val event = TimelineEntry.deletedVitalSign(id, owner, patient, specialty, episode, udn)
    emit(event)
  }

  def emitMedicalInfoClosed(id: Long, owner: Long, patient: Long, specialty: String, episode: Long, udn: String, location: Option[String]) = {
    val event = TimelineEntry.closeMedicalInfo(id, owner, patient, specialty, episode, udn, location)
    emit(event)
  }

  def emitMedicalInfoEdited(id: Long, owner: Long, patient: Long, overridenBy: Long, specialty: String, episode: Long, udn: String, location: Option[String]) = {
    val event = TimelineEntry.editMedicalInfo(id, owner, patient, overridenBy, specialty, episode, udn, location)
    emit(event)
  }

  def emitMedicalInfoDeleted(id: Long, owner: Long, patient: Long, specialty: String, episode: Long, udn: String) = {
    val event = TimelineEntry.deleteMedicalInfo(id, owner, patient, specialty, episode, udn)
    emit(event)
  }

  def emitNewSolicitation(id: Long, owner: Long, patient: Long, specialty: String, episode: Long, udn: String, location: Option[String]) = {
    val event = TimelineEntry.newSolicitation(id, owner, patient, specialty, episode, udn, location)
    emit(event)
  }

  def emitSolicitationDeleted(id: Long, owner: Long, patient: Long, specialty: String, episode: Long, udn: String) = {
    val event = TimelineEntry.deleteSolicitation(id, owner, patient, specialty, episode, udn)
    emit(event)
  }

  def emitConciliateProblem(patientId: Long, problemId: Long, episodeId: Long, ownerId: Long, specialty: String, udn: String) = {
    val event = TimelineEntry.conciliateProblem(patientId, problemId, episodeId, ownerId, specialty, udn)
    emit(event)
  }

  def emitContingency(contingencyId: Long, patientId: Long, episodeId: Long, ownerId: Long, specialty: String, udn: String) = {
    val event = TimelineEntry.contingency(contingencyId, patientId, episodeId, ownerId, specialty, udn)
    emit(event)
  }

  def emitContingencyDeleted(contingencyId: Long, patientId: Long, episodeId: Long, ownerId: Long, specialty: String, udn: String) = {
    val event = TimelineEntry.contingencyDeleted(contingencyId, patientId, episodeId, ownerId, specialty, udn)
    emit(event)
  }
}