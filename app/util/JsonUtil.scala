package util

import play.api.libs.json._

object JsonUtil {
  object ErrorWrites extends Writes[JsError] {
    def writes(error: JsError) = Json.obj {
      "errors" -> {
        error.errors map { case (path, validationErrors) => Json.obj(
          "path" -> path.toString(),
          "validationErrors" -> validationErrors.map(_.message)
        )}
      }
    }
  }
}
