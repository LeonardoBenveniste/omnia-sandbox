import play.api._
import play.api.mvc._
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.libs.ws._
import services.TimelineLog
import scala.concurrent.Future
import java.util.concurrent.Executors
import _root_.util.{Configuration => Conf}

object Global extends WithFilters(Mdc) with GlobalSettings with DependencyInjection {

  private val Ex = Executors.newSingleThreadExecutor
  implicit val TasksEx = scala.concurrent.ExecutionContext.fromExecutor(Ex)

  override def onStart(app: Application) {
    if (!Play.isTest) {
      startScheduledTasks()
    }
  }

  def logErrorAndExit(message: String): Unit = {
    Logger.error(message)
    System.exit(1)
  }

  override def onStop(app: Application) = {
    Ex.shutdown()
  }

  private def startScheduledTasks() = {
    import scala.concurrent.duration._
    Akka.system.actorOf(TimelineActor.props(injector.getInstance(classOf[TimelineLog])))
  }

  /*
    when a URL is not found in the routes, one of two things can happen:

    (1) it's indeed an error and we need to return 404.
    (2) the route exists in the javascript router, so we need to redirect to / and let it handle it.

    examples of (2) are: `/patients` or `/calendar` routes that have no place in the backend, but
    exists solely for UI routing purposes.

    we assume that all API calls are not UI routes and return the proper error code (404).
  */
  override def onHandlerNotFound(request: RequestHeader) = {
    val isApiCall = request.uri.startsWith("/api/v1/")
    val result = if (isApiCall) {
      Results.NotFound
    } else {
      val app = injector.getInstance(classOf[controllers.Application])
      app.indexResult(request)
    }

    Future.successful(result)
  }
}

object AccessLog extends Filter {

  type LogFunction = String => Unit

  private val info: LogFunction = message => Logger("access-log").info(message)
  private val debug: LogFunction = message => Logger("access-log").debug(message)

  def apply(next: (RequestHeader) => Future[Result])(req: RequestHeader) = {
    def noise() = req.uri.contains("/assets/") || req.uri.contains("/healthcheck")
    val logger = if (noise()) debug else info

    logger(s"${req.method} ${req.uri} (from: ${req.remoteAddress})")
    logger("User Agent: " + req.headers.get("User-Agent").getOrElse("Undefined"))
    logger("ClientVersion: " + req.headers.get("X-ClientVersion").getOrElse("Undefined"))

    next(req)
  }
}

object Mdc extends Filter {
  import org.slf4j.MDC

  def apply(next: (RequestHeader) => Future[Result])(request: RequestHeader) = {
    val user = request.session.get("id").getOrElse("unknown")
    MDC.put("UserId", user)
    next(request)
  }
}

object FuckIe extends Filter {
  val ForgetIt = Results.Redirect("/assets/pages/update_browser.html")

  def apply(next: (RequestHeader) => Future[Result])(request: RequestHeader) = {

    val agent = request.headers.get("User-Agent")
    val internetExplorer = (agent.isDefined && agent.get.contains("MSIE"))

    if (request.uri.contains("/assets/") || !internetExplorer)
      next(request)
    else {
      Logger.warn(s"User-Agent is ${agent.get}, redirecting to browser_update page")
      Future.successful(ForgetIt)
    }
  }
}
