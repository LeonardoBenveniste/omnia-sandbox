package controllers

import play.api.mvc._
import play.api.libs.json._
import play.api.Logger
import _root_.util.ContextAwareExecutor.Implicits._
import services.{EvolutionService, EventSupport}
import scala.concurrent.Future
import models.{Evolution, Problem, Episode}
import com.google.inject.Inject

class EvolutionsController @Inject() (service: EvolutionService)
  extends BaseController with EventSupport {

  def save(episode: Long) = Action.async(parse.json) { implicit request =>
    Logger.info(s"creating new evolution for episode ${episode}")
    val owner = 10L
    val fqe = Episode(episode, udn.get).fqe
    val json = request.body.as[JsObject] ++ Json.obj("episode" -> episode, "fqe" -> fqe)
    val evolution = json.as[Evolution]
    val location = (json \ "location").as[Option[String]]
    val problems = (json \ "problemsIds").as[List[Long]]
    if (problems.isEmpty) {
      Future.successful(BadRequest("Evolution should have at least one problem"))
    } else {
      service.save(evolution, problems) map { id =>
        emitNewEvolution(id, owner, evolution.patient, evolution.date, specialty.get, evolution.episode, udn.get, location)
        Created(Json.toJson(Map("id" -> id)))
      }
    }
  }

}
