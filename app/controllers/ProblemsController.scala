package controllers

import play.api.mvc._
import play.api.libs.json._
import play.api.Logger
import _root_.util.ContextAwareExecutor.Implicits._
import services._
import scala.concurrent.Future
import com.google.inject.Inject
import models.{Problem, Episode}

class ProblemsController @Inject() (service: ProblemService) extends BaseController with EventSupport {

  def getAll(patientId: Long) = Action.async {
    service.getByPatientId(patientId) map { problems =>
      Ok(Json.toJson(problems))
    }
  }

  def getWithAllergies(patientId: Long) = Action.async {
    Logger.info(s"retrieving problems and allergies for patient ${patientId}")
    service.getByPatientIdWithAllergies(patientId) map { problems =>
      Ok(Json.toJson(problems))
    }
  }

  def save(patientId: Long, episodeId: Long) = Action.async(parse.json) { implicit request =>
    Logger.info(s"saving new problem for patient: ${patientId} episode: ${episodeId}")
    val owner = 10L
    val fqe = Episode(episodeId, udn.get).fqe
    val json = request.body.as[JsObject] ++
      Json.obj("patient" -> patientId, "created" -> System.currentTimeMillis, "episode" -> episodeId, "fqe" -> fqe)

    val problem = json.as[Problem]
    val result = service.save(problem).map { id =>
      emitNewProblem(id, owner, problem.patient, problem.episode, specialty.get, udn.get)
      Created(Json.toJson(Map("id" -> id)))
    }
    result
  }

  def conciliate(patientId: Long, episodeId: Long, problemId: Long) = Action { implicit request =>
    val owner = 10L
    emitConciliateProblem(patientId, problemId, episodeId, owner, specialty.get, udn.get)
    Created
  }
}
