package controllers

import com.google.inject.Inject
import scala.concurrent.Future
import _root_.util.ContextAwareExecutor.Implicits._
import play.api.libs.json._
import play.api.mvc._
import play.api.Logger
import models._
import services._

class TimelineController @Inject() (log: TimelineLog, problems: ProblemService, evolutions: EvolutionService,
   medicalInfo: MedicalInfoService) extends BaseController {

  def getByEpisode(patient: Long, episodeNumber: Long) = Action.async { implicit request =>
    val episode = Episode(episodeNumber, udn.get)
    val episodeProblems = problems.getAllByPatientId(patient)
    val patientEvolutions = evolutions.getByPatientIdAndEpisode(patient, episode)
    val patientMedicalInfo = medicalInfo.getByEpisodeId(episode)
    val timeline = log.getByEpisode(patient, episode)

    val requester = info.get.data.id

    val response = for {
      pr <- episodeProblems
      ev <- patientEvolutions
      mi <- patientMedicalInfo
      tl <- timeline
    } yield createTimelineJson(pr, ev, mi, tl, List(Author(10, "Juan", "Juan", "Petruza", Some("MN: 1234"))))

    response.map(Ok(_))
  }

  private def problemsByEvolution(evolutions: Map[Evolution, List[Long]]) = evolutions.map { case (evo, problems) =>
    (evo.id.get.toString, problems)
  }

  def createTimelineJson(problems: List[Problem], evolutions: Map[Evolution, List[Long]], medicalInfo: List[MedicalInfo],
    timeline: List[TimelineEntry], authors: List[Author]) = {

    Json.obj() +
      (("problems", Json.toJson(problems))) +
      (("evolutions", Json.toJson(evolutions.keys))) +
      (("evolution_problems", Json.toJson(problemsByEvolution(evolutions)))) +
      (("medical_info", Json.toJson(medicalInfo))) +
      (("authors", Json.toJson(authors))) +
      (("timeline", Json.toJson(timeline)))
  }
}