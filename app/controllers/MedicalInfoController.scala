package controllers

import play.api.mvc._
import play.api.libs.json._
import play.api.Logger
import _root_.util.ContextAwareExecutor.Implicits._
import services.{EventSupport, MedicalInfoService}
import scala.concurrent.Future
import com.google.inject.Inject
import models.{MedicalInfo, Episode}

class MedicalInfoController @Inject() (service: MedicalInfoService) extends BaseController with EventSupport {

  type MedicalInfoToResult = PartialFunction[JsResult[MedicalInfo], Future[Result]]

  def SaveMedicalInfo(owner: Long, episode: Long)(implicit request: RequestHeader): MedicalInfoToResult = { case JsSuccess(medicalInfo, _) =>
    service.save(medicalInfo) map { id =>
      if (medicalInfo.kind == "vital-signs") {
        emitNewVitalSign(id, owner, medicalInfo.patientId, specialty.get, episode, udn.get)
      }
      Created(Json.toJson(Map("id" -> id)))
    }
  }

  def EditMedicalInfo(owner: Long, location: Option[String])(implicit request: RequestHeader): MedicalInfoToResult = { case JsSuccess(medicalInfo, _) =>
    service.getById(medicalInfo.id.get) flatMap { mi =>
      if (mi.status == "open") {
        service.edit(medicalInfo) map { id =>
          Created(Json.toJson(Map("id" -> medicalInfo.id.get)))
        }
      } else if (mi.status == "closed") {
        service.overrides(medicalInfo) map { id =>
          emitMedicalInfoClosed(id, owner, medicalInfo.patientId, specialty.get, medicalInfo.episode, udn.get, location)
          emitMedicalInfoEdited(medicalInfo.id.get, owner, medicalInfo.patientId, id, specialty.get, medicalInfo.episode, udn.get, location)
          Created(Json.toJson(Map("id" -> id)))
        }
      } else {
        Future.successful(Forbidden)
      }
    }
  }

  def get(patientId: Long, episodeId: Long, status: Option[String], kind: Option[String]) = Action.async {
    if (kind.isDefined) {
      service.getByKindAndEpisodeId(kind.get, patientId, episodeId) map { medical =>
        Ok(Json.toJson(medical))
      }
    } else if (status.isDefined) {
      service.getByStatusAndEpisodeId(status.get, patientId, episodeId) map { medical =>
        Ok(Json.toJson(medical))
      }
    } else {
      Future.successful(BadRequest)
    }
  }

  def save(pid: Long, episode: Long) = Action.async(parse.json){ implicit request =>
    val owner = 10L
    val fqe = Episode(episode, udn.get).fqe
    val info = request.body.as[JsObject] ++
      Json.obj("patientId" -> pid, "created" -> System.currentTimeMillis, "episode" -> episode, "fqe" -> fqe);
    val jsResult = info.validate[MedicalInfo]
    val saver = SaveMedicalInfo(owner, episode)
    saver(jsResult)
  }

  def edit(patientId: Long, episode: Long, infoId: Long) = Action.async(parse.json){ implicit request =>
    val owner = 10L
    val fqe = Episode(episode, udn.get).fqe
    val info = request.body.as[JsObject] ++
      Json.obj("patientId" -> patientId, "episode" -> episode, "fqe" -> fqe)
    val jsResult = info.validate[MedicalInfo]
    val location = (info \ "location").as[Option[String]]
    val saver = EditMedicalInfo(owner, location)
    saver(jsResult)
  }

  def updateStatus(patientId: Long, episode: Long, infoId: Long) = Action.async(parse.json){ implicit request =>
    val owner = 10L
    val json = request.body.as[JsObject]
    val status = (json \ "status").as[String]
    val kind = (json \ "kind").as[String]
    val location = (json \ "location").as[Option[String]]
    val updated = (json \ "updated").as[Long]
    service.updateStatus(infoId, status, updated) map { ids =>
      if (status == "closed") {
        emitMedicalInfoClosed(infoId, owner, patientId, specialty.get, episode, udn.get, location)
      } else if (status == "deleted" && kind == "vital-signs") {
        emitDeletedVitalSign(infoId, owner, patientId, specialty.get, episode, udn.get)
      }
      Created(Json.toJson(Map("id" -> infoId)))
    }
  }

  def remove(patientId: Long, episode: Long, infoId: Long) = Action.async { implicit request =>
    val owner = 10L
    service.delete(infoId) map { ids =>
      emitMedicalInfoDeleted(infoId, owner, patientId, specialty.get, episode, udn.get)
      Created(Json.toJson(Map("id" -> infoId)))
    }
  }

  def availableTemplates = Action.async { implicit request =>
    service.getAvailableTemplates(info.get.activeSpecialty.id).map { templateIds =>
      Ok(Json.toJson(templateIds))
    }
  }

}