package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import _root_.util.ContextAwareExecutor.Implicits._
import play.api.Logger
import play.api.Play.current
import services._
import scala.concurrent._
import scala.concurrent.duration._
import _root_.util.Configuration
import play.api.libs.json._
import com.google.inject.Inject

class Application () extends BaseController {

  type Arguments = Map[String, Seq[String]]

  def index = Action { implicit request =>
    indexResult(request)
  }

  def indexResult(request: RequestHeader): Result = {
    Ok(views.html.sandbox())
  }

}